import urllib
import lxml.html
from BeautifulSoup import BeautifulSoup as soup
import os
import sys
print "=========================="
pid = str(os.getpid())
print "Running process ID : " + pid
processes = open("./process/running_process", "a").write(sys.argv[1]+":"+sys.argv[2]+":"+pid+"\n")
print "=========================="

data = open("data.xml","r")
lines = data.read()
html = soup(lines)
print "starting script from : " + sys.argv[1] + "  :  " + sys.argv[2]
a = list(set([tag.attrMap['href'] for tag in html.findAll('a', {'href': True})]))[int(sys.argv[1]):int(sys.argv[2])]

def get_content(url):
	try:
		sock = urllib.urlopen(url)
		htmlsource = sock.read()
		sock.close()
		return htmlsource
	except:
		return None

def get_table(url):
	try:
		content = get_content(url)
		doc = lxml.html.document_fromstring(content)
		row_elements = doc.xpath('//table')[1][2][2]
		return row_elements
	except:
		return None

def write(con):
	text_file = open("./data/"+A.split("?")[-1], "w+")
	text_file.write("%s"%con)
	print "				Success !!"
	text_file.close()

counter = int(sys.argv[1])
for A in a:
	if(os.path.isfile("./data/"+A.split("?")[-1])):
		counter = counter + 1
		print sys.argv[1] + " [Skipping =>] " + A
		pass
	else:
		counter = counter + 1
		print str(counter) + " => " + A
		con = get_table(A)
		try:
			con = lxml.html.tostring(con)
			write(con)
		except:
			pass