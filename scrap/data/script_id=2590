<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">FSwitch : Allows switching between companion source files (like a.vim)</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>80/31</b>,
    Downloaded by 2237    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2590">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=17010">Derek Wyatt</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>This script is designed to switch between companion files much like <a href="/scripts/script.php?script_id=31">vimscript #31</a> (a.vim).<br><br>Out of the box it will support C and C++ for most usages, unless you've got an interesting setup I haven't guessed.<br><br>You can:<br><br>&#160;&#160;- switch the file and keep it in the current window with :FSHere<br><br>&#160;&#160;- switch the file and put it in the window to the left with :FSLeft<br>&#160;&#160;- switch the file, split the window to the left and put it there with :FSSwitchLeft<br><br>&#160;&#160;- the same can be done with Right, Above and Below.<br><br>The switch function should be easy enough to call in different ways that give you more exotic behaviour.<br><br>Using the location directive, you can be quite specific about how the companion file is resolved and, more importantly how it's created when it doesn't exist.&#160;&#160;This latter bit was apparently a sore point with a.vim and I hope that it's acceptable in FSwitch.<br><br>You can state locations as being the result of<br><br>- a regular expression substitution<br>- a globbed match<br>- a relative path (either unconditionally or only if the current buffer's path matches a required pattern)<br>- an absolute path (either unconditionally or only if the current buffer's path matches a required pattern)<br><br>Using an autocmd to set the buffer-local variables allows you great flexibility in specializing the behaviour of FSwitch.<br><br>There are also a couple of public functions you can use to retrieve the companion file for use in your own functions as well.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>I've put this in a format that's easiest to use with pathogen (<a href="/scripts/script.php?script_id=2332">vimscript #2332</a>).&#160;&#160;Just unzip it in the right spot (say, ~/.vim/bundle) or unzip it and copy the 'plugin' and 'doc' directories into the right spot in the runtime path (say, ~/.vim).</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2590"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2590">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=14047">fswitch-0.9.3.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-10-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17010">Derek Wyatt</a></i></td>
    <td class="rowodd" valign="top" width="2000">Small fix to check for Vim version (Timon Kelter)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=11504">FSwitch-0.9.2.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2009-10-10</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17010">Derek Wyatt</a></i></td>
    <td class="roweven" valign="top" width="2000">Fix to the split commands (Thanks to Michael Henry)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=10277">FSwitch-0.9.1.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-03-25</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17010">Derek Wyatt</a></i></td>
    <td class="rowodd" valign="top" width="2000">See :h fswitch-changes<br><br>Added :ifrel and :ifabs location directives<br>Exposed two public functions for retrieving the companion file in a script<br>Changed defaults for c/c++ code to use :ifrel instead of :rel</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=10275">FSwitch-0.9.0.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2009-03-24</i></td>
    <td class="roweven" valign="top" nowrap>7.2</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17010">Derek Wyatt</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  