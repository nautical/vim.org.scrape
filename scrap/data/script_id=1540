<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">mousefunc option patch : Patch set to create a mousefunc option</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>0/0</b>,
    Downloaded by 492    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1540">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=2467">Eric Arnold</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>When compiled with this patch set, gvim will have a new option, "mousefunc" which will call a user script function for every mouse event.<br><br>The zip file contains a test script which demonstrates how to do things like:<br><br><br>" This script tests various aspects of the "mousefunc" option:<br>"<br>" - demonstrate having the mouse move tabs in the tabline.<br>"<br>" - text hotspots:	Here is mouseover |tag1| and here is |tag2|<br>"<br>" - verbose option which shows all mouse info.<br>"<br>" - mouse-follow highlighting (set s:show_bullseye)<br>"<br>" - associate actions with clicking on statusline words<br><br><br><br><br><br>From the options.txt doc file:<br><br><br>'mousefunc' 'mfn'	string	(default empty)<br>			global<br>			{not in Vi}<br>	When nonempty, this option calls the given function when mouse events<br>	(click, drag, move, etc.) are detected.&#160;&#160;The function should accept<br>	these arguments:<br>&gt;<br>		MouseFunc(	mouse_key, <br>				\ buf_row, buf_col, buf_vcol, <br>				\ mouse_row, mouse_col, <br>				\ x, y,<br>				\ area, screen_line )<br>&lt;<br>	"mouse_key" is a string containing up to 6 chars.&#160;&#160;It will match<br>	against a string like:&#160;&#160; a:mouse_key == "\&lt;LeftMouse&gt;".<br>	"mouse_key" can be empty, particularly when cursor-move-only events<br>	are delivered.&#160;&#160;<br><br>	"buf_row/col/vcol" should match line(".") and col(".") if you were<br>	to click in a buffer at that point.<br><br>	"mouse_row/col" are independent of windows and buffers.<br><br>	The "x" and "y" are in pixels, and can be negative, when that<br>	information isn't available (see also |mousefocus| to have "x" and<br>	"y" track the mouse cursor).&#160;&#160;<br><br>	"area" is Vim's idea of where the mouse is.&#160;&#160;Values can be:<br>	"IN_BUFFER", "IN_UNKNOWN", "IN_SEP_LINE", "IN_STATUS_LINE".<br><br>	"screen_line" contains the literal characters in "mouse_row" which<br>	have been written to the screen, rather than any buffer.&#160;&#160;Useful to<br>	read what's on the statusline, tabline, and command line areas.<br><br>	The function should return 0 to consume the mouse click, or 1 to<br>	pass it on transparently.<br><br>	The |mousefunc| option is set empty for the duration of the call to<br>	prevent recursion.&#160;&#160;Setting it again during the call could have<br>	unexpected problems.<br><br>	It intercepts the mouse events at a low level, which has its good<br>	and bad points.&#160;&#160;The good is that you get the mouse events before<br>	other restrictions like statusline, tabline, empty regions, etc.,<br>	and receive mouse events while in any mode.&#160;&#160;The bad is that you<br>	have to watch out for things like recursion errors (i.e.&#160;&#160;calling<br>	getchar() from a script function while |mousefunc| is set).<br><br>	This function is called often when the mouse is moving, so attention<br>	should be given to returning early out of as many cycles as<br>	possible.<br><br>	At the moment, drag and release events are approximated based on the<br>	last primary event (i.e. &lt;leftmouse&gt; or &lt;rightmouse&gt; ).<br><br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>The zip contains the patch files to be applied to the corresponding Vim7 source files.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="1540"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1540">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5695">mousefunc.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-05-14</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=2467">Eric Arnold</a></i></td>
    <td class="rowodd" valign="top" width="2000">Now tst script fully uses tabline indexes and resize wrapping spots for TabLineSet.vim</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5693">mousefunc.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-05-14</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=2467">Eric Arnold</a></i></td>
    <td class="roweven" valign="top" width="2000">Smoother tab move code in test script.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5684">mousefunc.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-05-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=2467">Eric Arnold</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  