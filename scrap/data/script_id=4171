<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">sideways.vim : Move function arguments (and other delimited-by-something items) left and right</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>1/1</b>,
    Downloaded by 123    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:4171">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=31799">Andrew Radev</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Github project: <a target="_blank" href="https://github.com/AndrewRadev/sideways.vim">https://github.com/AndrewRadev/sideways.vim</a><br><br>The plugin defines two commands, ":SidewaysLeft" and ":SidewaysRight", which move the item under the cursor left or right, where an "item" is defined by a delimiter. As an example:<br><br>&#160;&#160;&#160;&#160;def function(one, two, three):<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;pass<br><br>Placing the cursor on "two" and executing `:SidewaysLeft`, the "one" and "two" arguments will switch their places, resulting in this:<br><br>&#160;&#160;&#160;&#160;def function(two, one, three):<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;pass<br><br>In this case, the delimiter is a comma. The plugin currently works with various other cases and it's intended to make the process configurable. While this particular example is in python, this should work for arguments in many different languages that use round braces to denote function calls.<br><br>Apart from functions, it works for square-bracket lists in dynamic languages:<br><br>&#160;&#160;&#160;&#160;list = [one, [two, four, five], three]<br><br>Notice that, if you experiment with this example, you'll find that you can move the entire second list around. The plugin takes into consideration nested structures.<br><br>Apart from functions, it works for lists in CSS declarations:<br><br>&#160;&#160;&#160;&#160;border-radius: 20px 0 0 20px;<br><br>And, it also works for cucumber tables (see docs for better table formatting):<br><br>&#160;&#160;&#160;&#160;Examples:<br>&#160;&#160;&#160;&#160;&#160;&#160;| input_1 | input_2 | button | output |<br>&#160;&#160;&#160;&#160;&#160;&#160;| 20&#160;&#160;&#160;&#160;&#160;&#160;| 30&#160;&#160;&#160;&#160;&#160;&#160;| add&#160;&#160;&#160;&#160;| 50&#160;&#160;&#160;&#160; |<br>&#160;&#160;&#160;&#160;&#160;&#160;| 2&#160;&#160;&#160;&#160;&#160;&#160; | 5&#160;&#160;&#160;&#160;&#160;&#160; | add&#160;&#160;&#160;&#160;| 7&#160;&#160;&#160;&#160;&#160;&#160;|<br>&#160;&#160;&#160;&#160;&#160;&#160;| 0&#160;&#160;&#160;&#160;&#160;&#160; | 40&#160;&#160;&#160;&#160;&#160;&#160;| add&#160;&#160;&#160;&#160;| 40&#160;&#160;&#160;&#160; |<br><br>It's highly suggested to map the two commands to convenient keys. For example, mapping them to &lt;c-h&gt; and &lt;c-l&gt; would look like this:<br><br>&#160;&#160;&#160;&#160;nnoremap &lt;c-h&gt; :SidewaysLeft&lt;cr&gt;<br>&#160;&#160;&#160;&#160;nnoremap &lt;c-l&gt; :SidewaysRight&lt;cr&gt;<br><br>The plugin is intended to be highly customizable, eventually. In the future, it should be able to work with ruby function arguments and it may also contain an "argument" text object (since the machinery to detect arguments is already there).<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>There are several ways to install the plugin. The recommended one is by using Tim Pope's pathogen (<a target="_blank" href="http://www.vim.org/scripts/script.php?script_id=2332">http://www.vim.org/scripts/script.php?script_id=2332</a>). In that case, you can clone the plugin's git repository like so:<br><br>&#160;&#160;&#160;&#160;git clone <a target="_blank" href="git://github.com/AndrewRadev/sideways.vim.git">git://github.com/AndrewRadev/sideways.vim.git</a> ~/.vim/bundle/sideways<br><br>If your vim configuration is under git version control, you could also set up the repository as a submodule, which would allow you to update more easily. The command is (provided you're in ~/.vim):<br><br>&#160;&#160;&#160;&#160;git submodule add <a target="_blank" href="git://github.com/AndrewRadev/sideways.vim.git">git://github.com/AndrewRadev/sideways.vim.git</a> bundle/sideways<br><br>Another way is to simply copy all the essential directories inside the ~/.vim directory: plugin, autoload, doc.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="4171"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=4171">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18756">sideways.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.0.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-06</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Single-line CSS declarations<br>- Various bugfixes and refactoring</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18404">sideways.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-08-15</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  