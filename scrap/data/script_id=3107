<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">writebackupToAdjacentDir : Backup to an adjacent backup directory. </span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>10/4</b>,
    Downloaded by 215    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3107">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Redirect backups made by the writebackup plugin (<a href="/scripts/script.php?script_id=1828">vimscript #1828</a>) that would<br>normally go into the original file's directory into an adjacent directory with a<br>"{dir}.backup" name, if it exists. This allows to use the plugin in places where<br>backup files would cause problems.<br><br>DESCRIPTION<br>Many customization directories (e.g. /etc/profile.d/) consider all contained<br>files, regardless of file extension or execute permissions. Creating a<br>{file}.YYYYMMDD[a-z] backup in there causes trouble and strange effects,<br>because the backups are used in addition to the main configuration file - not<br>what was intended! However, putting the backups in the same directory<br>generally is a good idea - just not for these special directories.<br><br>This plugin offers a solution by integrating into the writebackup.vim plugin<br>so that it checks for a directory with a '.backup' extension (e.g.<br>/etc/profile.d.backup/), and places the backups in there, in case it exists.<br>In all other cases, the backup is made in the default directory, as before.<br><br>USAGE<br>Adjacent backup directories are never created by this plugin; you have to<br>create such a directory yourself to indicate that backups should be placed in<br>there.<br><br>:WriteBackupMakeAdjacentDir [{prot}]<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Create a backup directory adjacent to the current file's<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;directory. If {prot} is given it is used to set the protection<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;bits; default is 0755.<br><br>After the adjacent backup directory has been created, just use :WriteBackup<br>as before.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>INSTALLATION<br>This script is packaged as a vimball. If you have the "gunzip" decompressor<br>in your PATH, simply edit the *.vba.gz package in Vim; otherwise, decompress<br>the archive first, e.g. using WinZip. Inside Vim, install by sourcing the<br>vimball or via the :UseVimball command. <br>&#160;&#160;&#160;&#160;vim writebackupToAdjacentDir.vba.gz<br>&#160;&#160;&#160;&#160;:so %<br>To uninstall, use the :RmVimball command. <br><br>DEPENDENCIES<br>- Requires Vim 7.0 or higher. <br>- Requires the writebackup plugin (<a href="/scripts/script.php?script_id=1828">vimscript #1828</a>), version 1.30 or<br>&#160;&#160;higher. <br>- The writebackupVersionControl plugin (<a href="/scripts/script.php?script_id=1829">vimscript #1829</a>), which<br>&#160;&#160;complements writebackup, fully supports this extension, but is not<br>&#160;&#160;required. <br><br>CONFIGURATION<br>For a permanent configuration, put the following commands into your vimrc (see<br>:help vimrc):<br><br>To change the name of the adjacent backup directory, specify a different<br>template via<br>&#160;&#160;&#160;&#160;let g:WriteBackupAdjacentDir_BackupDirTemplate = '%s.backup'<br>This must contain the "%s" placeholder, which is replaced with the original<br>file's directory, e.g. "backup of %s". <br><br>This plugin injects itself into writebackup.vim via the<br>g:WriteBackup_BackupDir configuration. Its previous value is saved in<br>g:WriteBackupAdjacentDir_BackupDir and used as a fallback, when no adjacent<br>directory exists. If you need to change the fallback after sourcing the<br>plugins, use the latter variable. However, to override this for a particular<br>buffer, you still have to use the b:WriteBackup_BackupDir variable, as this<br>plugin does not provide yet another override. </td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3107"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3107">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17485">writebackupToAdjacentDir.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.10</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-02-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">ENH: Save configured g:WriteBackup_BackupDir and use that as a fallback instead of always defaulting to '.', thereby allowing absolute and dynamic backup directories as a fallback. Suggested by Geoffrey Nimal. </td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13088">writebackupToAdjacentDir.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.00</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-06-02</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  