<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">SnippetySnip : Inserts named snippets from various other files into a file</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>0/2</b>,
    Downloaded by 274    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3717">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>SnippetySnip is a vim tool for automatically inserting parts (snippets) of other files in a file. I wrote it to insert sourcecode examples from source files into html blog-posts, but it can be used for all sorts of text files.<br><br>In the source file, you mark up a region of text (a snippet) and give it a name. In the target file, you mark where you want this particular snippet inserted. <br><br>For instance:<br><br>source.py:<br>&#160;&#160;&#160;&#160;(...)<br>&#160;&#160;&#160;&#160;#snippetysnip_begin:foo<br>&#160;&#160;&#160;&#160;def foo():<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;return 0<br>&#160;&#160;&#160;&#160;#snippetysnip_end<br>&#160;&#160;&#160;&#160;(...)<br><br>target.html:<br>&#160;&#160;&#160;&#160;&lt;p&gt;&lt;code&gt;<br>&#160;&#160;&#160;&#160;&lt;!-- snippetysnip:/path/to/source.py:foo --&gt;<br>&#160;&#160;&#160;&#160;&lt;/code&gt;&lt;/p&gt;<br><br>When SnippetySnip is run, the function foo() from source.py is inserted into the html file, which then looks like this:<br><br>target.html:<br>&#160;&#160;&#160;&#160;&lt;p&gt;&lt;code&gt;<br>&#160;&#160;&#160;&#160;&lt;!-- snippetysnip:/path/to/source.py:foo --&gt;<br>&#160;&#160;&#160;&#160;def foo():<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;return 0<br>&#160;&#160;&#160;&#160;&lt;!-- snippetysnip_end:/path/to/source.py:foo --&gt;<br>&#160;&#160;&#160;&#160;&lt;/code&gt;&lt;/p&gt;<br><br><br>SnippetySnip does not care about the format of the comments in your particular language, it only looks for the snippetysnip-strings. A snippet name can consist of letters, numbers or any of the characters "-_.".<br><br>You can have many snippets in a source file, and a target file can reference multiple snippets from multiple sources.<br><br>Paths can be relative or absolute.<br><br><br><br>HOW TO USE<br>In the source files, surround each snippet with snippetysnip_begin:SNIPPET_NAME and snippetysnip_end. These tags need to be on separate lines, but except from that, they can be inside the comment style of your choice, depending on the host language. See the example above.<br>In the target file, put on a separate line snippetysnipp:FILE_NAME:SNIPPET_NAME<br>Paths can be relative or absolute. See the example above, or python/SnippetySnip/integration_tests/ for a more extensive one.<br>To run SnippetySnip and insert all your snippets, do ":call SnippetySnip()" without the quotes. It is suggested you make a map to make this easier, see INSTALLATION<br>It can be tedious to type the snippet inclusion strings in the target file. To get this string automatically, go to your source file, place the cursor inside the snippet you want the inclusion string for and do ":call SnippetySnipPrintCurrentSnippetString()" without the quotes. It will print the string for you (with html comments). It is suggested you make a map to make this easier, see INSTALLATION.<br>About cursor position: Since the entire buffer is replaced, it is impossible to exactly remember the cursor position. SnippetySnip does however make a best effort, and puts the cursor back at the same line and column.<br><br><br><br>INSTALLATION<br>Download SnippetySnip-&lt;version&gt;.tgz into your vim directory (usually ~/.vim on Linux systems) and do<br>tar zxf SnippetySnip-&lt;version&gt;.tgz<br><br>To make SnippetySnip easier to use, I suggest you set up a mapping in ~/.vimrc. These should do nicely:<br>map &lt;Leader&gt;s :call SnippetySnip()&lt;CR&gt; "Type &lt;Leader&gt;s to run SnippetySnip<br>map &lt;C-s&gt; :call SnippetySnip()&lt;CR&gt; "Press Ctrl+s to run SnippetySnip<br>map &lt;Leader&gt;S :call SnippetySnipPrintCurrentSnippetString()&lt;CR&gt; "Type &lt;Leader&gt;S to print the name of the current snippet<br><br><br><br>ADVANCED USAGE<br>It is possible to give arguments before="..." and/or after="..." when including a snippet. These arguments will be inserted between the snippet command and the actual snippet. Here is an example, inserting code snippets into Wordpress posts:<br><br>original target:<br>&#160;&#160;&#160;&#160;&lt;!-- snippetysnip:source.cpp:foo:(before='[sourcecode language="cpp"]', after='[/sourcecode]') --&gt;<br><br>modified target:<br>&#160;&#160;&#160;&#160;&lt;!-- snippetysnip:source.cpp:foo:(before='[sourcecode language="cpp"]', after='[/sourcecode]') --&gt;<br>&#160;&#160;&#160;&#160;[sourcecode language="cpp"]<br>&#160;&#160;&#160;&#160;imported();<br>&#160;&#160;&#160;&#160;code();<br>&#160;&#160;&#160;&#160;here();<br>&#160;&#160;&#160;&#160;[/sourcecode]<br>&#160;&#160;&#160;&#160;&lt;!-- snippetysnip_end:source.cpp:foo --&gt;<br><br>(This was motivated by the fact that html-comments do not work well inside [sourcecode] blocks on Wordpress)<br><br>If you want SnippetySnipPrintCurrentSnippetString() to include arguments, define g:SnippetySnipArguments. For instance, you can put this line in ~/.vimrc:<br>let g:SnippetySnipArguments = "(before='[sourcecode language=\"cpp\"]', after='}[/sourcecode]')"<br><br>SnippetySnip is also available on github: <a target="_blank" href="https://github.com/knatten/SnippetySnip">https://github.com/knatten/SnippetySnip</a><br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>INSTALLATION<br>Download SnippetySnip-&lt;version&gt;.tgz into your vim directory (usually ~/.vim on Linux systems) and do<br>tar zxf SnippetySnip-&lt;version&gt;.tgz<br><br>To make SnippetySnip easier to use, I suggest you set up a mapping in ~/.vimrc. These should do nicely:<br>map &lt;Leader&gt;s :call SnippetySnip()&lt;CR&gt; "Type &lt;Leader&gt;s to run SnippetySnip<br>map &lt;C-s&gt; :call SnippetySnip()&lt;CR&gt; "Press Ctrl+s to run SnippetySnip<br>map &lt;Leader&gt;S :call SnippetySnipPrintCurrentSnippetString()&lt;CR&gt; "Type &lt;Leader&gt;S to print the name of the current snippet</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3717"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3717">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18826">SnippetySnip-0.6.tgz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></i></td>
    <td class="rowodd" valign="top" width="2000">Fixed bug that would match a snippet named "fooMoreChars" when searching for snippet "foo"<br>Snippet names are now restricted to the letters a-z/A-Z, numbers, or any of the characters "-_." (or if you prefer a regex: /[0-9a-zA-Z\-_\.]+/)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18163">SnippetySnip-0.5.tgz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-06-19</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></i></td>
    <td class="roweven" valign="top" width="2000">Add SnippetySnipPrintCurrentSnippetString() to print the inclusion string for the snippet currently under the cursor<br>Make a best effort to preserve cursor position</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17844">SnippetySnip-0.4.tgz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-05-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></i></td>
    <td class="rowodd" valign="top" width="2000">Put a line of whitespace at the beginning and end of each snippet for improved readability</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16743">SnippetySnip-0.3.tgz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-10-26</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></i></td>
    <td class="roweven" valign="top" width="2000">It is now possible to give arguments before="..." and/or after="..." when including a snippet. These arguments will be inserted between the snippet command and the actual snippet.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16412">SnippetySnip-0.2.tgz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-08-31</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></i></td>
    <td class="rowodd" valign="top" width="2000">Improved README with suggestions for keymaps</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16399">SnippetySnip-0.1.tgz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-08-29</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10364">Anders Schau Knatten</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  