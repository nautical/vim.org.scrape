<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">htmlmap : macros to help type HTML entities</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>21/8</b>,
    Downloaded by 1774    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:258">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=579">Antoine Mechelynck</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>This script defines a number of macros, all for insert mode in HTML files. Once installed, it will automatically "translate" characters into their &amp;...; strings: so for instance if you type "&#228;" the characters " &amp; a u m l ; " will be inserted (without the intervening spaces). A mechanism similar to Vim's "digraphs" is also supported but with another key (F12, but you can replace it with something else if it doesn't suit you), so that e.g. "&lt;F12&gt;oe" inserts the numeric code for the oe ligature (above 255, some browsers know the numeric codes but not the symbolic entities such as &amp;oelig;).<br><br>The script contains a lot of comments to make it largely self-explanatory.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Add the line<br><br>&#160;&#160;&#160;&#160;source &lt;path&gt;/htmlmap.vim<br><br>(where &lt;path&gt; is the directory where you downloaded this script) to the file ~/.vim/ftplugin/after/html.vim (for Unix) or ~/vimfiles/ftplugin/after/html.vim (for Windows). Create the file and the directories if they don't yet exist.<br><br>Optionally, add<br><br>&#160;&#160;&#160;&#160;imap &lt;F12&gt; &lt;C-K&gt;<br><br>to your vimrc if you want to use the same key as a digraph prefix for entities in HTML and for characters in other files.<br><br>Note that if your vim does not support autocommands you can still make this script work by sourcing it manually in the buffer(s) where you need it.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="258"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=258">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=1689">htmlmap.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.01pl4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2003-02-02</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1436">Ingo Lantschner</a></i></td>
    <td class="rowodd" valign="top" width="2000">Euro-Sign added.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=1336">htmlmap.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.01pl3</b></td>
    <td class="roweven" valign="top" nowrap><i>2002-10-24</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=579">Antoine Mechelynck</a></i></td>
    <td class="roweven" valign="top" width="2000"> - Bugfix for some rare Cyrillic characters<br> - Bugfix for circumflex-accented o and O<br> - Improvement: Added map for non-breaking space (but commented-out by default)<br> - Cosmetic improvement: Release history added.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=737">htmlmap.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.01pl1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2002-03-31</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=579">Antoine Mechelynck</a></i></td>
    <td class="rowodd" valign="top" width="2000">Bugfix for Cyrillic uppercase&#160;&#160;io, dje, gje and Ukrainian ie (was 1024-1027, should be 1025-1028)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=734">htmlmap.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.01</b></td>
    <td class="roweven" valign="top" nowrap><i>2002-03-30</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=579">Antoine Mechelynck</a></i></td>
    <td class="roweven" valign="top" width="2000">bugfix = incorrect output for &lt;F12&gt;B= ; &lt;F12&gt;V= not recognized (Cyrillic uppercase B and V)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=727">htmlmap.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2002-03-29</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=579">Antoine Mechelynck</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  