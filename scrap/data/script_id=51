<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">cscope_macros.vim : basic cscope settings and key mappings</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>112/40</b>,
    Downloaded by 5182    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:51">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=81">Jason Duell</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>This script implements the recommended settings for vim's cscope interface, and also provides a set of key mappings I've found useful.&#160;&#160;If you don't know what cscope is, go to <a target="_blank" href="http://cscope.sourceforge.net">http://cscope.sourceforge.net</a> and check it out--it's more powerful than ctags, though it works with fewer languages (primarily C/C++/Java).<br><br>The script takes care of automatically loading any cscope database you have in the current directory, or that you have set the $CSCOPE_DB environment variable to point to.&#160;&#160;It also tells vim to allow cscope to share the CTRL-]&#160;&#160;(goto tag) command with ctags.&#160;&#160;Finally, it creates a set of keyboard maps (all starting with CTRL-spacebar) that perform the various types of cscope search, and which can either display cscope's results in a new vim window or in the current window.<br><br>--------------------------------------------------<br>A quick vim/cscope tutorial:<br>--------------------------------------------------<br><br>0) Get and install cscope if you don't have it already on your machine.<br><br>1) Install this plugin (see instructions below).<br><br>2) Go into a directory with some C code in it, and enter 'cscope -r'.&#160;&#160;Check out the native cscope interface program (which will call vim to edit files if you set your EDITOR variable to 'vim').&#160;&#160;The native cscope interface is very nice, but has the problem that you need to exit vim each time you want to do a new search.&#160;&#160; Hit CTRL-D to exit cscope.<br><br>3) Start up vim.&#160;&#160;If you want, you can start it with a C symbol (ex: 'vim -t&#160;&#160;main'), and you should hop right to the definition of that symbol in your code.<br><br>4) Put the cursor over a C symbol that is used in several places in your program.&#160;&#160;Type "CTRL-spacebar s" in quick succession, and you should see a menu showing you all the uses of the symbol in the program.&#160;&#160;Select one of them and hit enter, and you'll jump to that use.&#160;&#160;As with ctags, you can hit CTRL-T to jump back to your original location before the search (and you can nest searches and CTRL-T will unwind them one at a time).<br><br>5) Try the same search, but this time via "CTRL-spacebar h s".&#160;&#160;This time, the window will split in two horizontally, and the cscope search result will be put in the new window. [If you have trouble hitting the keys fast enough for this to work, go into the cscope_macro.vim script and change vim's timeout settings as described in the comments.]&#160;&#160;<br><br>6) Try splitting the window vertically, by using 'v' instead of 'h' in the above search.<br><br>7) Explore some of the other cscope search types, like 'find file' and 'find functions that call the function under the cursor'.&#160;&#160;These are all described in the comments in cscope_macros.vim.<br><br>8) Try using cscope with another language, like Java or C++.&#160;&#160;The best way to do this is by doing "ls *.java &gt; cscope.files"&#160;&#160;(or "find -name '*.java' &gt; cscope.files" if you've got a directory tree of Java files), then running cscope.<br><br>9) Try setting the $CSCOPE_DB environment variable to point to a cscope database you create, so you won't need to lauch vim in the same directory as the database. This is particularly useful for projects where code is split into multiple subdirectories.&#160;&#160;(Note: for this to work, you should build the database with absolute pathnames: cd to /, and do "find /my/fancy/project -name '*.c' -o -name '*.h' &gt; /my/cscope.files", then run cscope in the same directory as the cscope.files file, then set and export the $CSCOPE_DB variable, pointing it to the cscope.out file that results).<br><br>10) Use ":help cscope" within vim, and/or "man cscope" from your shell to answer any questions you have.&#160;&#160;I.e., RTFM.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>vim 6:&#160;&#160;&#160;&#160; Stick this file in your ~/.vim/plugin directory (or in a<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'plugin' directory in some other directory that is in your<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'runtimepath'.<br><br>vim 5:&#160;&#160;&#160;&#160; Stick this file somewhere and call 'source cscope_macros.vim' from<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;your ~/.vimrc file (or cut and paste cscope_macros.vim into your .vimrc).<br></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="51"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=51">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=171">cscope_macros.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.0.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2001-09-14</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=81">Jason Duell</a></i></td>
    <td class="rowodd" valign="top" width="2000">Hmm, backslashes don't seem to show up when I type them on the upload form.&#160;&#160;What I meant to write in that last post was that the new default mapping for regular cscope searches is 'CTRL-\'&#160;&#160;(Control-backslash).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=170">cscope_macros.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2001-09-14</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=81">Jason Duell</a></i></td>
    <td class="roweven" valign="top" width="2000">I've changed the default mappings in the script, to avoid 3-key maps.&#160;&#160;Now it uses 'CTRL-' to do basic (no window split) cscope searches, 'CTRL-spacebar' to do searches that display results in a new, horizontally split window, and 'CTRL-spacebar CTRL-spacebar' to do searches with a vertical split.&#160;&#160;I've also trimmed my overly verbose comments in the file.<br><br>Since I can't update the description text on this site, I'm going to be keeping the latest tutorial on vim-cscope usage at <a target="_blank" href="http://cscope.sourceforge.net">http://cscope.sourceforge.net</a> from now on.&#160;&#160;I'll be posting a new version of the tutorial there soon.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=73">cscope_macros.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2001-07-30</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=81">Jason Duell</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  