<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">TeX-PDF : Lightweight "stay-out-of-your-way" TeX-to-PDF development support</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>34/12</b>,
    Downloaded by 860    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3230">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>ftplugin</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Description<br>===========<br><br>This plugin provides support for specialized lightweight "stay-out-of-your-way"<br>TeX-to-PDF compilation and viewing.&#160;&#160;In contrast to some of the other more<br>comprehensive TeX/LaTeX suites, it provides nothing else but two commands, and<br>leaves the rest of your Vim platform undisturbed, except for a few key maps to<br>invoke the commands.<br><br>Features<br>========<br><br>&#160;&#160;&#160;&#160;- Runs "make" if a Makefile is found, otherwise invokes Rubber (if<br>&#160;&#160;&#160;&#160;&#160;&#160;installed) or pdflatex (otherwise) on the current file.<br>&#160;&#160;&#160;&#160;- Successful builds will result in PDF being opened/refreshed in the<br>&#160;&#160;&#160;&#160;&#160;&#160;default external viewer.<br>&#160;&#160;&#160;&#160;- Unsuccessful builds will result in the QuickFix buffer being opened and<br>&#160;&#160;&#160;&#160;&#160;&#160;to list the errors, with line numbers, description etc.<br><br>Usage<br>=====<br>Once the plugin is installed (see below), when you have a TeX or LaTeX document<br>open in the buffer, type '\r' or &lt;Shift-F9&gt; to compile the document and open<br>(or refresh) the resulting PDF in your system's default PDF viewer. To just<br>compile the document to PDF without opening it, type '\m' or &lt;F9&gt;.&#160;&#160;If there<br>are any compile errors, a window will open and list all the problematic lines.<br>As usual, you can navigate forward and backward through this list using the<br>':cnext' and ':cprev' commands, and you will automatically be taken to the<br>corresponding line in the source document.<br><br>Working with Rubber<br>===================<br><br>This plugin will work quite happily with the stock TeX/LaTeX tools found on<br>most systems. As described above, for example, this plugin will run "make" if a<br>Makefile is found in the current working directory (by-passing Rubber, even if<br>installed), and fall back to the standard "pdflatex" if Rubber is not<br>installed.<br><br>However, installation of "Rubber" (<a target="_blank" href="https://launchpad.net/rubber">https://launchpad.net/rubber</a>) in conjunction<br>with this plugin makes Vim a *very* powerful, yet highly ergonomic and<br>non-intrusive, LaTeX IDE, making Makefiles unneccessary for many document<br>projects. Rubber takes care of the multiple TeX compile passes required to get<br>the the bibliographies, references, cross-references, etc. correctly built,<br>while this plug-in takes care of opening/refreshing the PDF view on successful<br>builds, as well as providing for a *clean* view of the errors on an<br>unsuccessful build.<br><br>Operation<br>=========<br><br>This plugin provides two commands: "BuildAndViewTexPdf" and "BuildTexPdf".<br><br>"BuildAndViewTexPdf" will build the PDF from the TeX source, and, if<br>successful, will open the resulting PDF for viewing in an external viewer (or<br>refresh the PDF if already opened).<br><br>"BuildTexPdf" will build the PDF from the TeX source without opening/refreshing<br>the PDF.<br><br>The build logic is:<br><br>&#160;&#160;&#160;&#160;(1) If a Makefile is found in the current directory, then "make" will be<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;invoked.<br>&#160;&#160;&#160;&#160;(2) If there is no Makefile in the current directory, but "Rubber"<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;(<a target="_blank" href="https://launchpad.net/rubber">https://launchpad.net/rubber</a>) is installed and available, then this will<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;invoked on the current buffer file.<br>&#160;&#160;&#160;&#160;(3) If Rubber is not installed, then "pdflatex" will be invoked on the<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;current buffer file.<br><br>If the build is unsuccessful, the QuickFix buffer is populated with the<br>(parsed, cleaned, and filtered) error messages and automatically opened,<br>showing, for each error, the line number and description of the error. You can<br>then used ":cnext", ":cprev", etc. to visit each of the error lines.<br><br>Key Mapping<br>===========<br><br>By default, '&lt;Leader&gt;r' (mnemonic: 'run') and '&lt;S-F9&gt;' are mapped to<br>"BuildAndViewTexPdf", while '&lt;Leader&gt;m' (mnemonic: 'make') and '&lt;F9&gt;' are<br>mapped to "BuildTexPdf"<br><br>If you prefer not to have any of the above keys mapped, then include the<br>following in your '~/.vimrc':<br><br>&#160;&#160;&#160;&#160;let g:tex_pdf_map_keys = 0<br><br>If you prefer not to have the '\r' and '\m' leader keys mapped but keep the<br>function key mapping, you can include the following in your '~/.vimrc':<br><br>&#160;&#160;&#160;&#160;let g:tex_pdf_map_leader_keys = 0<br><br>Alternatively, you can keep the leader keys mapped but by-pass the function key<br>mapping by including the following in your '~/.vimrc':<br><br>&#160;&#160;&#160;&#160;let g:tex_pdf_map_func_keys = 0<br><br>You can, of course, map any keys that you want to the commands. For example,<br>by including the following in your '~/.vimrc', you will map Command-V to the<br>"compile and view" command and Command-T to the "just compile" command in<br>both normal and insert modes:<br><br>&#160;&#160;&#160;&#160;noremap &lt;silent&gt; &lt;D-V&gt; &lt;Esc&gt;:BuildAndViewTexPdf&lt;CR&gt;<br>&#160;&#160;&#160;&#160;inoremap &lt;silent&gt; &lt;D-V&gt; &lt;Esc&gt;:BuildAndViewTexPdf&lt;CR&gt;<br>&#160;&#160;&#160;&#160;noremap &lt;silent&gt; &lt;D-T&gt; &lt;Esc&gt;:BuildTexPdf&lt;CR&gt;<br>&#160;&#160;&#160;&#160;inoremap &lt;silent&gt; &lt;D-T&gt; &lt;Esc&gt;:BuildTexPdf&lt;CR&gt;<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Unarchive the file while in your Vim home directory: this will result in&#160;&#160;the bundled file, "tex_pdf.vim" being copied to "~/.vim/ftplugin/tex". Alternatively, you can just unpack and copy the file to this location yourself manually.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3230"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3230">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18261">tex_pdf.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.41</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-07-14</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="rowodd" valign="top" width="2000">Restore header guard</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18260">tex_pdf.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-07-14</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="roweven" valign="top" width="2000">- Jump to first error line on previous window on error </td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13845">tex_pdf.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.3A</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-09-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="rowodd" valign="top" width="2000">Correct compression protocol used (previous upload was actually bzip2).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13793">tex-pdf.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-09-09</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="roweven" valign="top" width="2000">Fixed failure to redraw in console Vim when running external make/rubber.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13785">tex-pdf.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-09-08</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="rowodd" valign="top" width="2000">On successful compiles, cursor is restored to original position. On compile error, smarter jumping to location of first error.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13782">tex-pdf.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-09-08</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="roweven" valign="top" width="2000">Better handling of compile errors, automatically jumping to first error line.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13775">tex-pdf.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-09-08</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=19500">Jeet Sukumaran</a></i></td>
    <td class="rowodd" valign="top" width="2000">Re-upload.</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  