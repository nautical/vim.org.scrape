<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">VimChatBot : A simple self-teaching chat bot game for Vim</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>4/1</b>,
    Downloaded by 192    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:4264">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>game</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Agetian's Vim ChatBot (C) 2012 by Michael Kamensky, all rights reserved.<br>This script is distributed under Vim license.<br>==============================================================================<br><br>Vim ChatBot is a simple self-teaching chat bot game for Vim. It will try to<br>communicate with the human and will accumulate the human's questions and answers<br>over time, utilizing them later and trying to mimic human behavior. This chat<br>bot is not meant to be smart, at best it resembles a talking parrot and it's far<br>from being a real human-like chat robot companion. It's meant to be fun and it<br>was originally intended as a little project to teach myself how to script Vim.<br><br>IMPORTANT -- READ BEFORE USING OR DON'T TELL ME YOU WERE NOT WARNED:<br>====================================================================<br><br>Vim ChatBot will use the currently open file in the buffer as a source and as a<br>target for its database of known questions and replies! It will automatically<br>append new lines to the end of the current file as it learns the phrases from<br>the human! Therefore, if you were working on some important document prior to<br>starting the chat bot, make sure you switch to a new document or open your chat<br>database before initiating a conversation.<br><br>Vim ChatBot will never automatically save the file for you though, and it's<br>always up to you to decide if you want to save the updated database or not. You<br>should do it explicitly using the :write (or another command) to save the file. <br><br>Vim ChatBot utilizes the concept of a here-document (similar to here-documents<br>in bash) to be able to store its database of known questions and replies in the<br>script file itself. When VimChatBot.vim is opened in Vim, the chat bot will<br>automatically append new entries to the end of the script file as it learns new<br>phrases from you, and it will not corrupt the script file. If you'd like to keep<br>the updated database, you need to make sure you save the script file (:w or :x)<br>after you're done chatting.<br><br>Starting the Vim ChatBot<br>========================<br><br>By default, Vim ChatBot maps itself to the &lt;Leader&gt;Cb command in normal mode<br>(&lt;Leader&gt; is the backslash symbol by default, so unless you've changed your<br>leader, the default command to start Vim Chatbot is \Cb ).<br><br>If the &lt;Leader&gt;Cb command is already taken by something among your scripts, feel<br>free to change the mapping (just search for nnoremap in the script file and<br>change the mapping to another combination of keys).<br><br>Macros<br>======<br><br>You can use the following macros when typing answers, the bot will also utilize<br>them when learning to use the phrase from you:<br><br>$TIME$ - refers to the current time.<br>$TIME12$ - (experimental) refers to the current time in 12-hour format.<br>$DAY$ - refers to the current day.<br>$WEEKDAY$ - refers to the current day of the week.<br>$MONTH$ - refers to the current month.<br>$YEAR$ - refers to the current year.<br><br>Finishing the conversation<br>==========================<br><br>To finish the conversation, type /Q into the "You Say:" prompt. Unless you are<br>asked something (or told something) by the bot and it's waiting for you to teach<br>it how to reply to that, /Q will end the conversation immediately. If, however,<br>the bot is waiting for your reply to learn something from you, /Q will merely<br>inform the bot that you don't have the intention to answer the question. If you<br>want to quit the conversation altogether, you will need to type /Q again.<br><br>Ctrl+C can be used as the universal quit command if you want to quit chatting<br>when in the middle of a bot-asked question and when you don't feel like typing<br>/Q twice.<br><br>Magical contexts<br>================<br><br>By default, Vim ChatBot treats the first two responses as special, that is, the<br>bot will store a separate set of responses for the phrases you type as your<br>first and as your second lines, and will respond accordingly. This is to prevent<br>absolute stupidity, such as saying "Hi!" in the middle of a conversation. The<br>number of "magical contexts" (responses which should be treated specially) can<br>be modified and is stored in the s:MagicalContexts variable.<br><br>Database Modification<br>=====================<br><br>You can modify the database manually by adding or removing phrases in case you<br>are not satisfied with something that the bot has learned or would like to teach<br>it something without having to go through the process of teaching-by-chatting. <br><br>The database for Vim ChatBot is stored in the following way:<br><br>&lt;Phrase&gt;:::[MagicalContextID]<br>&lt;Response1&gt;<br>&lt;Response2&gt;<br>...<br>&lt;BLANK_LINE&gt;<br><br>Quirks<br>======<br><br>The database structure described above has an important consequence: when<br>speaking to Vim ChatBot, never use a triple colon (:::) symbol in your phrases,<br>they will confuse the bot and may make it behave more stupidly than normal.<br><br>In the beginning of the conversation, the bot might seem not to recognize some<br>of the phrases you taught it before - this is due to the magical contexts<br>kicking in, the bot hasn't yet been taught to recognize that specific phrase in<br>that particular position. This is normal behavior, just patiently repeat the<br>lesson.<br><br>If you have a Python-enabled version of Vim, ChatBot will utilize the Python<br>random number generation algorithm from the "random" library. If your Vim is not<br>Python-enabled, a simpler algorithm will be utilized instead, based on the idea<br>suggested by Bee-9 at:<br><a target="_blank" href="http://vim.1045645.n5.nabble.com/simple-16-bit-random-number-generator-td2843842.html">http://vim.1045645.n5.nabble.com/simple-16-bit-random-number-generator-td2843842.html</a><br>This algorithm is less efficient and will produce less random numbers, which may<br>or may not be apparent in the actual conversation with the bot (your mileage may<br>vary). At any rate, a Python-enabled Vim is recommended for more randomness, but<br>not strictly required.<br><br>==============================================================================<br>Remember: Don't take Vim ChatBot seriously and just have fun! :)<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Either put the file VimChatBot.vim to your "plugin" folder so it's auto-loaded, or unpack to any folder you like and then :source the script file when necessary.<br><br>Before initiating the chat bot conversation, make sure you either:<br>1) have the script file itself open in the current buffer (in this case, the here-document database in the end of VimChatBot.vim will be used);<br>2) have your own chat database file open in the current buffer (should initially be an empty file, will then be auto-filled with known questions and replies by the chat bot);<br>3) have a new empty file open in the current buffer (in case you don't yet have a chat bot database and you don't want to use VimChatBot.vim as a source/target for the database).<br><br>Once ready, use the &lt;Leader&gt;Cb command in normal mode (by default) to initiate a conversation.<br>Make sure you've read and understood all the information in the detailed description before starting!<br></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="4264"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=4264">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19228">VimChatBot_v17.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-12-21</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Made one of the chat bot's inquiries less confusing to the user.<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18806">VimChatBot_v16.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.6</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-10-14</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="roweven" valign="top" width="2000">- Auto set the regular expression search to ignore case mode prior to initiating the chat (to make it case-insensitive no matter what settings the user has), then restore the case-sensitivity setting after the chat is over.<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18805">VimChatBot_v15.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-14</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Added a possibility of the chat bot initiating the conversation first (has a random 50% chance of happening).<br>- Some minor refactoring<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18794">VimChatBot_v14.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-10-13</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="roweven" valign="top" width="2000">- Added support for macros $HOUR$, $MINUTE$, and $SECOND$ referring to the individual components (hours, minutes, seconds) of current time.<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18791">VimChatBot_v13.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Fixed a bug with the chat initiation always mapping to \Cb instead of &lt;Leader&gt;Cb<br>- Less global namespace pollution due to the use of local functions<br>- Added a Vim version check, currently set to v7.0 (not sure if this script would run on Vim below v7.0, don't have an older build to test on).<br>- Added a load-only-once check.<br>- Added some basic documentation to the script file itself.<br>- Changed the name of the function MainChatLoop to VCB_MainChatLoop, following the naming scheme of other functions.<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18790">VimChatBot_v12b.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2b</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="roweven" valign="top" width="2000">- Some more code cleanup, added the proper header to the script, added the vi compatibility mode check/workaround. Functionally the same as v1.2a. </td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18786">VimChatBot_v12a.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2a</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="rowodd" valign="top" width="2000">Code Cleanup: Gave the functions more unique names, hopefully this will ensure much better compatibility with other scripts.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18783">VimChatBot_v12.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="roweven" valign="top" width="2000">- Code cleanup: removed the unnecessary "s:" flags for most variables that just need to be local to their functions. No other functional difference from the previous version.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18781">VimChatBot_v11.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Added some basic macro functionality, you can now use $TIME$, $DAY$, $WEEKDAY$, $MONTH$, and $YEAR$ to refer to the current time/date while speaking. Check the README for details.<br>- Fixed a potential crash-prone spot in one of the functions. </td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18779">VimChatBot_v10a.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0a</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="roweven" valign="top" width="2000">- Updated the README file with some additional information on the possible quirks in behavior, please read the "Quirks" section of the updated file.<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18777">VimChatBot_v10.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-10-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=55995">Michael Kamensky</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  