<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">Limp : Lisp IDE</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>180/54</b>,
    Downloaded by 4828    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2219">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=14305">Mikael Jansson</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Limp's goal is to provide a compelling alternative to Slime for Vim. It will let you send s-expressions to a running Lisp.<br><br>It will do this for you:<br><br>* booting/attaching/detaching a Lisp (SBCL) from Vim or a command-line script, optionally specifying a core;<br>* send code to the attached Lisp (and limited introspection, see Help Describe);<br>* automatically close brackets;<br>* ease s-exp navigation and (some) transposing;<br>* highlight current form under the cursor;<br>* lookup documentation from the HyperSpec;<br>* complete (some) names.<br><br>Quickstart (better version with screenshots in the documentation):<br><br>In a file with "filetype=lisp" set, hit &lt;F12&gt; to invoke Limp and start a Lisp or connect to an existing.<br>Write a piece of code, e.g.:<br><br>&#160;&#160;(defun hello-world (who)<br>&#160;&#160;&#160;&#160;(format t "Hello, ~A!~%" who))<br><br>Place the cursor at any location inside the code and type &lt;Leader&gt;et (Evaluate Top) to send your code to the running Lisp.<br><br>Hit &lt;F12&gt; again to see the results!&#160;&#160;Then, by placing the cursor at hello-world and typing &lt;Leader&gt;hd (Help Describe) followed by &lt;F12&gt;, you'll see information about the symbol!<br><br>(Full list of key bindings at <a target="_blank" href="http://mikael.jansson.be/hacking/limp/docs/#keyboard-reference">http://mikael.jansson.be/hacking/limp/docs/#keyboard-reference</a> )<br><br>To test it, type<br><br>&#160;&#160;:Eval (hello-world '|Limp User|)<br><br>followed by &lt;Enter&gt; and &lt;F12&gt;.<br><br>More info in the documentation! <br>Uses code from ViLisp (<a href="/scripts/script.php?script_id=221">vimscript #221</a>), AutoClose (<a href="/scripts/script.php?script_id=1849">vimscript #1849</a>), HiMtchBrkt (<a href="/scripts/script.php?script_id=1435">vimscript #1435</a>)</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>See <a target="_blank" href="http://mikael.jansson.be/hacking/limp/docs">http://mikael.jansson.be/hacking/limp/docs</a> or the included documentation for complete installation instructions.<br><br>Author's setup: Linux/Ubuntu-7.04, SBCL-1.0.x, GVim/Vim-7.1 (make sure to install a Vim with +eval, i.e. `sudo aptitude install vim-full`)<br><br>Issue tracker and roadmap at <a target="_blank" href="http://mikael.jansson.be/hacking/limp">http://mikael.jansson.be/hacking/limp</a><br>Repository at <a target="_blank" href="https://svn.jansson.be/public/limp">https://svn.jansson.be/public/limp</a></td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2219">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=8771">limp-0.3.4.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.3.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-06-08</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14305">Mikael Jansson</a></i></td>
    <td class="rowodd" valign="top" width="2000">Mostly bugfix release, with some extra candy.<br>Fixed bugs: #15 (mappings set twice when navigating Lisp buffers); #16 (more robust startup procedure), #19 (eval working on unsaved buffers).<br>New features: #20 (proper installer), #21 (s-exp navigation and (experimental) transposing).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=8720">limp-0.3.3.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-05-24</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14305">Mikael Jansson</a></i></td>
    <td class="roweven" valign="top" width="2000">* CompileFile/CompileAndLoadFile now saves the file before telling Lisp to load it (#4).<br>* New command, :Eval, same functionality as \ex.<br>* Easier keyboard configuration (keys.vim)<br>* It is now a Lisp filetype plugin, so Limp will start automatically for Lisp files. (set filetype=lisp)<br>* Configurable location of SBCL in lisp.sh<br>* Updated documentation about Cmd for Fn-keys in OS X</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=8617">limp-0.3.2.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.3.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-04-29</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14305">Mikael Jansson</a></i></td>
    <td class="rowodd" valign="top" width="2000">* 2008-04-28 by Mikael Jansson &lt;mail@mikael.jansson.be&gt;<br><br>&#160;&#160;+ **ADDED**: TODO file in the distro.<br>&#160;&#160;+ *FIXED*: Last occurance of "Lim" found in the HyperSpec helper. Now<br>&#160;&#160;&#160;&#160;documentation should work properly.<br>&#160;&#160;+ TWEAK: Only change colorscheme and nocompatible when not previously set.<br><br>* 2008-04-27 by Mikael Jansson &lt;mail@mikael.jansson.be&gt;<br><br>&#160;&#160;+ *FIXED*: Connect-or-boot, without a name given to boot.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=8609">limp-0.3.1.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-04-27</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14305">Mikael Jansson</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  