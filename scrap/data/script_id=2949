<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">The Max Impact Experiment : A playground to see how much we can do with pithy code</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>16/10</b>,
    Downloaded by 338    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2949">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=801">David Larson</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>The other day I was sitting around drinking beer and thinking, "Gosh, I hate how long it takes for the list of buffers to show up on the screen when I type :ls. I wish there was a built-in command that quickly opens a window with a list of buffers. Like a :bopen command that works just like the :copen and :lopen commands..." Then it hit me. Yes, the Frisbee hit me right on the melon and I had to get off my lawn chair to throw it back. When I sat back down I had an idea... The buffer list is a perfect use of the location list! Its got file names... even line numbers! Well, I took out the laptop that I was laying on, brushed off the sand and experimented with it. So... How much work did it take to create a location list containing buffers? Precious little it turns out... <br><br>Here's the jist of it... <br><br>" 1. place the output of the ls command into a variable... <br>&#160;&#160; redir =&gt; out <br>&#160;&#160; silent ls <br>&#160;&#160; redir END <br><br>" 2. split the string into an array... <br>&#160;&#160; let l = split(out, '\n') <br><br>" 3. set the error format to conform to the buffer list format... <br>&#160;&#160; set efm=%m\"%f\"%*\\sline\ %l <br><br>" 4. create a location list based on the list of buffers, then open the window... <br>&#160;&#160; lgetexpr l <br>&#160;&#160; lopen <br><br>Blam! That's it! Now I've got a window that lists buffers, and selecting one of them will take me right to it! Granted, it's raw. But it works and it only took 7 lines of code! <br><br>Then I had another loathsome thought as I was rubbing the bump on my forehead... There are many pretty, feature-rich buffer scripts out there. So what use is this? <br><br>Now, either I had too much to drink or the Frisbee hit me harder that I thought because my darth-vader pug (<a target="_blank" href="http://bboylimping.files.wordpress.com/2009/05/darth-vader-dog-costume.jpg">http://bboylimping.files.wordpress.com/2009/05/darth-vader-dog-costume.jpg</a>) started arguing with me. He said, "Because you are using the built-in location list, vim will take care of buffer selection for you, even window management, and all the location list commands are at your disposal!" <br><br>Hoping that nobody could see me arguing with Darthy I said, "Yea, so what? There are other scripts out there that have a much prettier buffer list, have more features..." <br><br>"Shut up!&#148; he said, interrupting me. "It isn't about that. It's about doing as much as you can with as little code as possible." <br><br>Knowing that he was right and humiliated that my dog beat me in an argument, I threw the Frisbee as hard as I could and hoped that he would leave me alone. It didn't work. He just sat there and stared at me with a look that said "you're so immature..." <br><br>"Now, post it!" <br>"Hell no!" I said with a scoff. "Who do you think you are? You're a no-talent-ass-clown pug in a Darth-Vader costume! You're not fooling anyone, you know. Oh, and news flash: you missed the Men in Black auditions by 15 years." <br><br>He just stared at me with a judgmental look on his face. Not wanting to give him the satisfaction, I casually turned over with a yawn so he couldn't see me post the script. <br><br>So, here it is... I've added only a *minimum* collection of useful features to the buflist. I've refined the hell out of it; smoothing out the worst of the rough edges. No feature takes more than a few lines of code, the whole thing is only about 70-75 lines. <br><br>Why have this experiment? Well... there are some important advantages to keeping things short: <br>- There are fewer places for bugs to hide. <br>- The code runs considerably faster because it leverages more of the native C code.<br>- It is easier to understand (usually). <br>- It is easier to fix bugs. <br>- A sufficient level of obfuscation adds to job security... and <br>- It saves $0.000000001 in hard-drive space. <br><br>Download it and take a look at it. Play around with it. Adore the elegance of it. Can you think of any other features to add?<br><br>Darthy: "Arff! You can do it!" Aww, crap... he caught me. <br><br>Feel free to contact me with ideas and updates (take a look at my profile for the email). <br><br>For those of you who are expecting the world's best buffer script, then give a negative rating... hey lighten up! This is just a fun exercise! Please add a comment before rating! Comments can be posted on the corresponding wiki page (see link at the top of this page).</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Drop it in your plugin directory.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2949"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2949">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12503">buflist.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2a</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-02-26</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="rowodd" valign="top" width="2000">You can now change the mapping to open the buffer list by setting the buflist_open variable. Added usage documentation.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12369">buflist.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-02-12</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="roweven" valign="top" width="2000">Found that if I use an autocommand to close the window it simplified the script a lot! It is only about 29 lines long now! (ignoring empty lines) </td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12300">buflist.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1aa</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-02-04</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="rowodd" valign="top" width="2000">Bug fix: sometimes shortening a path made it longer. <br>Strengthened my resolve to add only pithy features. (I almost gave in to the temptation of adding custom highlighting that was of little to no use and was several lines long... but I stood strong. Thank you for your prayers.)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12269">buflist.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1a</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-02-01</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="roweven" valign="top" width="2000">Added more comments and obfuscation.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12217">buflist.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-01-28</i></td>
    <td class="rowodd" valign="top" nowrap>7.2</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  