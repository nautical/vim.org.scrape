<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">vim-dict : The Dict client for Vim</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>6/3</b>,
    Downloaded by 167    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:4180">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=53679">Szymon Wrozynski</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>vim-dict<br><br>vim-dict is a dict client. It uses curl to connect to dict servers, so make sure you have curl installed.<br><br>The most recent version can be found on github: <a target="_blank" href="https://github.com/szw/vim-dict">https://github.com/szw/vim-dict</a><br><br>Usage<br><br>To lookup a word (or words) in the dictionary use Dict command:<br><br>:Dict hello<br>:Dict start up<br><br>The Dict command uses hosts and databases defined in the g:dict_hosts global list. By default it is set to [["dict.org", ["all"]]] (the format will be explained a bit later).<br><br>Dict command can use a current word under the cursor. Just move the cursor to a word and type in the command line:<br><br>:Dict<br><br>You can also select words in the visual mode with help of the DictSelection command:<br><br>:DictSelection<br><br>Configuration<br><br>There are just a few global variables (options) you may set in the .vimrc file.<br><br>&#160;&#160;&#160;&#160;g:dict_hosts<br><br>&#160;&#160;&#160;&#160;The most important one is a list g:dict_hosts mentioned earlier. It combines hosts/databases used by vim-dict. The list entries are lists themselves and share the following format:<br><br>&#160;&#160;&#160;&#160;["host_name", ["database1", "database2", ...]]<br><br>&#160;&#160;&#160;&#160;The sample extract from someone's ~/.vimrc file could look like this:<br><br>&#160;&#160;&#160;&#160;let g:dict_hosts = [<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\["dict.org", ["all"]],<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\["dict.mova.org", ["slovnyk_en-pl", "slovnyk_pl-en"]]<br>&#160;&#160;&#160;&#160;\]<br><br>&#160;&#160;&#160;&#160;Moreover vim-dict can help you figure out what databases are available on your servers. There is a special command for this:<br><br>&#160;&#160;&#160;&#160;:DictShowDb<br><br>&#160;&#160;&#160;&#160;You can even open your .vimrc and provide some host urls only:<br><br>&#160;&#160;&#160;&#160;let g:dict_hosts = [<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\["dict.org", []],<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\["dict.mova.org", []]<br>&#160;&#160;&#160;&#160;\]<br><br>&#160;&#160;&#160;&#160;Then save and reload .vimrc, perform DictShowDb and yank-paste the databases you want :).<br><br>&#160;&#160;&#160;&#160;The list of DICT servers can be found on the internet, e.g. here.<br><br>&#160;&#160;&#160;&#160;g:dict_leave_pw<br><br>&#160;&#160;&#160;&#160;If set to 1 vim-dict leaves the preview window (the focus remains on the current window). By default it is set to 0.<br><br>&#160;&#160;&#160;&#160;Example:<br><br>&#160;&#160;&#160;&#160;let g:dict_leave_pw = 0<br><br>&#160;&#160;&#160;&#160;g:dict_curl_command<br><br>&#160;&#160;&#160;&#160;This variable holds the curl command to be fired by Dict function. You will find it handy if curl is not on your $PATH environment variable. By default it is set to "curl".<br><br>&#160;&#160;&#160;&#160;Example:<br><br>&#160;&#160;&#160;&#160;let g:dict_curl_command = "curl"<br><br>&#160;&#160;&#160;&#160;g:dict_curl_options<br><br>&#160;&#160;&#160;&#160;Sometimes you might want to add additional options to the curl invocation, e.g. additonal proxy settings. By default it defines only the connection timeout. Notice, the option -s (silent) is always present regardless of this variable.<br><br>&#160;&#160;&#160;&#160;Example:<br><br>&#160;&#160;&#160;&#160;let g:dict_curl_options = "--connect-timeout 30"<br><br>License<br><br>Copyright &#169; 2012 Szymon Wrozynski. Distributed under the same terms as Vim itself. See :help license<br><br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Place in ~/.vim/plugin/dict.vim or in case of Pathogen:<br><br>cd ~/.vim/bundle<br>git clone <a target="_blank" href="https://github.com/szw/vim-dict.git">https://github.com/szw/vim-dict.git</a><br></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="4180"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=4180">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19289">vim-dict.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-01-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=53679">Szymon Wrozynski</a></i></td>
    <td class="rowodd" valign="top" width="2000">Added vim help file and some minor fixes</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18450">szw-vim-dict-1.2.0.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-08-22</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=53679">Szymon Wrozynski</a></i></td>
    <td class="roweven" valign="top" width="2000">DictSelection is no longer necessary. Now dict command can be performed in visual mode</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18446">szw-vim-dict-1.1.1.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-08-21</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=53679">Szymon Wrozynski</a></i></td>
    <td class="rowodd" valign="top" width="2000">New options (g:dict_curl_options, g:dict_curl_command) and a few more improvements</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18434">szw-vim-dict-1.1.0.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-08-19</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=53679">Szymon Wrozynski</a></i></td>
    <td class="roweven" valign="top" width="2000">Support for multiple hosts/databases. Support for show:db.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18429">szw-vim-dict-1.0.0.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-08-19</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=53679">Szymon Wrozynski</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  