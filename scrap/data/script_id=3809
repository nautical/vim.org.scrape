<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">ipi : Load plugins individually - cut down start-up time.</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>10/4</b>,
    Downloaded by 205    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3809">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=9552">Jan Christoph Ebersbach</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>This plugin is very much inspired by Tim Pope's pathogen plug-in. vim-ipi<br>adds the functionality of loading infrequently used plugins later. This<br>greatly helps in cutting down vim's start-up time.<br><br>Examples:<br>Load gundo plugin later:<br>:IP gundo<br><br>Load gundo and speeddating plugins later:<br>:IP gundo speedating<br><br>Load all known plugins later:<br>:IP!<br><br>A very high level of convenience can be achieved by loading the plug-ins<br>automatically right before using their functionality. Here is an example for<br>the gundo plug-in. I just prefixed the mapping with ":silent! IP gundo&lt;CR&gt;":<br>nmap &lt;leader&gt;u :silent! IP gundo&lt;CR&gt;:GundoToggle&lt;CR&gt;<br><br>Tips:<br>Some plugins use the autocommand VimEnter to do some initialization. ipi<br>has built-in support for a number of plugins that use this feature. A<br>detailed description is provided for the g:ipi_vimenter_autocommand<br>variable.&#160;&#160;In case a plugin is not support yet, manual execution of the<br>autocommand can be done by running the following command:<br>:do &lt;AUTOGROUPNAME&gt; VimEnter<br><br>For the convenience wrapper it would look like this:<br>nmap &lt;leader&gt;f :silent! IP NERDTree&lt;CR&gt;:do NERDTree VimEnter&lt;CR&gt;:NERDTree&lt;CR&gt;</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Install in ~/.vim/autoload (or ~\vimfiles\autoload) together with pathogen<br>(<a href="/scripts/script.php?script_id=2332">vimscript#2332</a>).<br><br>Pathogen's settings can be used to configure ipi as well, e.g. disable<br>plugins (g:pathogen_disabled).<br><br>For loading plugins later, install them in ~/.vim/ipi (or ~\vimfiles\ipi)<br>and&#160;&#160;add `call ipi#inspect()` to .vimrc. This will create a list of plugins<br>that can be loaded later by using the LL command:<br><br>The development version is available at <a target="_blank" href="http://www.github.com/jceb/vim-ipi">http://www.github.com/jceb/vim-ipi</a>.</td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3809">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17078">ipi.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-12-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9552">Jan Christoph Ebersbach</a></i></td>
    <td class="rowodd" valign="top" width="2000">- add support for automatically executing the VimEnter autocommand when<br>&#160;&#160;loading a plugin<br>- rename LL to IP</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16909">ipi.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-11-20</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9552">Jan Christoph Ebersbach</a></i></td>
    <td class="roweven" valign="top" width="2000">- update documentation</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16895">ipi.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-11-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9552">Jan Christoph Ebersbach</a></i></td>
    <td class="rowodd" valign="top" width="2000">- change global variable lp to be script local<br>- add support for a specific path to ipi#inspect<br>- externalize ipi#source<br>- source scripts in ftdetect and after directories as well</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16884">ipi.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-11-16</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9552">Jan Christoph Ebersbach</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  