<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">context_complete.vim : Context sensitive word completion</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>30/15</b>,
    Downloaded by 1295    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1179">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=801">David Larson</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>The focus of this script is to provide context-sensitive word completion - taking advantage of the popular ctags utility and Vim's built-in perl features. <br><br>Suppose you have an instance of an object (o), and you've typed:<br><br>o.set_<br><br>While you are still in insert mode, press CTRL-J, and this script will then look up the first member of that object that starts with "set_" and complete it for you, like this:<br><br>o.set_range(integer max, integer min)<br><br>If this isn't the completion that you want, then press CTRL-J again for the next member that starts with "set_":<br><br>o.set_name(string name)<br><br>and again for the next one:<br><br>o.set_value<br><br>If you've gone too far then you can go back with CTRL-K. After each completion you are left in insert mode - so you can just continue typing when you've found the one you want.<br><br>The object can also be replaced with a struct, class, 'this', or 'super'.<br><br>Completions can also be made after the open parentheses to complete the list of parameters:<br><br>o.set_range(&lt;c-j&gt;<br><br>Or after the function:<br><br>o.get_name().compar&lt;c-j&gt;<br><br>Pressing CTRL-S will skip through the function parameters, visually selecting each in turn - useful for filling in the parameters after completing a function.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Unpack the tar file into ~/.vim. Then from any vim window type :helptags ~/.vim/doc. You can read the installation details at :help context_complete-setup</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="1179"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1179">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=3960">context_complete1.0.tar</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2005-03-04</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="rowodd" valign="top" width="2000">Major upgrade.<br>- context completion continues after a function (var.getObject().&lt;c-j&gt;)<br>- search pattern and keywords are configurable<br>- added a new mapping to highlight parameter items in turn<br>- added a tag cache file for mega-fast searching.<br>- *much* more.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=3848">context_complete.tar</a></td>
    <td class="roweven" valign="top" nowrap><b>0.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2005-02-10</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="roweven" valign="top" width="2000">- completes 'this.&lt;c-q&gt;'<br>- completes 'super.&lt;c-q&gt;'<br>- object type guessing is smarter - limits tag choises to members of the current object.<br>- more bug fixes and documentation.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=3818">context_complete.tar</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2005-01-31</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="rowodd" valign="top" width="2000">- implemented a basic understanding of class inheritance.<br>- tag searching is now *much* faster - uses binary searching whenever possible.<br>- function parameter completions are given when asked after "(". e.g.: util.do_suff(&lt;c-q&gt;<br>- completion suggestions are now sorted.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=3792">context_complete.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2005-01-24</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=801">David Larson</a></i></td>
    <td class="roweven" valign="top" width="2000">- static members are completed: i.e. ClassName::&lt;c-q&gt;<br>- C structs are now understood and treated like classes<br>- local variable definition detection is much better... it uses more of vim's built-in features. (yay!)<br>- more bug fixes. Finding the definition of a local variable is much more solid.</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  