<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">word_complete.vim : automatically offers word completion as you type</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>368/150</b>,
    Downloaded by 13578    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:73">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=92">Benji Fisher</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Each time you type an alphabetic character, the script attempts<br>to complete the current word.&#160;&#160;The suggested completion is selected<br>in Select mode, so you can just type another character to keep going.<br>Other options:&#160;&#160;&lt;Tab&gt; to accept, &lt;BS&gt; to get rid of the completion,<br>&lt;Esc&gt; to leave Insert mode without accepting the completion, &lt;C-N&gt;<br>or &lt;C-P&gt; to cycle through choices, &lt;C-X&gt; to enter &lt;C-X&gt; mode.<br><br>Limitations:<br>The script works by :imap'ping each alphabetic character, and uses<br>Insert-mode completion (:help i_ctrl-p).&#160;&#160;It is far from perfect.&#160;&#160;For example, the :imap's mean that you are constantly switching out of Insert mode, which means that undo only works a few characters at a time.&#160;&#160;This also messes up any abbreviations you may have defined.&#160;&#160;Since Select mode uses the same mappings as Visual mode, the special keys mentioned above may conflict with what you are used to in Visual mode.&#160;&#160;I have received one report that mswin.vim interferes with this script.<br><br>Every second character you type is in Select mode, so with versions of vim before 6.2, completions are offered only half the time.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>:source it from your vimrc file or drop it in your plugin directory.<br>To activate, choose "Word Completion" from the Tools menu, or type<br>&#160;&#160;:call DoWordComplete()<br>To make it stop, choose "Tools/Stop Completion," or type<br>&#160;&#160;:call EndWordComplete()<br>If you want to activate word completion for every buffer, add the line<br>&#160;&#160;:autocmd BufEnter * call DoWordComplete()<br>to your vimrc file.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="73"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=73">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=6504">word_complete.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-12-06</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=92">Benji Fisher</a></i></td>
    <td class="rowodd" valign="top" width="2000">Minor change:&#160;&#160;I replaced<br>&#160;&#160;while letter &lt;= "z"<br>with<br>&#160;&#160;while letter &lt;=# "z"<br>(case-sensitive comparison, regardless of 'ignorecase' option) so that if someone decides to add a loop for "A" to "Z" using the same model, (s)he does not get odd results.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5704">word_complete.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-05-15</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=92">Benji Fisher</a></i></td>
    <td class="roweven" valign="top" width="2000">Since vim 7.0 supports :smap (mappings that apply only to Select mode) I updated the script to use these.&#160;&#160;Now the maps defined by the script for when completion is active will not do anything unexpected in Visual mode.<br><br>I also updated the installation instructions in the comments at the top of the file.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=2218">word_complete.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2003-08-09</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=92">Benji Fisher</a></i></td>
    <td class="rowodd" valign="top" width="2000">I added a configuration section.&#160;&#160;You can now change<br>(1) a minimum length of word to be completed<br>(2) the character (default &lt;Tab&gt;) used to accept the completion.<br><br>I also added some comments and separated a StartAppend() function (which is called twice).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=2048">word_complete.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2003-06-06</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=92">Benji Fisher</a></i></td>
    <td class="roweven" valign="top" width="2000">better support for keymaps</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=1882">word_complete.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2003-03-31</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=92">Benji Fisher</a></i></td>
    <td class="rowodd" valign="top" width="2000">This version tries to treat :lmap's (and keymaps) properly.<br><br>Minor improvement:&#160;&#160;completion is attempted before <br>punctuation, as well as at the end of a line or before white space.<br><br>This version will probably no longer work with vim 5.x.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=143">word_complete.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2001-08-29</i></td>
    <td class="roweven" valign="top" nowrap>5.7</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=92">Benji Fisher</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  