<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">arpeggio : Key mappings for simultaneously pressed keys</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>84/24</b>,
    Downloaded by 1277    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2425">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>*arpeggio* is a Vim plugin to define another type of |key-mapping| called<br>*arpeggio-key-mapping* -- it consists of 2 or more keys ({lhs}) and it will be<br>expanded to another key sequence ({rhs}) whenever all keys in {lhs} are<br>simultaneously pressed.&#160;&#160;For example:<br><br>	Arpeggio inoremap jk&#160;&#160;&lt;Esc&gt;<br>	" OR<br>	call arpeggio#map('i', '', 0, 'jk', '&lt;Esc&gt;')<br><br>With the above definition, you can input &lt;Esc&gt; in Insert mode by pressing "j"<br>and "k" simultaneously, while you can move the cursor by pressing "j" or "k"<br>solely.<br><br>The concept of this plugin is derived from Emacs' key-chord.el (*1), but there<br>are the following differences:<br><br>- Number of keys to be simultaneously pressed is unlimited.<br><br>- Custom delay for each key is supported (see |g:arpeggio_timeoutlens|).<br>&#160;&#160;This is a generalization of space-chord.el (*2).<br><br>(*1) <a target="_blank" href="http://www.emacswiki.org/emacs/key-chord.el">http://www.emacswiki.org/emacs/key-chord.el</a><br>(*2) <a target="_blank" href="http://www.emacswiki.org/emacs/space-chord.el">http://www.emacswiki.org/emacs/space-chord.el</a><br><br><br>Requirements:<br>- Vim 7.2 or later<br><br>Latest version:<br><a target="_blank" href="http://github.com/kana/vim-arpeggio">http://github.com/kana/vim-arpeggio</a><br><br>Document in HTML format:<br><a target="_blank" href="http://kana.github.com/config/vim/arpeggio.html">http://kana.github.com/config/vim/arpeggio.html</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>...</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2425"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2425">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12873">vim-arpeggio-0.0.6.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.0.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-04-20</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Fix |arpeggio-commands| to behave the same as |:map| commands.&#160;&#160;In other words, " and keys after " in {rhs} for |arpeggio-commands| are treated as a part of {rhs}.&#160;&#160;Old versions don't behave so.<br>- Fix minor bugs.<br>- Revise the document a bit.<br>- Revise additional syntax highlighting a bit.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9622">vim-arpeggio-0.0.5.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.0.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-12-06</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="roweven" valign="top" width="2000">- Remove the assumption on 'timeout' and 'ttimeout'.&#160;&#160;Old version<br>&#160;&#160;assumes that "set timeout notimeout", but now their values can be<br>&#160;&#160;arbitrary.<br>- Fix the bug that 'ttimeoutlen' can be interpreted as an unexpected<br>&#160;&#160;value because of the adjustment of 'timeout' and 'timeoutlen' for<br>&#160;&#160;arpeggio key mappings.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9488">vim-arpeggio-0.0.4.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-11-11</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Add syntax highlighting for |arpeggio-commands|.<br>- Fix bugs of "noremap" version of |arpeggio-commands| such as<br>&#160;&#160;|:Arpeggionoremap| that {rhs} are remapped despite the meaning of<br>&#160;&#160;the commands.&#160;&#160;(Thanks for id:ampmmn)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9466">vim-arpeggio-0.0.3.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-11-09</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="roweven" valign="top" width="2000">- Add |:Arpeggio|.<br>- Support |:map-&lt;unique&gt;| for |arpeggio#map()| and other commands.<br>- In |arpeggio#map()| and other commands, warn if a key in {lhs} is<br>&#160;&#160;already mapped to something other than a part of another<br>&#160;&#160;|arpeggio-key-mapping|.<br>- Add custom 'timeoutlen' per key.&#160;&#160;See |g:arpeggio_timeoutlens|.<br>- Add a bit of completion for |arpeggio-commands|.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9457">vim-arpeggio-0.0.2.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.0.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-11-07</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Fix |:Arpeggionoremap| that allowed remapping but it should not do<br>&#160;&#160;so.&#160;&#160;(Thanks for id:ampmmn)<br>- Define |arpeggio-commands| and |arpeggio-variables| automatically<br>&#160;&#160;for interactive or other use.&#160;&#160;(But you still have to call<br>&#160;&#160;|arpeggio#load()| to use them in vimrc.)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9435">vim-arpeggio-0.0.1.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-11-06</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="roweven" valign="top" width="2000">- Implement |arpeggio-commands|.<br>- Add missing |arpeggio#list()|.<br>- Revise minor stuffs.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9434">vim-arpeggio-0.0.0.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.0.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-11-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=13595">Kana Natsuno</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  