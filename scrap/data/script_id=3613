<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">splitjoin.vim : A plugin that helps with switching between single-line and multiline code</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>25/7</b>,
    Downloaded by 662    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3613">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=31799">Andrew Radev</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Github repo is at <a target="_blank" href="https://github.com/AndrewRadev/splitjoin.vim">https://github.com/AndrewRadev/splitjoin.vim</a><br><br>This plugin is meant to simplify a task I've found too common in my workflow: switching between a single-line statement and a multi-line one.<br><br>I usually work with ruby and a lot of expressions can be written very concisely on a single line. A good example is the "if" statement:<br><br>&#160;&#160;&#160;&#160;puts "foo" if bar?<br><br>This is a great feature of the language, but when you need to add more statements to the body of the "if", you need to rewrite it:<br><br>&#160;&#160;&#160;&#160;if bar?<br>&#160;&#160;&#160;&#160;&#160;&#160;puts "foo"<br>&#160;&#160;&#160;&#160;&#160;&#160;puts "baz"<br>&#160;&#160;&#160;&#160;end<br><br>The idea of this plugin is to introduce a single key binding for transforming a line like this:<br><br>&#160;&#160;&#160;&#160;&lt;div id="foo"&gt;bar&lt;/div&gt;<br><br>Into this:<br><br>&#160;&#160;&#160;&#160;&lt;div id="foo"&gt;<br>&#160;&#160;&#160;&#160;&#160;&#160;bar<br>&#160;&#160;&#160;&#160;&lt;/div&gt;<br><br>And another binding for the opposite transformation.<br><br>This currently works for:<br>&#160;&#160;* Various constructs in Ruby and Eruby<br>&#160;&#160;* Various constructs in Coffeescript<br>&#160;&#160;* Various constructs in Perl<br>&#160;&#160;* Various constructs in Python<br>&#160;&#160;* PHP arrays<br>&#160;&#160;* Javascript object literals and functions<br>&#160;&#160;* Tags in HTML/XML<br>&#160;&#160;* CSS, SCSS, LESS style declarations.<br>&#160;&#160;* YAML arrays<br>&#160;&#160;* Lua functions<br>&#160;&#160;* Vimscript line continuations<br><br>For more information, try :help splitjoin, or take a look at the help file online at <a target="_blank" href="https://github.com/AndrewRadev/splitjoin.vim/blob/master/doc/splitjoin.txt">https://github.com/AndrewRadev/splitjoin.vim/blob/master/doc/splitjoin.txt</a><br><br>For more examples and corner cases, you can explore the "examples" directory here: <a target="_blank" href="https://github.com/AndrewRadev/splitjoin.vim/tree/master/examples">https://github.com/AndrewRadev/splitjoin.vim/tree/master/examples</a>. It's not present in the downloadable zip file to avoid cluttering your vimfiles with useless stuff.<br><br>If you encounter any bugs or have an idea for a new feature, the issue tracker is on github, at <a target="_blank" href="https://github.com/AndrewRadev/splitjoin.vim/issues">https://github.com/AndrewRadev/splitjoin.vim/issues</a>.<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Download splitjoin.zip.<br><br>If you're using pathogen (<a href="/scripts/script.php?script_id=2332">vimscript #2332</a>), extract it to a directory in your bundles or just clone the git repository at <a target="_blank" href="https://github.com/AndrewRadev/splitjoin.vim">https://github.com/AndrewRadev/splitjoin.vim</a><br><br>Alternatively, extract it to your vimfiles root (~/.vim on *nix, ~/vimfiles on Windows).</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3613"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3613">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18990">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.6.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-11-18</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Default mappings: gJ and gS<br>- Python lists, tuples, imports<br>- Ruby heredocs<br>- HTML tags in PHP code<br>- Various bugfixes</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18263">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.5.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-07-14</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">Various bugfixes</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18028">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.5.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-05-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Split ternary operators in coffeescript<br>- Perl support<br>- Various bugfixes</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17742">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.5.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-04-09</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial PHP support, thanks to Giuseppe Rota (<a target="_blank" href="https://github.com/grota">https://github.com/grota</a>)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17738">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-04-08</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">Support for multiline strings in coffeescript</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17608">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-03-18</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">Fix for small indenting problem with coffeescript.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17606">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-03-18</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Coffeescript support<br>- LESS support (same as SCSS)<br>- Various bugfixes</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16855">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-11-12</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Support for ruby 1.9-style hashes<br>- Vimscript support<br>- Initial Lua support<br>- Initial YAML support<br>- Various bugfixes</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16290">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-08-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">Bugfix for repeat.vim regression, thanks to kien (<a target="_blank" href="https://github.com/kien">https://github.com/kien</a>)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16206">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.2.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-08-06</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">Bugfix for eruby.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16177">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-08-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Javascript support<br>- Python support<br>- Option to omit curly braces when splitting ruby option hashes<br>- Refactoring and various bugfixes</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16009">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-06-30</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Support for auto-aligning ruby hashes and CSS declarations using Tabular (<a target="_blank" href="https://github.com/godlygeek/tabular">https://github.com/godlygeek/tabular</a>) or Align (<a href="/scripts/script.php?script_id=294">vimscript #294</a>)<br>- Support for repeat.vim (<a href="/scripts/script.php?script_id=2136">vimscript #2136</a>) thanks to kien (<a target="_blank" href="https://github.com/kien">https://github.com/kien</a>)<br>- Various bugfixes<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15931">splitjoin.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-06-18</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- CSS support<br>- Various bugfixes</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=15801">splitjoin.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-06-03</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=31799">Andrew Radev</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  