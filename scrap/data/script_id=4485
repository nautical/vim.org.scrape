<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">vim-git-log : View your git log interactively in Vim.&#160;&#160;Side by side diffs!</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>0/0</b>,
    Downloaded by 52    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:4485">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=2372">eric johnson</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>vim-git-log is a Vim plugin that helps browse your git log.&#160;&#160;This plugin<br>requires [Fugitive](<a target="_blank" href="https://github.com/tpope/vim-fugitive">https://github.com/tpope/vim-fugitive</a>).<br><br>See the git repo here: <a target="_blank" href="https://github.com/kablamo/vim-git-log">https://github.com/kablamo/vim-git-log</a><br><br>GitLog<br>-------------<br><br>To open a new buffer which displays a list of the changes in your git repo:<br><br>&#160;&#160;&#160;&#160;:GitLog<br><br>This plugin is basically a wrapper around `git log` so nearly any option or<br>argument that you can use with `git log` can be used here.&#160;&#160;The exceptions are<br>--pretty and options that affect --pretty.&#160;&#160;Here are some more examples:<br><br>View changes from the 'lib' directory instead of the repository root:<br><br>&#160;&#160;&#160;&#160;:GitLog lib<br><br>View changes from a specifi author/committer:<br><br>&#160;&#160;&#160;&#160;:GitLog --author Batman<br><br>View changes where the commit message matches a (PCRE) regular expression<br><br>&#160;&#160;&#160;&#160;:GitLog --grep 'dinosaurs'<br><br>View changes for a date range<br><br>&#160;&#160;&#160;&#160;:GitLog --since yesterday<br>&#160;&#160;&#160;&#160;:GitLog --since '1 month 1 week 1 day 1 hour 1 second'<br>&#160;&#160;&#160;&#160;:GitLog --since 2012-12-31<br>&#160;&#160;&#160;&#160;:GitLog --until 2012-12-31 23:30:60<br>&#160;&#160;&#160;&#160;:GitLog &lt;since&gt;..&lt;until&gt;<br><br><br>How to use the log browser<br>--------------------------<br><br>Running any of the above commands will open a window titled 'GitLog' that looks<br>like this and allows you to browse the git log:<br><br>&#160;&#160;&#160;&#160;Eric Johnson 4 weeks ago 5ef0fb2<br>&#160;&#160;&#160;&#160;Added daysPerYear.<br>&#160;&#160;&#160;&#160;lib/Networth/Controller/Calculator.pm<br>&#160;&#160;&#160;&#160;lib/Networth/Out/RealCost.pm<br>&#160;&#160;&#160;&#160;root/html/calculator/realCost.tt<br><br>&#160;&#160;&#160;&#160;Eric Johnson 5 weeks ago 9595fa0<br>&#160;&#160;&#160;&#160;fix css margin class.<br>&#160;&#160;&#160;&#160;root/css/networth.css<br>&#160;&#160;&#160;&#160;root/css/style.less<br>&#160;&#160;&#160;&#160;root/css/style.less.old<br>&#160;&#160;&#160;&#160;root/html/calculator/realCost.tt<br>&#160;&#160;&#160;&#160;root/html/fi.tt<br><br>&#160;&#160;&#160;&#160;Eric Johnson 6 weeks ago ecf43db<br>&#160;&#160;&#160;&#160;Css tweaks.<br>&#160;&#160;&#160;&#160;root/html/calculator/realCost.tt<br><br>Here are some commands you can use in the browser:<br><br>&#160;&#160; &lt;cr&gt;&#160;&#160;View the side by side diff of any file by putting your cursor on that<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; line and typing `d` (for diff).<br><br>&#160;&#160; d&#160;&#160;&#160;&#160; This has the same behavior as `&lt;cr&gt;`.<br><br>&#160;&#160; n&#160;&#160;&#160;&#160; Move your cursor to the first filename in the next commit.<br><br>&#160;&#160; N&#160;&#160;&#160;&#160; Move your cursor to the first filename in the previous commit.<br><br><br>To quickly exit out of the diff and return to the 'GitLog' window, type `q`.<br><br><br>Ribbon<br>-------------<br><br>**First** mark your place with<br><br>&#160;&#160;&#160;&#160;:RibbonSave<br><br>This will place a tag named __ribbon at origin/master.&#160;&#160;Basically you are<br>bookmarking our current spot with a `ribbon`.<br><br>**Next**, use Fugitive to pull down the latest changes made by your fellow conspirators from the<br>remote repository.&#160;&#160;<br><br>&#160;&#160;&#160;&#160;:Git pull<br><br>To review those changes use the following command:<br><br>&#160;&#160;&#160;&#160;:Ribbon<br><br>This will open a window titled 'Ribbon' that looks like this:<br><br>&#160;&#160;&#160;&#160;Eric Johnson 6 weeks ago ecf43db<br>&#160;&#160;&#160;&#160;Css tweaks.<br>&#160;&#160;&#160;&#160;root/html/calculator/realCost.tt<br><br>&#160;&#160;&#160;&#160;Eric Johnson 5 weeks ago 9595fa0<br>&#160;&#160;&#160;&#160;fix css margin class.<br>&#160;&#160;&#160;&#160;root/css/networth.css<br>&#160;&#160;&#160;&#160;root/css/style.less<br>&#160;&#160;&#160;&#160;root/css/style.less.old<br>&#160;&#160;&#160;&#160;root/html/calculator/realCost.tt<br>&#160;&#160;&#160;&#160;root/html/fi.tt<br><br>&#160;&#160;&#160;&#160;Eric Johnson 4 weeks ago 5ef0fb2<br>&#160;&#160;&#160;&#160;Added daysPerYear.<br>&#160;&#160;&#160;&#160;lib/Networth/Controller/Calculator.pm<br>&#160;&#160;&#160;&#160;lib/Networth/Out/RealCost.pm<br>&#160;&#160;&#160;&#160;root/html/calculator/realCost.tt<br><br>See the section on *How to use the GitLog browser* for a complete list of all<br>the commands available to you. <br><br>**Finally**, after you have reviewed all the changes, mark your place again with:<br><br>&#160;&#160;&#160;&#160;:RibbonSave<br><br>Bonus tips<br>----------<br><br>The default colors used in vimdiff look like they were created by crazy clowns.<br>You might like my colorscheme instead:<br><br>&#160;&#160;&#160;&#160;&#9889; mkdir -p ~/.vim/colors/<br>&#160;&#160;&#160;&#160;&#9889; wget <a target="_blank" href="https://github.com/kablamo/dotfiles/blob/master/links/.vim/colors/iijo.vim">https://github.com/kablamo/dotfiles/blob/master/links/.vim/colors/iijo.vim</a> -O ~/tmp/iijo.vim<br>&#160;&#160;&#160;&#160;&#9889; echo "colorscheme iijo" &gt;&gt; ~/.vimrc<br><br>How to use vimdiff:<br> - To switch windows type `ctl-w l` and `ctl-w h`.&#160;&#160;For more help see `:help window-move-cursor`.<br> - To open and close folds type `zo` and `zc`.&#160;&#160;For more help see `:help fold-commands`.<br><br>See also<br>--------<br><br>This script was inspired by<br><a target="_blank" href="http://gitready.com/advanced/2011/10/21/ribbon-and-catchup-reading-new-commits.html">http://gitready.com/advanced/2011/10/21/ribbon-and-catchup-reading-new-commits.html</a><br><br>I also wrote [git-ribbon](<a target="_blank" href="https://github.com/kablamo/git-ribbon">https://github.com/kablamo/git-ribbon</a>), a little Perl<br>script that does pretty much the same thing but from the commandline.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="4485"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=4485">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19686">vim-git-log-master.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.01</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-03-23</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=2372">eric johnson</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  