<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">code2html : Generate HTML or XHTML+CSS from a Vim buffer using the syntax-hili colorscheme</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>28/10</b>,
    Downloaded by 1841    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:330">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=723">S&#246;ren Andersen</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>WHAT:<br>This is a substantially modified rewrite of the "2html.vim" script that's found in the Vim runtime "syntax" directory, and used exactly as that file is used (i.e.): <br><br>&#160;&#160;:so[urce] ~/scripts/code2html.vim<br><br>RECENT:<br>24 Sep 2008 - ATTENTION PLEASE! There will be no more revisions to this script under *this* name.&#160;&#160;Because there is a collision between the name "code2html[.vim]" and another F/LOSS software project by that name, I am renaming this script to "buf2html[.vim]".&#160;&#160;THE NEW SCRIPT IS LOCATED AT <a target="_blank" href="http://www.vim.org/scripts/script.php?script_id=2384">http://www.vim.org/scripts/script.php?script_id=2384</a>.<br><br>WHO:<br>Based on work by Bram Moolenaar and David Ne&#269;as ("Yeti"), now maintained by Soren Andersen.&#160;&#160;Past contributions by Christian Hujer.&#160;&#160;&#160;&#160;&#160;&#160; <br><br>WHY:<br>The main thing this module changes is the automatic addition of some simple css code to specify the fixed-width (monospace) font you want the HTML-ized syntax-colored code to be rendered in -- IF any of those fonts are findable by the reader's WWW browser. This provides an HTML rendering in modern browsers that can surprisingly closely replicate the appearance in the GVIM editor.<br><br>INSTALL:<br>Save this vim file to a directory convenient for you. Do *not* place it in a runtime plugin directory; you do not want it to be evaluated when opening any file for editing.&#160;&#160;This is a script but not a plugin.&#160;&#160;The author tends to have a ~/scripts dir on his accounts, and places this file there, alongside awk, sed, perl and other such script/programs.<br><br>FEATURES:<br>The code will find out what "guifont" value the user is using, and convert that specification (usually set in ".gvimrc") to a css font spec. There are also changes to output an up-to-date xml -type header (DTD) by Christian Hujer.<br><br>It will also create a "&lt;DIV&gt;" section containing the main "&lt;PRE&gt;" block. To understand why this is good, running a test example shows better than a thousand words. It allows specification of the font in css, as well as some other things, like padding of the contents of the "&lt;PRE&gt;" and some differing bgcolor for the document as a whole vs the "code" content. This is all primarily a matter of personal preference of course.<br><br>Examples of source highlighted with this script can be found at my home page at <a target="_blank" href="http://intrepid.perlmonk.org">http://intrepid.perlmonk.org</a><br><br>If you view the output of this script -- run on your GVIM on one of your own source code documents, and viewed in a competent css-supporting browser -- you may find that the document looks so identical to your GVIM display that you'll be tempted to start sending vim commands to your browser ;-). <br><br>UPDATE: 22 Oct 2002<br>In a little while a new version will be uploaded (presently I am having Christian check our merged changes before we commit to releasing this major update).<br>&#160;&#160;* Made output of DTD not dependant on whether user has html_use_css=1.<br>&#160;&#160;* Made significant changes to program flow, fixed a problem with output of the style definitions in the header STYLE block.<br>&#160;&#160;* Added several new variables that the user can set to change the behavior of the script.<br><br>All in all, the program is more robust, outputs more correct (according to latest W3C standards) HTML, script has more configurability, and the output is prettier (as source).<br><br>UPDATE: Aug 08 2002 20:32:28 EDT<br>Sorry -- did not have time right now to properly document ALL the changes in v1.1, but these include:<br>&#160;&#160;* fixed the stupid ".main_code" bug (should have written to the head style block "#main-code" instead of ".main-code"). aargh.<br>&#160;&#160;* added a new function to create the CSS font specifier, it is doing a much better job than the previous code<br>&#160;&#160;* the output is much more cosmetically proper.<br>Thank you very much to all the people who downloaded version 1.0. Please give the update a try -- it is much improved.<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>There is no name-clash at present with any syntax file shipped in the VIM distro, so it could be placed in $VIMRUNTIME/syntax. But I prefer and recommend that you place it in an easy-to-remember location such as ~/scripts/.<br><br>You will also most likely want to ":let html_use_css = 1" before :source'ing this module, or there will be little point in it at all, because most of the changes go into action in the doc head '&lt;STYLE ...' block.&#160;&#160;Set that in your ~/.vimrc, -or- do :let g:html_use_css=1 | source ~/scripts/code2html.vim<br><br>A few notes on system fixed-width fonts, ADDED 06 Jul 2002:<br>Well, about fonts, not much can be said that isn't prefaced with the acknowledgement that the bottom line is personal taste. I am sure that fonts have been discussed by VIMmers many times before.<br><br>Anyway I wanted to note that the choices I make in my "_vimrc" reflect my personal tastes and knowledge, YMMV. However as it may be helpful for some readers I want to show what my _vimrc contains now:<br><br>&#160;&#160;:set guifont=anonymous:h8,lucida_console:h9,andale_mono:h10,lucida_sans_typewriter:h9,<br>&#160;&#160;:set guifont+=onuava:h9,crystal:h9,monospace_821_bt:h9,courier_new:h9<br><br>The first entry, "Anonymous", is a new discovery that I am excited about. It is a monospaced (fixed-width) font that looks very good and is unbelievably readable in my Win32-GVIM display at a 8-pixel height setting (":h8") whereas I dare not go lower than ":h9" with my second fav, Lucida Console (which is a very good font and slightly nudges out the popular "Andale Mono" at lowest point sizes, in my preference).<br><br>The Windows version of "Anonymous" is presently at<br><a target="_blank" href="http://www.ms-studio.com/FontSales/Resources/AnonymousTT.zip">http://www.ms-studio.com/FontSales/Resources/AnonymousTT.zip</a><br> linked to from&#160;&#160;<a target="_blank" href="http://www.ms-studio.com/FontSales/anonymous.html">http://www.ms-studio.com/FontSales/anonymous.html</a><br><br>"Anonymous" is released as Copyrighted freeware (I didn't read any detailed licensing terms but the sense one gets from that terminology is that it's likely to be pretty much compatible with the intent of most Open Source software licensing philosophies). I can definitely recommend that readers give this font a try.<br><br>In any case, the point of setting as many possible alternates in _gvimrc (or wherever you find it convenient to do so) is to hit as many target systems as possible without having the browser default to it's unqualified default monospace font. Of course for *serious* Web users that setting may already be those users' strong personal preference, but this is likely to be true in only a small minority of cases (any anyway such users will also often be using WWW clients that can override our specification with a user setting, such as Opera, if they object to our choice).<br><br>Please read the comments in the script source code for a more detailed explanation of Usage.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="330"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=330">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=2227">code2html-1_51_vim.tar</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.51</b></td>
    <td class="rowodd" valign="top" nowrap><i>2003-08-12</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=723">S&#246;ren Andersen</a></i></td>
    <td class="rowodd" valign="top" width="2000">The .tar file contains code2html-1_51_vim.gz and code2html-1_51_vim.gz.asc (a PGP/GnuPG signature for verifying the gzipped-script). I jumped version numbers, sorry.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=1105">code2html_vim.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2002-08-09</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=723">S&#246;ren Andersen</a></i></td>
    <td class="roweven" valign="top" width="2000">More small bug-stomping + now adds a little note at page bottom -- "made with VIM" sort of thing.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=1093">code2html_vim.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2002-08-08</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=723">S&#246;ren Andersen</a></i></td>
    <td class="rowodd" valign="top" width="2000">many "robustifications" and bug fixes.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=987">code2html.vim.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2002-07-03</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=723">S&#246;ren Andersen</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  