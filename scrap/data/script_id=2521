<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">searchfold.vim : Fold away lines not matching the last search pattern</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>45/12</b>,
    Downloaded by 664    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2521">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=8357">Andy Wokula</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>This script is an improved version of&#160;&#160;f.vim&#160;&#160;(<a href="/scripts/script.php?script_id=318">vimscript #318</a>).<br><br>Improvements: folding depth is limited, previous fold settings can be restored, pattern is always the last search pattern.<br><br>Script uses manual folds.&#160;&#160;The user's manual folds are not preserved (unlike <a href="/scripts/script.php?script_id=2302">vimscript #2302</a> does it).<br><br><br>Usage:<br><br>&lt;Leader&gt;z<br>fold away lines not matching the last search pattern.<br>With [count], change the initial foldlevel to ([count] minus one).&#160;&#160;The setting will be stored in g:searchfold_foldlevel and will be used when [count] is omitted.<br><br>&lt;Leader&gt;iz<br>fold away lines that do match the last search pattern (inverse folding).<br><br>&lt;Leader&gt;Z<br>try hard to restore the previous fold settings<br><br><br><br>Customization:<br><br>:let g:searchfold_maxdepth = 7<br>(number) maximum fold depth<br><br>:let g:searchfold_usestep = 1<br>(boolean)<br>Controls how folds are organized: If 1 (default), each "zr" (after "\z") unfolds the same amount of lines above and below a match.&#160;&#160;If 0, only one more line is unfolded above a match.&#160;&#160;This applies for next "\z" or "\iz".<br><br>:let g:searchfold_postZ_do_zv = 1<br>(boolean)<br>If 1, execute "zv" (view cursor line) after &lt;Leader&gt;Z.<br><br>:let g:searchfold_foldlevel = 0<br>(number)<br>Initial 'foldlevel' to set for &lt;Leader&gt;z and &lt;Leader&gt;iz<br><br>:let g:searchfold_do_maps = 1<br>(boolean)<br>Whether to map the default keys or not.<br><br><br><br>A few more links to check out:<br>- <a target="_blank" href="http://www.noah.org/wiki/Vim#Folding">http://www.noah.org/wiki/Vim#Folding</a><br>- <a href="/scripts/script.php?script_id=158">vimscript #158</a> (foldutil.vim)<br>- <a href="/scripts/script.php?script_id=578">vimscript #578</a> (allfold.tar.gz)<br><br>2011 May 24</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>this is a global plugin, :h add-plugin<br><br>which means:<br>- remove the version part "_0.9" from the file name (optional)<br>- drop the script file into your plugin folder (e.g. ~/vimfiles/plugin) and restart Vim, or :source the script manually</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2521"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2521">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15711">searchfold_0.9.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-05-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=8357">Andy Wokula</a></i></td>
    <td class="rowodd" valign="top" width="2000">renamed &lt;Plug&gt;SearchFoldDisable, fixed g:searchfold_usestep description (wrong default), removed annoying redraw</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13960">searchfold_0.8.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.8</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-09-30</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=8357">Andy Wokula</a></i></td>
    <td class="roweven" valign="top" width="2000">added inverse folding (&lt;Leader&gt;iz), g:searchfold_foldlevel, count for &lt;Leader&gt;z, &lt;Plug&gt; mappings, disabled F(), minor fixes</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9850">searchfold_0.7.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-01-22</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=8357">Andy Wokula</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  