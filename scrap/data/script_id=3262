<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">diff-fold.vim : Folds changesets, files, and hunks in Mercurial diff output</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>0/4</b>,
    Downloaded by 438    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3262">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=21650">Ryan Mechelke</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>ftplugin</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Pipe various Mercurial diff output to vim and see changesets, files, and<br>hunks folded nicely together.&#160;&#160;In addition to providing folding of diff<br>text, diff_fold also provides a navigation pane which you can use to more<br>easily navigate large diffs.<br><br>Some examples:<br>&#160;&#160;&#160;&#160;hg in --patch | vim -<br>&#160;&#160;&#160;&#160;hg diff | vim -<br>&#160;&#160;&#160;&#160;hg diff -r 12 -r 13 | vim -<br>&#160;&#160;&#160;&#160;hg export -r 12: | vim -<br>&#160;&#160;&#160;&#160;hg log --patch src\somefile.cpp | vim -<br><br>Navigation pane usage:<br>&#160;&#160;&#160;&#160;&#160;&#160; <br>&#160;&#160;&#160;&#160;The keybinding &lt;Leader&gt;nav will bring up the navigation pane.&#160;&#160;You can<br>&#160;&#160;&#160;&#160;use the 'Enter' or 'v' keys to either go to, or view whole changsets<br>&#160;&#160;&#160;&#160;or files in the Diff View<br><br>&#160;&#160;&#160;&#160;If you don't like the default mapping, you can map a new one as<br>&#160;&#160;&#160;&#160;follows:<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;map {newmap} &lt;Plug&gt;DiffFoldNav<br><br>Right now, this pretty much just works with Mercurial diff output.&#160;&#160;Because Hg can use Git style patches, it does work with some Git output but it is not 100% reliable or useful.<br><br>Eventually, I hope to make this work with more version control systems and just about anything that can produce unified diff output.<br><br>This plugin has also been made into a pathogen bundle and can be installed with an 'hg clone <a target="_blank" href="https://bitbucket.org/thetoast/diff-fold">https://bitbucket.org/thetoast/diff-fold</a>' in your pathogen bundle folder.<br>In the future, the pathogen bundle may be more up-to-date, but any major updates will be made available here.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Unzip into your ~/.vim or vimfiles folder.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3262"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3262">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15000">diff-fold.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-02-10</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=21650">Ryan Mechelke</a></i></td>
    <td class="rowodd" valign="top" width="2000">* Added syntax highlighting to changeset details<br>* Added a navigation pane for navigating large diffs</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=14730">diff-fold.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-01-11</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=21650">Ryan Mechelke</a></i></td>
    <td class="roweven" valign="top" width="2000">* Fixed a bug with 'hg export' style changsets<br>* Added foldtext for 'hg export' style changesets<br>* Made global commands silent</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=14693">diff-fold.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-01-07</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=21650">Ryan Mechelke</a></i></td>
    <td class="rowodd" valign="top" width="2000">* Adding an ftdetect script so that hg output is recognized automatically. '-c "setlocal ft=diff"' no longer required<br>* Adding support for folding changesets produced by 'hg export' command<br>* More error handling</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13966">diff_fold.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-10-01</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=21650">Ryan Mechelke</a></i></td>
    <td class="roweven" valign="top" width="2000">* changed all "exec normal" calls to "normal!"<br>* checking for existence of final hunks/diffs/changesets to avoid double-folding in some cases<br>* foldtext now being set with "setlocal"<br><br>Thanks, Ingo, for the patch!<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13965">diff.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-10-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=21650">Ryan Mechelke</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  