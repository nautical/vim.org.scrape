<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">Window Sizes : Allows multiple windows to have specific vertical sizes</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>4/1</b>,
    Downloaded by 663    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:997">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=64">Salman Halim</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>History:<br><br>1.0:&#160;&#160;Initial version<br><br>1.1:&#160;&#160;Added mapping &lt;leader&gt;tog to toggle the behavior of observing fixed<br>window heights.&#160;&#160;If disabled, vertically split windows can be used without a<br>problem.&#160;&#160;Added two functions, WindowSizesEnabled and WindowSizesMaxEnabled,<br>which return whether fixed sizes are observed and whether window maxing is<br>enabled, respectively.<br><br>Plugin to keep the vertical sizes of windows within specifications.&#160;&#160;For<br>example, if you're using the Decho.vim plugin, you could force the DBG<br>window to always be 10 lines high and for all other windows to automatically<br>maximize upon entry.&#160;&#160;Or, you could specify explicit sizes for two windows<br>and let all the remaining space be shared equally among the other windows.<br><br>This is a bit different from using equalalways and winheight=999 because it<br>will always honor the vertical height settings of individual windows, using<br>the maximizing or equalization only on those windows that don't have<br>anything set explicitly.<br><br>Caveat (and it's a big one):&#160;&#160;No support for vertical splits.&#160;&#160;If you have a<br>vertical split, the behavior is undefined (well, it's not undefined, but<br>it's ugly).<br><br>Usage (default mappings):<br><br>&lt;leader&gt;max:&#160;&#160;Toggle whether windows without an explicit fixed size will<br>automatically maximize upon entry or whether all un-fixed-size windows will<br>simply share the vertical space left over once the fixed windows have taken<br>their share of the space.<br><br>&lt;leader&gt;fix:&#160;&#160;If the current window has a preferred fixed vertical size,<br>remove it, allowing this window to expand and contract according to the<br>g:WindowSizes_windowMax setting.<br><br>&lt;leader&gt;tog:&#160;&#160;Toggle the fixed-window-size behavior; the &lt;leader&gt;max<br>behavior is controlled separately.&#160;&#160;If fixed-window-sizes are not being<br>observed, then all windows are maximized blindly upon entering (without<br>regard to their preferred height) if maximizing is enabled.<br><br>&lt;s-up&gt;:&#160;&#160;Increases the current windows's size by the value of the<br>WindowSizes_increment variables (see below); using this mapping results in<br>fixing the window height.<br><br>&lt;s-down&gt;:&#160;&#160;Decreases the current windows's size by the value of the<br>WindowSizes_increment variables (see below); using this mapping results in<br>fixing the window height.<br><br>Variables (configuration):<br>g:WindowSizes_windowMax:&#160;&#160;If 1, then windows are maximized (the &lt;leader&gt;max<br>mapping merely toggles this value between 1 and 0).<br><br>b:preferredWindowSize:&#160;&#160;If defined, the fixed vertical size of the current<br>window -- the window will be this size vertically (tweaked by the<br>&lt;leader&gt;fix, &lt;s-up&gt; and &lt;s-down&gt; mappings).&#160;&#160;If not defined, a prompt is<br>displayed for the user to specify a fixed vertical height for the current<br>window where the current height is used as the default value (hit &lt;ESC&gt; to<br>get out of it).<br><br>g:WindowSizes_increment:&#160;&#160;If defined, this is the increment by which to<br>increase or decrease the current window's size if using the &lt;s-up&gt; or<br>&lt;s-down&gt; mappings.&#160;&#160;If not defined, 5 is used.<br><br>b:WindowSizes_increment:&#160;&#160;If defined in a given window, this is the<br>increment by which to increase or decrease the current window's size when<br>using the &lt;s-up&gt; or &lt;s-down&gt; mappings.&#160;&#160;If not defined, the global version<br>(g:WindowSizes_increment) is used (if even THAT's not defined, 5 is used).<br><br>Functions (for use in the statusline, for example):<br><br>WindowSizesEnabled:&#160;&#160;Returns 1 if the preferred heights of windows is<br>currently being observed (toggle with the &lt;leader&gt;tog mapping).<br><br>WindowSizesMaxEnabled:&#160;&#160;Returns 1 if the current window is going to get as<br>much vertical real estate as possible (taking into consideration the<br>WindowSizesEnabled setting and the presence of other windows).<br><br>Tips:<br><br>- Always have at least one window without a specified size so it can take<br>&#160;&#160;over the remaining vertical space.<br><br>- Always make sure that the total size of the fixed windows is less than the<br>&#160;&#160;actual number of lines visible on-screen!&#160;&#160;(The behavior otherwise is<br>&#160;&#160;undefined -- that is, not tested by me.)<br><br>- To restore a window to not having a preferred size, unlet<br>&#160;&#160;b:preferredWindowSize (just use the &lt;leader&gt;fix mapping).<br><br>TODO:<br><br>- Observe the 'laststatus' value.<br><br>- Figure out how to change all the global variables to script or some such<br>&#160;&#160;so they don't pollute the global variable pool.<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Pop into your plugin directory; see the variables section for potential configuration (though none is explicitly required).</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="997"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=997">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5301">WindowSizes.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-02-22</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=64">Salman Halim</a></i></td>
    <td class="rowodd" valign="top" width="2000">Added the ability to toggle the behaviour on or off.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=3062">WindowSizes.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2004-05-26</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=64">Salman Halim</a></i></td>
    <td class="roweven" valign="top" width="2000">Added ability to disable the behavior where preferred heights are observed -- useful with vertically split windows (not perfect, but at least vertical splits are usable now).&#160;&#160;Added two functions to return whether the main sizing behavior as well as the window maxing are enabled.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=3060">WindowSizes.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2004-05-25</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=64">Salman Halim</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  