<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">python.vim : Enhanced version of the python syntax highlighting script</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>1292/540</b>,
    Downloaded by 77379    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:790">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>syntax</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Enhanced version of the original (from vim6.1) python.vim for Python programming language.<br><br>The changes since the original python.vim are:<br><br>- added support for Python 3 syntax highlighting<br>- added :Python2Syntax and :Python3Syntax commands which allow to<br>&#160;&#160;switch between Python 2 and Python 3 syntaxes respectively without<br>&#160;&#160;reloads/restarts<br>- changed strings highlighting;<br>- enhanced special symbols highlighting inside strings;<br>- enhanced numbers highlighting;<br>- added optional highlighting for %-formatting inside strings;<br>- added highlighting for some error conditions (wrong symbols in source file,<br>&#160;&#160;mixing spaces and tabs, wrong number values,<br>&#160;&#160;wrong %-formatting inside strings);<br>- added highlighting for magic comments: source code encoding<br>&#160;&#160;and #! (executable) strings;<br>- added highlighting for new exceptions and builtins introduced in python 2.3, 2.4 and 2.5;<br>- added highlighting for doctests;<br>- added highlighting for new @decorator syntax introduced in Python 2.4a2;<br>- added highlighting for trailing-space errors (triggered by new<br>&#160;&#160;option: python_highlight_space_errors);<br>- added highlighting for variable name errors;<br>- added highlighting for hex number errors;<br><br>Check the comments in the python.vim header for details how to report bugs and feature requests.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Place python.vim file in ~/.vim/syntax/ folder.<br><br>Check the comments in the python.vim header for additional options.</td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=790">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20262">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>3.3.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-06-02</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">More lightweight syntax reloading. Patch by Will Gray.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=20259">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.3.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-06-01</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">Fixed behavior of b:python_version_2 variable. Reported by Will Gray.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20103">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>3.3.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-05-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">Script was moved to its own repository: <a target="_blank" href="https://github.com/hdima/python-syntax">https://github.com/hdima/python-syntax</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=19619">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.3.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-03-10</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">Merge Python 2 and Python 3 script versions into the single python.vim script.<br><br>See the comments in the script header for details how to switch between highlighting for Python 2 and Python 3.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17430">python3.0.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>3.0.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-02-11</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">Updated email and URL</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17429">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.6.7</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-02-11</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">Updated email and URL</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=14268">python3.0.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>3.0.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-11-15</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Fixed highlighting for str.format syntax. Patch by Anton Butanaev.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=14240">python3.0.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.0.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-11-12</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Fixed bytes escapes highlighting. Patch by Anton Butanaev.<br>- Fixed highlighting for erroneous numbers.<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=14216">python3.0.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>3.0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-11-09</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Applied patch by Anton Butanaev which fixes highlighting for raw bytes literals</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12805">python3.0.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-04-09</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Applied patch by Andrea Riciputi with new configuration options "python_highlight_builtin_objs" and "python_highlight_builtin_funcs"</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12804">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.6.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-04-09</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Applied patch by Andrea Riciputi with new configuration options "python_highlight_builtin_objs" and "python_highlight_builtin_funcs"</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=11057">python3.0.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.0.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2009-07-24</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Applied patch by Caleb Adamantine which fixes highlighting for decorators</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=11056">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.6.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-07-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Applied patch by Caleb Adamantine which fixes highlighting for decorators</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=10509">python3.0.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2009-05-03</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Fixed compatibility with pyrex.vim</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=10508">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.6.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-05-03</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Fixed compatibility with pyrex.vim</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9627">python3.0.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>3.0.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-12-07</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">*Warning: For Python 3.0 only!*<br><br>- Added support for non-ASCII identifiers;<br>- Added support for new text strings and binary data (bytes);<br>- Updated support for numeric literals;<br>- Updated support for str.format;<br>- Added new builtins introduced in Python 2.6: "ascii", "exec", "memoryview", "print";<br>- Added new keyword "nonlocal";<br>- Removed exception "StandardError";<br>- Removed builtins: "apply", "basestring", "buffer", "callable", "coerce", "execfile", "file", "help", "intern", "long", "raw_input", "reduce", "reload", "unichr", "unicode", "xrange";</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9293">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.6.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-09-29</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Return back trailing 'L' support for numbers. Actually it was changed for future Python 3.0 syntax but in wrong file;</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9271">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.6.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-09-22</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Added "VMSError" exception;<br>- Added support for b"..." syntax;<br>- Added support for str.format brace escaping;</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9262">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.6.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-09-21</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">First Python 2.6 compatible release. Changes:<br><br>- Added new builtins and exceptions introduced in Python 2.6: "bin", "bytearray", "bytes", "format", "next", "BufferError", "BytesWarning";<br>- Added builtin "__debug__";<br>- Added global variables: "__doc__", "__file__", "__name__", "__package__";<br>- Removed "OverflowWarning" (removed in Python 2.5);<br>- Added option "python_print_as_function" for highlight "print" as a function;<br>- Added support for new integer literal syntax "0o" and "0b";<br>- Added support for string.Template syntax controlled by "python_highlight_string_templates" option;<br>- Added support for str.format syntax controlled by "python_highlight_string_format" option;<br>- Removed highlighting for "--" and "++" because it is a valid Python expressions which can be interpreted as "a + +b";</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=6716">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.5.6</b></td>
    <td class="roweven" valign="top" nowrap><i>2007-02-04</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- Applied patch by Pedro Algarvio to enable spell checking only for<br>&#160;&#160;the right spots (strings and comments);<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=6230">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.5.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-09-26</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- added new warnings (ImportWarning, UnicodeWarning) introduced in Python 2.5;</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5677">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.5.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-05-11</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- added highlighting for erroneous operators: &amp;&amp;, ||, ++, --, ===<br>&#160;&#160;(inspired by <a target="_blank" href="http://www.vim.org/tips/tip.php?tip_id=969">http://www.vim.org/tips/tip.php?tip_id=969</a>, thanks<br>&#160;&#160;Jeroen Ruigrok van der Werven for the link);<br>- added highlighting for new 'with' statement and 'BaseException',<br>&#160;&#160;'GeneratorExit' exceptions introduced in Python 2.5;<br>- added highlighting for 'OverflowWarning' exception which had been<br>&#160;&#160;forgotten;<br>- returned more robust recognition for function names;</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5373">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.5.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-03-06</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- fixed %-formatting highlighting for raw unicode strings;</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=4998">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.5.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-01-23</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- slightly simplified option handling;<br>- fixed regexp for indentation errors;<br>- fixed highlighting for backslashed symbols inside strings;<br>- added highlighting for trailing-space errors (triggered by new<br>&#160;&#160;option: python_highlight_space_errors);<br>- added highlighting for variable name errors;<br>- added highlighting for hex number errors;</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=4009">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.5.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2005-03-13</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- added new builtins 'all' and 'any' (Python 2.5a0)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=3303">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>2.4.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2004-08-05</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- added highlighting for new @decorator syntax introduced in Python 2.4a2</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=2840">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>2.4.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2004-03-17</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">- new versioning scheme (based on python version numbers);<br>- added highlighting for new types/builtins introduced in python 2.4<br>&#160;&#160; (set, frozenset, reversed, sorted);<br>- new option added: python_slow_sync (set this for slow but more<br>&#160;&#160;robust syntax synchronization);<br>- added highlighting for doctests;</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=2410">python.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.19</b></td>
    <td class="roweven" valign="top" nowrap><i>2003-10-17</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="roweven" valign="top" width="2000">- new option added: python_highlight_indent_errors;<br>- python_highlight_all now not override previously<br>&#160;&#160;set options, for example code<br>&#160;&#160;&#160;&#160;&#160;&#160;let python_highlight_indent_errors = 0<br>&#160;&#160;&#160;&#160;&#160;&#160;let python_highlight_all = 1<br>&#160;&#160;set all highlight options except indentation<br>&#160;&#160;errors highlighting option;</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=2392">python.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.17</b></td>
    <td class="rowodd" valign="top" nowrap><i>2003-10-13</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1203">Dmitry Vasiliev</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  