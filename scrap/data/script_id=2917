<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">tplugin : A plugin loader/runtimepath manager with autoload capabilities</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>7/3</b>,
    Downloaded by 859    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2917">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=4037">Tom Link</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>tplugin is a plugin management tool similar to pathogen. Other than <br>pathogen tplugin expands the 'runtimepath' as needed (e.g. when <br>calling a command or function that is defined in a bundle) in order to <br>have minimal impact on startup time.<br><br>This plugin helps users to deal with plugins that are distributed via <br>source code repositories. Usually, you would make those plugins <br>available to vim by adding the local copies of those repositories to <br>|runtimepath|. The disadvantage of this approach is that the value of <br>|runtimepath| becomes unnecessarily complex and that the plugins are <br>loaded on startup.<br><br>tplugin scans those plugins for commands and functions. It then amends <br>the |runtimepath| only if a respective command/function/map is called <br>(similar to the AsNeeded plugin). This helps to keep your |runtimepath| <br>simple and to improve startup time.<br><br>In order to achieve this goal, plugins are installed in an alternative <br>directory structure where each plugin (consequently referred to as <br>"repo") is installed in a subdirectory. tplugin takes care of modifying <br>'runtimepath' and of loading the vim script files as necessary.<br><br>Alternative directory layout:<br><br>&#160;&#160;&#160;&#160;ROOT/<br>&#160;&#160;&#160;&#160;&#160;&#160;repo1/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;after/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;autoload/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;doc/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;plugin/<br>&#160;&#160;&#160;&#160;&#160;&#160;repo2/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;after/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;autoload/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;doc/<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;plugin/<br>&#160;&#160;&#160;&#160;&#160;&#160;...<br><br>Repos can be enabled (i.e. added to 'runtimepath') either explicitly <br>with the |:TPlugin| command or automatically by calling a command, <br>function, or by requesting a filetype plugin/syntax/indent file defined <br>in a repo -- this is done similar to how AsNeeded autoloads files.<br><br>Full docs:<br><a target="_blank" href="http://github.com/tomtom/tplugin_vim/blob/master/doc/tplugin.txt">http://github.com/tomtom/tplugin_vim/blob/master/doc/tplugin.txt</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Edit the vba file and type: &gt;<br><br>&#160;&#160;&#160;&#160;:so %<br><br>See :help vimball for details. If you have difficulties or use vim 7.0, <br>please make sure, you have the current version of vimball<br>(<a href="/scripts/script.php?script_id=1502">vimscript #1502</a>) installed or update your runtime.<br><br>Getting started:<br><br>&#160;&#160;&#160;&#160;1. Add this to your vimrc file:<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;runtime macros/tplugin.vim<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;" Only necessary if you use a non-standard root directory<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TPluginRoot /home/x/src/repos<br><br>&#160;&#160;&#160;&#160;2. Install your plugins/repos in the root directory.<br><br>&#160;&#160;&#160;&#160;3. After restarting vim, type :TPluginScan!<br><br>Also available as git repository:<br><a target="_blank" href="http://github.com/tomtom/tplugin_vim">http://github.com/tomtom/tplugin_vim</a></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2917"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2917">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19016">tplugin.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.00</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-11-22</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Don't try to run :helptags on directories with no text files<br>- g:tplugin#show_helptags_errors: If true, show errors when running :helptags.<br>- Support for extradite<br>- TPluginVimEnter(): :do VimEnter commands only if such autocommands are defined<br>- Make sure to register repos with no plugin file<br>- Autoload: Ignore E121 errors (local variables) and echo v:exception on errors<br>- g:tplugin#show_helptags_errors: If true, show errors when running helptags<br>MD5 checksum: 7c454f3a1f0a380743d5dcb39cfae912</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17288">tplugin.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.14</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-01-21</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- TPluginRegisterPlugin works again<br>- TPluginRoot!: With bang, mark this root for shallow scans<br>- Speed up scanning of "shallow roots"<br>MD5 checksum: fba3e4a4fad872e074f76dd3988c2cb3</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17075">tplugin.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.13</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-12-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Fix message when rescanning roots due to outdated meta files; rescan only once<br>MD5 checksum: 395629f472fea537b25cfb12ae01f829</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=15733">tplugin.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.12</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-05-26</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- Run helptags with silent!<br>- vcsdo: Finalize the name of the log buffer as script-local variable<br>- Experimental support for &lt;VIMPLUGIN&gt; tags used in vx* plugins.<br>- .gitignore<br>- Experimental support for parsing *-addon-info.txt and simpler dependency management<br>- s:AutoloadFunction(): Properly support autoload funcs in subdirectories<br>- &amp;rtp wasn't properly updated in certain situations<br>- Experimental: Scan sources for VimEnter autocommands<br>- autoload/tplugin/vcscommand: Adapt to new arguments<br>- FIX: scanning of autocmds: Don't use \k<br>- autoload/tplugin/fugitive: Don't call #fugitive_utility#VimEnter<br>- SetRoot(): Use inputdialog() to display "Rescanning roots: Please be patient" message<br>- TPluginRequire: Remove check for s:done[rootrepo] (over-optimization)<br>- s:TPluginComplete(): show all known repos as completion candidates<br>MD5 checksum: 8ff9f5f7c97d3e306bb543a60cf16271</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=14160">tplugin.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.11</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-11-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">Please see <a target="_blank" href="http://github.com/tomtom/tplugin_vim/commits/master/">http://github.com/tomtom/tplugin_vim/commits/master/</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13976">tplugin.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.10</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-10-02</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- Make helptags of repositories that weren't yet loaded available to the user.<br>- Renamed variables: g:tplugin#autoload_exclude, g:tplugin#scan<br>- Custom autoload definitions for some 3rd party plugins (PoC)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13660">tplugin.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-08-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Renamed #TPluginInclude to @TPluginInclude<br>- Added support for @TPluginMap, @TPluginBefore, @TPluginAfter annotations<br>- TPluginMap() restores the proper mode<br>- Load after/autoload/* files<br>- ...</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12831">tplugin.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.8</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-04-14</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- Delete commands only when they were defined without a bang; make sure all commands in a file defined without a bang are deleted<br>- g:tplugin_scan defaults to 'cfpt'<br>- Don't register each autoload function but deduce the repo/plugin from the prefix.<br>- g:tplugin_scan defaults to 'cfpta'<br>- TPluginCommand and TPluginFunction are functions. Removed the commands with the same name.<br>- #TPluginInclude tag<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12443">tplugin.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-02-20</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">- TPluginScan: try to maintain information about command-line completion (this won't work if a custom script-local completion function is used)<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12391">tplugin.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.6</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-02-16</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- CHANGE: The root specific autoload files are now called '_tplugin.vim'<br>- Provide a poor-man implementation of fnameescape() for users of older versions of vim.<br>- If the root name ends with '*', the root is no directory tree but a single directory (actually a plugin repo)<br>- s:TPluginComplete(): Hide tplugin autoload files.<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12275">tplugin.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-02-02</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Support for ftdetect<br>- Support for ftplugins in directories and named {&amp;FT}_{NAME}.vim<br>- :TPlugin accepts "-" as argument, which means load "NO PLUGIN".<br>- :TPluginScan: don't use full filenames as arguments for TPluginFiletype()<br>- FIX: Concatenation of filetype-related files<br>- FIX: Don't add autoload files to the menu.<br>- FIX: Don't load any plugins when autoloading an "autoload function"<br>- FIX: Filetype-related problems<br>- FIX: s:ScanLine: Don't create duplicate autoload commands.<br>- FIX: s:ScanRoots(): Remove empty entries from filelist<br>- g:tplugin_autoload_exclude: Exclude repos from autoloading<br>- If g:tplugin_autoload == 2, run |:TPluginScan| after updating tplugin.<br>- Moved autoload functions to macros/tplugin.vim -- users have to rescan their repos.<br>- Per repo metadata (ROOT/REPO/tplugin.vim)<br>- Relaxed the rx for functions<br>- Replaced :TPluginMap with a function TPluginMap()<br>- Speed up :TPluginScan (s:ScanRoots(): run glob() only once, filter file contents before passing it to s:ScanSource())<br>- TPluginMap(): Don't map keys if the key already is mapped (via maparg())<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12127">tplugin.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-01-19</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- TPluginBefore, TPluginAfter commands to define inter-repo dependencies<br>- Support for autoloading filetypes<br>- Support for autoloading &lt;plug&gt; maps<br>- Build helptags during :TPluginScan (i.e. support for helptags requires <br>autoload to be enabled) - Call delcommand before autoloading a plugin because of an unknown command<br>- TPluginScan: Take a root directory as the second optional argument<br>- The autoload file was renamed to ROOT/tplugin.vim<br>- When adding a repository to &amp;rtp, ROOT/tplugin_REPO.vim is loaded<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12050">tplugin.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-01-10</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Experimental autoload for commands and functions (&#224; la AsNeeded)<br>- Improved command-line completion for :TPlugin<br>- The after path is inserted at the second to last position<br>- When autoload is enabled and g:tplugin_menu_prefix is not empty, build a menu with available plugins (NOTE: this is disabled by default)<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12015">tplugin.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-01-05</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  