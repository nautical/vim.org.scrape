<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">scalefont : Switch between your favourite font/gui settings</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>7/3</b>,
    Downloaded by 1259    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1030">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=4037">Tom Link</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Sometimes you want to change the font size without changing the window <br>geometry. This plugin tries to set the 'guifont', 'columns', and 'lines' to <br>appropriate values in order to achieve this goal. You can define modes to <br>quickly switch between your favourite gui configuration: fonts, window <br>position and size, guioption settings etc.<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>It is important to correctly configure the plugin because it will set <br>the font and the window size when being loaded unless the <br>g:scaleFontDontSetOnStartup variable was defined.<br><br>Edit the vba file and type: &gt;<br><br>&#160;&#160;&#160;&#160;:so %<br><br>&lt;&#160;&#160; See :help vimball for details. If you have difficulties or use vim 7.0, <br>&#160;&#160;&#160;&#160;please make sure, you have the current version of vimball<br>&#160;&#160;&#160;&#160;(<a href="/scripts/script.php?script_id=1502">vimscript #1502</a>) installed.<br><br>Edit |vimrc|:<br><br>&#160;&#160;&#160;&#160;Set g:scaleFontSize, g:scaleFontWidth, and g:scaleFont according to your <br>&#160;&#160;&#160;&#160;likings. "#{SIZE}" and "#{WIDTH}" will be replaced with the appropriate <br>&#160;&#160;&#160;&#160;values. On non-Windows systems you also have to define <br>&#160;&#160;&#160;&#160;ScaleFontMaximizeWindow().<br><br>&#160;&#160;&#160;&#160;Examples: &gt;<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;let g:scaleFontSize&#160;&#160;= 11<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;let g:scaleFontWidth = 8<br>&#160;&#160;&#160;&#160;&#160;&#160;<br>&lt;&#160;&#160; Unix: &gt;<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;let g:scaleFont = "-misc-fixed-medium-r-semicondensed-*-#{SIZE}-120-*-*-c-*-iso8859-15"<br><br>&lt;&#160;&#160; Windows: &gt;<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;let g:scaleFont = "Lucida_Console:h#{SIZE}:cANSI""<br><br>Check if ScaleFontMaximizeWindow() function is functional on your <br>system. If not, redefine the function in |vimrc|.<br><br>Restart vim.<br><br><br>Also available via git<br><a target="_blank" href="http://github.com/tomtom/scalefont_vim">http://github.com/tomtom/scalefont_vim</a></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="1030"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1030">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=10001">scalefont.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-02-21</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">BufExec &amp; WinExec options</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=8408">scalefont.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-03-07</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">- Partial rewrite for vim7.0<br>- Default configuration is now done with a dictionary.<br>- On mode changes, always evaluate g:scaleFontExec__0 unless g:scaleFontAlwaysResetGuioptions is 0.<br>- FIX: Error when switching back to Normal.<br>- g:scaleFontMenuPrefix was replaced with g:scaleFontMenu<br>- Load only in gui<br>- s:ScaleFontSetSize(mode, ...): Arguments have changed<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=4123">scalefont.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2005-04-05</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">New variable g:scaleFontExec_{MODE}, the content of which will be executed before reconfiguring the window geometry. This can be used to set guioptions as needed (e.g., for a full screen mode without menu bar and all as for NormalFull). If this variable isn't set, the variable g:scaleFontExec_0 will be executed that resets the guioptions to its initial value.&#160;&#160;ScaleFontMaximizeWindow() is defined for OS other than Windows too (kind of). Build modes menu (see g:scaleFontMenuPrefix).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=3717">scalefont.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2004-12-20</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">We can now set the window/frame position too (e.g.&#160;&#160;NormalNarrowLeft and NormalWideTop); new standard modes: NormalMax, NormalNarrowLeft, NormalWideTop, bigMax; :ScaleFont as an alias to :ScaleFontMode; removed mode "0" from the mode list</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=3552">scalefont.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2004-10-17</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="rowodd" valign="top" width="2000">Take care of whether the font size is increased or decreased; command line completion for ScaleFontMode; ability to define the desired lines and cols parameters per mode (if one of these variables is -1, the window will be maximized; on non-Windows systems you have to define ScaleFontMaximizeWindow() first); new standard modes "largeMax" and "LargeMax" (maximize the window)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=3230">scalefont.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2004-07-15</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4037">Tom Link</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  