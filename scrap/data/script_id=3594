<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">ucompleteme : better code completion for vim</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>-1/1</b>,
    Downloaded by 233    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3594">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=30907">Aaron Stacy</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Overview<br>--------<br><br>On Github: <a target="_blank" href="https://github.com/aaronj1335/ucompleteme">https://github.com/aaronj1335/ucompleteme</a><br><br>`ucompleteme` is a Vim plugin for insert-mode completion.&#160;&#160;It combines the<br>results of omni-completion with its own "proximity" completion function.<br><br>`ucompleteme` attempts to provide completion options intelligently, so the user<br>doesn't have to think about what kind of code completion to use<br>([omni-completion](<a target="_blank" href="http://vimdoc.sourceforge.net/htmldoc/insert.html#i_CTRL-X_CTRL-O">http://vimdoc.sourceforge.net/htmldoc/insert.html#i_CTRL-X_CTRL-O</a>),<br>[current file completion](<a target="_blank" href="http://vimdoc.sourceforge.net/htmldoc/insert.html#i_CTRL-X_CTRL-N">http://vimdoc.sourceforge.net/htmldoc/insert.html#i_CTRL-X_CTRL-N</a>),<br>etc.).&#160;&#160;Plugins like<br>[SuperTab](<a target="_blank" href="http://www.vim.org/scripts/script.php?script_id=1643">http://www.vim.org/scripts/script.php?script_id=1643</a>) provide a<br>means to easily switch between different completion methods, but they aren't as<br>opinionated about building the list of matches for the user.<br><br>The philosophy behind `ucompleteme` is that instead of having to think about<br>which completion type is best, the completion funciton should make an educated<br>guess by first running the omni-completion function (where available), then<br>finding matches within the current buffer, then other buffers of the same<br>filetype, and finally the remaining buffers.<br><br><br>Details<br>-------<br><br>`ucompleteme` provides a [user-defined completion function](<a target="_blank" href="http://vimdoc.sourceforge.net/htmldoc/insert.html#i_CTRL-X_CTRL-U">http://vimdoc.sourceforge.net/htmldoc/insert.html#i_CTRL-X_CTRL-U</a>).<br>By default it re-maps `&lt;tab&gt;` in insert mode to use this function.&#160;&#160;If an<br>omnifuc is defined, `ucompletme` starts by populating the list with those<br>results first and giving the user a chance to interact.&#160;&#160;After that it will run<br>its own "proximity" completion function that searches each line of a buffer<br>progressively further from the cursor.&#160;&#160;The proximity search is run on the<br>current buffer, and then the other buffers of the same filetype, and finally<br>the remaining open buffers.<br><br><br>Options<br>-------<br><br> - **g:ucompleteme\_map\_tab**: if this is set to 1, `ucompleteme` will re-map<br>&#160;&#160; `&lt;tab&gt;` in insert mode<br><br> - **g:max\_lines\_for\_omnifunc**: omni-completion functions can be slow for<br>&#160;&#160; large files.&#160;&#160;this determines how large a file can be before `ucompleteme`<br>&#160;&#160; will not skip using omni-completion and just use keyword completion.<br><br><br><br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Installation<br>------------<br><br>Put the "ucompleteme.vim" file in your [Vim Runtimepath][1]'s autoload<br>direcory:<br><br> - On Linux/Mac OS X: `~/.vim/autoload`<br><br> - On Windows: `$HOME/vimfiles/autoload`<br><br>If you're using a Vim package manager like [Tim Pope][3]'s [pathogen][4], then<br>you should be able to just clone this repository into the "bundles" directory.<br><br>Finally, add the following to your [.vimrc][2]:<br><br>	call ucompleteme#Setup()<br><br><br>[1]: <a target="_blank" href="http://vimdoc.sourceforge.net/htmldoc/options.html">http://vimdoc.sourceforge.net/htmldoc/options.html</a>#'runtimepath' "Vim Runtimepath"<br>[2]: <a target="_blank" href="http://vimdoc.sourceforge.net/htmldoc/starting.html#.vimrc">http://vimdoc.sourceforge.net/htmldoc/starting.html#.vimrc</a> ".vimrc"<br>[3]: <a target="_blank" href="http://tpo.pe/">http://tpo.pe/</a> "Tim Pope"<br>[4]: <a target="_blank" href="http://www.vim.org/scripts/script.php?script_id=2332">http://www.vim.org/scripts/script.php?script_id=2332</a> "pathogen"</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3594"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3594">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15741">ucompleteme.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-05-27</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=30907">Aaron Stacy</a></i></td>
    <td class="rowodd" valign="top" width="2000">Search across all buffers</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=15682">ucompleteme.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-05-20</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=30907">Aaron Stacy</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  