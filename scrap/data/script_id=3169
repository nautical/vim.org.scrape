<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">luainspect.vim : Semantic highlighting for Lua in Vim</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>34/12</b>,
    Downloaded by 1784    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3169">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=14483">Peter Odding</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>syntax</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>This Vim plug-in uses the LuaInspect tool to (automatically) perform semantic highlighting of variables in Lua source code. It was inspired by lua2-mode (for Emacs) and the SciTE plug-in included with LuaInspect. For more information about the plug-in please refer to its homepage or the project page on GitHub:<br><br> &#149; <a target="_blank" href="http://peterodding.com/code/vim/lua-inspect/">http://peterodding.com/code/vim/lua-inspect/</a><br> &#149; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect">http://github.com/xolox/vim-lua-inspect</a><br><br>If you have questions, bug reports, suggestions, etc. the author can be contacted at peter@peterodding.com. If you like this plug-in please vote for it below!</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Please note that the vim-lua-inspect plug-in requires my vim-misc plug-in which is separately distributed (see <a href="/scripts/script.php?script_id=4597">vimscript #4597</a>).<br><br>Unzip the most recent ZIP archives of the vim-lua-inspect and vim-misc plug-ins inside your Vim profile directory (usually this is ~/.vim on UNIX and %USERPROFILE%\vimfiles on Windows), restart Vim and execute the command :helptags ~/.vim/doc (use :helptags ~\vimfiles\doc instead on Windows).<br><br>If you prefer you can also use Pathogen, Vundle or a similar tool to install &amp; update the vim-lua-inspect and vim-misc plug-ins using a local clone of the git repository.<br><br>Now try it out: Edit a Lua file and within a few seconds semantic highlighting should be enabled automatically!<br><br>Note that on Windows a command prompt window pops up whenever LuaInspect is run as an external process. If this bothers you then you can install my shell.vim plug-in (see <a href="/scripts/script.php?script_id=3123">vimscript #3123</a>) which includes a DLL that works around this issue. Once you&#146;ve installed both plug-ins it should work out of the box!</td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3169">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20223">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.27</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-05-27</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Make "go to definition" compatible* with Lua 5.2:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/77f9da1">http://github.com/xolox/vim-lua-inspect/commit/77f9da1</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=20215">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.26</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-05-25</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Document vim-misc as external dependency (needs to be installed separately from now on):<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/ebf637d">http://github.com/xolox/vim-lua-inspect/commit/ebf637d</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20179">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.24</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-05-20</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/942f58a">http://github.com/xolox/vim-lua-inspect/commit/942f58a</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=20167">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.23</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-05-19</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/dab5a33">http://github.com/xolox/vim-lua-inspect/commit/dab5a33</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20121">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.22</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-05-13</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/8e23d8e">http://github.com/xolox/vim-lua-inspect/commit/8e23d8e</a><br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/9c88b0e">http://github.com/xolox/vim-lua-inspect/commit/9c88b0e</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=20099">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.21</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-05-12</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Bug fix &amp; improvements for running LuaInspect as external process:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/a95cd00">http://github.com/xolox/vim-lua-inspect/commit/a95cd00</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20097">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.20</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-05-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Bug fix: Remove hidden Unicode character causing E129:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/1c183f4">http://github.com/xolox/vim-lua-inspect/commit/1c183f4</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=20004">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.19</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-05-02</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/2192365">http://github.com/xolox/vim-lua-inspect/commit/2192365</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19964">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.18</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-04-28</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/e0815fe">http://github.com/xolox/vim-lua-inspect/commit/e0815fe</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=19870">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.17</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-04-20</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Make compatibility with miscellaneous scripts explicit:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/c66d2b7">http://github.com/xolox/vim-lua-inspect/commit/c66d2b7</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16958">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.16</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-11-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; xolox#misc#os#exec() uses xolox#shell#execute() when available, falls back to system():<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/9b4c774">http://github.com/xolox/vim-lua-inspect/commit/9b4c774</a><br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/d488dd2">http://github.com/xolox/vim-lua-inspect/commit/d488dd2</a><br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/2161dc7">http://github.com/xolox/vim-lua-inspect/commit/2161dc7</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16925">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.15</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-11-21</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/1529fff">http://github.com/xolox/vim-lua-inspect/commit/1529fff</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16875">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.14</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-11-15</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; New :LuaInspectToggle command:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/1ed6e7a">http://github.com/xolox/vim-lua-inspect/commit/1ed6e7a</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16579">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.13</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-09-25</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/c8c3405">http://github.com/xolox/vim-lua-inspect/commit/c8c3405</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16457">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.12</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-09-04</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Updated miscellaneous scripts:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/fd90005">http://github.com/xolox/vim-lua-inspect/commit/fd90005</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16379">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.11</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-08-28</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Define version as variable, include version in messages:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/b92c629">http://github.com/xolox/vim-lua-inspect/commit/b92c629</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15725">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.10</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-05-25</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Added example to showcase semantic highlighting:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/0249104">http://github.com/xolox/vim-lua-inspect/commit/0249104</a><br><br> &#149; Make g:lua_inspect_path default compatible with Pathogen:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/26e6f35">http://github.com/xolox/vim-lua-inspect/commit/26e6f35</a><br><br> &#149; Bug fix: Only enable tooltips when supported: (thanks Micheal for the bug report!)<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/987eda1">http://github.com/xolox/vim-lua-inspect/commit/987eda1</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=14034">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.7</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-10-09</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Improve syntax error messages for unnamed buffers:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/3cbe412883edda9ce4cc">http://github.com/xolox/vim-lua-inspect/commit/3cbe412883edda9ce4cc</a><br><br> &#149; Make parse_text() more robust against missing buffer variables:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/bc013227961f7dc24818">http://github.com/xolox/vim-lua-inspect/commit/bc013227961f7dc24818</a><br><br> &#149; Bug fix: Focus doesn't return to original window:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/806dcb21f9f7be1de715">http://github.com/xolox/vim-lua-inspect/commit/806dcb21f9f7be1de715</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13627">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-08-19</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Always clear previously overlayed highlighting:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/62942b011979937fba84ea4795f7a5c593323f31">http://github.com/xolox/vim-lua-inspect/commit/62942b011979937fba84ea4795f7a5c593323f31</a><br><br> &#149; Bug fix for `E776: No location list':<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/47b25952b638bb02f69c7fb479cbb2d57c806a94">http://github.com/xolox/vim-lua-inspect/commit/47b25952b638bb02f69c7fb479cbb2d57c806a94</a><br><br> &#149; Make argument count warnings highlight only function names:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/22b75bcf657d4f55e48cce3801613ee3e3086af1">http://github.com/xolox/vim-lua-inspect/commit/22b75bcf657d4f55e48cce3801613ee3e3086af1</a><br><br>Lots of miscellaneous changes (see <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commits/">http://github.com/xolox/vim-lua-inspect/commits/</a>) to make the plug-in compatible with the newest release of LuaInspect, which itself includes bug fixes that fix bugs in this plug-in (e.g. when you add some empty lines to the start of a Lua buffer the highlighting would break, this turned out to be a bug in the Metalua lexer).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13590">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-08-16</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Bug fix: Strip newlines from warnings:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/59ba90c6c496dfe929cb83b896fe4e594d06354a">http://github.com/xolox/vim-lua-inspect/commit/59ba90c6c496dfe929cb83b896fe4e594d06354a</a><br><br> &#149; Ignore LuaInspect status messages on stderr (only applies when not using if-lua):<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/fd3920c5fd7faba9881a2b89525b5caa7753baa8">http://github.com/xolox/vim-lua-inspect/commit/fd3920c5fd7faba9881a2b89525b5caa7753baa8</a><br><br> &#149; Enable renaming variables that only differ in case:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/14b1ff64e29b6333d492a9fc8dfddb0427ee449e">http://github.com/xolox/vim-lua-inspect/commit/14b1ff64e29b6333d492a9fc8dfddb0427ee449e</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13581">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-08-15</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000">Some new features and lots of bug fixes:<br><br> &#149; Support dark colorschemes following scite.lua:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/c6a8ecf5067329540d70a313336d28cd33dbf4f9">http://github.com/xolox/vim-lua-inspect/commit/c6a8ecf5067329540d70a313336d28cd33dbf4f9</a><br><br> &#149; Bug fix: Show error highlighting only in Lua buffers:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/2d671c489bdf42dfe68b289588c09e0a18c7ce0b">http://github.com/xolox/vim-lua-inspect/commit/2d671c489bdf42dfe68b289588c09e0a18c7ce0b</a><br><br> &#149; Bug fix: Overlay argument count highlighting:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/7f9cbd60cbbc949ddb5c809eac2ba3a88dad44a6">http://github.com/xolox/vim-lua-inspect/commit/7f9cbd60cbbc949ddb5c809eac2ba3a88dad44a6</a><br><br> &#149; Split plug-in into command definitions / autoload functions:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/527e1b76a06c04baf2b9b597f1fbd5e0ef02fca4">http://github.com/xolox/vim-lua-inspect/commit/527e1b76a06c04baf2b9b597f1fbd5e0ef02fca4</a><br><br> &#149; if-lua now includes io.*; remove workaround<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/5f3bdbafcc5258487e94cf55dae9cde5374d97d1">http://github.com/xolox/vim-lua-inspect/commit/5f3bdbafcc5258487e94cf55dae9cde5374d97d1</a><br><br> &#149; Time plug-in execution for all actions:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/2a33bcb6876ef1bd0705ea8738ee0e5c735fff42">http://github.com/xolox/vim-lua-inspect/commit/2a33bcb6876ef1bd0705ea8738ee0e5c735fff42</a> <br><br> &#149; Use real buffer names instead of &#145;noname.lua&#146;:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/2e93bdd4f3de8a39fc22a6f2d29021a5873d057b">http://github.com/xolox/vim-lua-inspect/commit/2e93bdd4f3de8a39fc22a6f2d29021a5873d057b</a><br><br> &#149; Improved highlighting for argument count warnings:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/17bb37cefb7e562cacc0d076c6189aaf92026aca">http://github.com/xolox/vim-lua-inspect/commit/17bb37cefb7e562cacc0d076c6189aaf92026aca</a><br><br> &#149; Use LuaInspect's get_value_details() function:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/cca74e2556c351e91e150bcf33b69f537b4a2644">http://github.com/xolox/vim-lua-inspect/commit/cca74e2556c351e91e150bcf33b69f537b4a2644</a><br><br> &#149; Show warnings by LuaInspect in location list:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/f0eeca23130a1bbff1293e7203dcde8f7340d06a">http://github.com/xolox/vim-lua-inspect/commit/f0eeca23130a1bbff1293e7203dcde8f7340d06a</a><br><br> &#149; The packages for version 0.3.3 and 0.3.6 below should have been self contained but I failed to include the LuaInspect sources. This has been fixed now. Sorry about that!</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13555">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3.6</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-08-12</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Warn user + show tooltips for syntax errors:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/b0b58008127bcecae658922707370f0df915847c">http://github.com/xolox/vim-lua-inspect/commit/b0b58008127bcecae658922707370f0df915847c</a><br><br> &#149; Better highlighting of selected variables:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/026fdd42bca366b95a271c3b8c9fa78cbc17c0f9">http://github.com/xolox/vim-lua-inspect/commit/026fdd42bca366b95a271c3b8c9fa78cbc17c0f9</a><br><br> &#149; Highlight wrong function argument count:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/72a058e773a97781eaa731567faf1f2268576563">http://github.com/xolox/vim-lua-inspect/commit/72a058e773a97781eaa731567faf1f2268576563</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13549">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.3.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-08-11</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; &lt;F2&gt; mapping to rename all occurrences of variable:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/0c52221a952874807e50d668b0c89511378bdb10">http://github.com/xolox/vim-lua-inspect/commit/0c52221a952874807e50d668b0c89511378bdb10</a><br><br> &#149; 'gd' mapping to jump to variable declaration:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/bb99531941ca10ffd9b77a2a9dcd83a354a022cf">http://github.com/xolox/vim-lua-inspect/commit/bb99531941ca10ffd9b77a2a9dcd83a354a022cf</a><br><br> &#149; Tooltips to show variable details like state/type/signature/value:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/dc640de6799ba3da5f70073828da91ffaf0b596a">http://github.com/xolox/vim-lua-inspect/commit/dc640de6799ba3da5f70073828da91ffaf0b596a</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13536">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.2.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-08-10</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Highlight syntax errors as spelling errors:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/f5b367d8dd7d9d6da1fb359ab19b0e44c49161f7">http://github.com/xolox/vim-lua-inspect/commit/f5b367d8dd7d9d6da1fb359ab19b0e44c49161f7</a><br><br> &#149; Support automatic updates using GLVS plug-in:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/4c2c0bfd45e86dbfdbedf66c20425a3b4e9426a6">http://github.com/xolox/vim-lua-inspect/commit/4c2c0bfd45e86dbfdbedf66c20425a3b4e9426a6</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13533">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.2.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-08-10</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Now works on Windows and disables easytags.vim:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/ce2b578242ccf3d4eef658accc7e4711f34fbbbc">http://github.com/xolox/vim-lua-inspect/commit/ce2b578242ccf3d4eef658accc7e4711f34fbbbc</a><br><br> &#149; Converted the plug-in to a self contained package: Unzip and you're off!<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/43307cbb3a99b3a6075640151626f50846ced474">http://github.com/xolox/vim-lua-inspect/commit/43307cbb3a99b3a6075640151626f50846ced474</a><br><br> &#149; Don't show command prompts on Windows (integration with my shell.vim plug-in):<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/2689c2d1896d47c6aa3fa2d919d06d353cf0c17f">http://github.com/xolox/vim-lua-inspect/commit/2689c2d1896d47c6aa3fa2d919d06d353cf0c17f</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13519">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.2.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-08-07</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Highlight occurrences of variable under text cursor:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/6a0430cff4f4806a89732037437562ef3c36d0a0">http://github.com/xolox/vim-lua-inspect/commit/6a0430cff4f4806a89732037437562ef3c36d0a0</a><br><br> &#149; Don't parse the text when it hasn't been changed:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/a9205704796749a0556ce6e7a7959654b3983caa">http://github.com/xolox/vim-lua-inspect/commit/a9205704796749a0556ce6e7a7959654b3983caa</a><br><br> &#149; Use new LuaInspect API (without incremental parsing for now):<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/a14b977c69b6682054e80b9bcceb1c0f797fcd21">http://github.com/xolox/vim-lua-inspect/commit/a14b977c69b6682054e80b9bcceb1c0f797fcd21</a><br><br> &#149; Use if-lua by default (because of if-lua-corelibs.diff):<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/7bec1f49c6f12bbdde1f6d5a3470a9f66a8e34e2">http://github.com/xolox/vim-lua-inspect/commit/7bec1f49c6f12bbdde1f6d5a3470a9f66a8e34e2</a><br><br> &#149; Support disabling plug-in for specific buffers:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/3e5967143918c6bc383fae0442f10210c81be0f8">http://github.com/xolox/vim-lua-inspect/commit/3e5967143918c6bc383fae0442f10210c81be0f8</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13417">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-07-28</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000"> &#149; Don't perform semantic highlighting in 'diff' mode:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/47a223e11b469d871f8968c7f5bb7543d0f54e2c">http://github.com/xolox/vim-lua-inspect/commit/47a223e11b469d871f8968c7f5bb7543d0f54e2c</a><br><br> &#149; Add word boundaries to Vim search patterns:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/59637bd8e2843b51780866f10899f759aa07c416">http://github.com/xolox/vim-lua-inspect/commit/59637bd8e2843b51780866f10899f759aa07c416</a><br><br> &#149; (Performance) improvements suggested by Luis Carvalho:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/7de26617d1f659a3e173f8be5ed0305fee6a83a9">http://github.com/xolox/vim-lua-inspect/commit/7de26617d1f659a3e173f8be5ed0305fee6a83a9</a><br><br> &#149; `ErrorMsg' has better highlighting between colorschemes than `Error':<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/63dc4bb0f8094b45c7abe416d1ccbc28426d54cc">http://github.com/xolox/vim-lua-inspect/commit/63dc4bb0f8094b45c7abe416d1ccbc28426d54cc</a><br><br> &#149; Simplified wrapper as suggested by David Manura:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/ed34601710a09222ef3351082c40cc249eae9821">http://github.com/xolox/vim-lua-inspect/commit/ed34601710a09222ef3351082c40cc249eae9821</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13415">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-07-27</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000"> &#149; Enable users to define styles which the plug-in won't override:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/67777a75f4b9f4896d5818b57611d34c82a0187c">http://github.com/xolox/vim-lua-inspect/commit/67777a75f4b9f4896d5818b57611d34c82a0187c</a><br><br> &#149; Enable highlighting on user defined events:<br>&#160;&#160; <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/d07a1ff10cbd78d5e16c28d234b5e37bc0de88ad">http://github.com/xolox/vim-lua-inspect/commit/d07a1ff10cbd78d5e16c28d234b5e37bc0de88ad</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13414">luainspect.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-07-27</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="rowodd" valign="top" width="2000">README improvements, miscellaneous other changes: <a target="_blank" href="http://github.com/xolox/vim-lua-inspect/commit/ec4e1c31facda80b316a5062fe71a4fe40e07f84">http://github.com/xolox/vim-lua-inspect/commit/ec4e1c31facda80b316a5062fe71a4fe40e07f84</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13413">luainspect.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-07-27</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=14483">Peter Odding</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  