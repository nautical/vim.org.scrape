<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">unicode.vim : A Completion function for Unicode glyphs</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>8/5</b>,
    Downloaded by 1149    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2822">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Basically, this plugin serves 3 purposes:<br>1) it provides the possibility to complete characters using their Unicode Name or the Unicode Codepoint<br>2) It can be used to identify the character under the Cursor using the :UnicodeName command<br>3) It eases the use of digraphs, by providing the :Digraphs command (so one can more easily find a specific digraph).<br><br>For a screenshot, see: <a target="_blank" href="http://www.256bit.org/~chrisbra/vim_unicode.png">http://www.256bit.org/~chrisbra/vim_unicode.png</a><br><br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;*unicode-plugin*<br>1. Functionality<br><br>This plugin was written to enable an easier use of any Unicode glyph<br>available. The unicode.vim Plugin uses the data available from the Unicode<br>Consortium's website (<a target="_blank" href="http://www.unicode.org">http://www.unicode.org</a>) to let you enter Unicode<br>characters using a completion function.<br><br>By default, the plugin creates a directory unicode below the path autoload<br>where this plugin is located. Within this directory it will store&#160;&#160;the file<br>Index.txt from <a target="_blank" href="http://www.unicode.org/Public/UNIDATA/Index.txt">http://www.unicode.org/Public/UNIDATA/Index.txt</a> which it will<br>try to download using *netrw* . If this is unsuccessfull, or you do not have<br>|netrw| enabled, dowload the file manually and save it in the unicode<br>directory below the autoload directory in which unicode.vim is located.<br><br><br>						 *:EnableUnicodeCompletion*<br>By default the plugin is not enabled. To enable it enter<br>:EnableUnicodeCompletion<br>When you run this command, *unicode.vim* checks for the availability of<br>Index.txt from the Unicode Consortium, and if it is not available, it will try<br>to download it. <br><br>This will also set up the completion function |completefunc| to use for your<br>buffer. You can use |i_CTRL-X_CTRL-U| then to start the completion. <br><br>						 *:DisableUnicodeCompletion*<br>If you want to disable the plugin, enter<br>:DisableUnicodeCompletion<br><br>						&#160;&#160;&#160;&#160;*unicode-plugin-usage*<br>If you have enabled the plugin using |:EnableUnicodeCompletion| then there are<br>2 possibilities to use the plugin. You can either enter the Unicode Character<br>name, or enter the Unicode-Codeposition.<br><br>For example, you would like to enter &#198;, so you enter AE and press &lt;C-X&gt;&lt;C-U&gt;<br>while in insert mode. Alternatively you can enter the Unicode-Codepoint: U+C6<br>and press &lt;C-X&gt;&lt;C-U&gt; and the popup menu will show you all characters, that<br>have a codepoint like C6 with leading zeros, eg. U+00C6 and U+0C66<br><br>A popup menu will appear, showing you the Unicode-Codeposition value, the<br>Unicode Character Name and the Unicode Character (and if you have enabled it,<br>it can also show you the digraph characters needed to create this character in<br>paranthesis, see |unicode-plugin-config| ). You can scroll down in the menu by<br>pressing &lt;C-N&gt; and up by pressing &lt;C-P&gt;.<br><br><br>						&#160;&#160;&#160;&#160;*unicode-plugin-config*<br>The plugin can be customized to include the 2 digraph characters you have to<br>type, to get that character. This works only, if there is a digraph defined<br>for that Unicode char. If you would like this you need to set<br>g:showDigraphCode, e.g.<br>:let g:showDigraphCode=1<br>This functionality is by default disabled, cause it seems to cause some delay<br>and screen-rendering errors in the menu. Enter<br>:let g:showDigraphCode=0<br>to disable the Digraph feature afterwards.<br><br>If you would like to specify a different URL from which to download Index.txt,<br>enter the URL as:<br>:let g:unicode_URL='http:....'<br>To force downloading the file from that new url, enter<br>:call unicode#CheckUniFile(1)<br><br>						&#160;&#160;&#160;&#160; *unicode-plugin-error*<br>If the plugin gives an error, first check, that Index.txt from the Unicode<br>Consortium has been successfully downloaded. It should look something like<br>this:<br>A WITH ACUTE, LATIN CAPITAL LETTER	00C1<br>A WITH ACUTE, LATIN SMALL LETTER	00E1<br>A WITH BREVE, LATIN SMALL LETTER	0103<br>A WITH CARON, LATIN SMALL LETTER	01CE<br>A WITH CIRCUMFLEX, LATIN CAPITAL LETTER	00C2<br>A WITH CIRCUMFLEX, LATIN SMALL LETTER	00E2<br>A WITH DIAERESIS, LATIN CAPITAL LETTER	00C4<br>A WITH DIAERESIS, LATIN SMALL LETTER	00E4<br>A WITH DOT ABOVE, LATIN SMALL LETTER	0227<br>A WITH DOT BELOW, LATIN SMALL LETTER	1EA1<br>A WITH DOUBLE GRAVE, LATIN SMALL LETTER	0201<br>A WITH GRAVE, LATIN CAPITAL LETTER	00C0<br>[...]<br>(several thounsand lines following)<br><br>elinks is known to mangle that file and make it unusable, so please check<br>first if the format is right.<br><br>If the file looks correct, and the plugin is still not working correctly<br>contact the maintainer. You'll find his email-adress in the first line of this<br>document. Please be patient, it might take a while, until I can take care of<br>your report.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>simply source the file and the plugin will be installed automatically<br>:e unicode.vba<br>:so %<br><br>And afterwards, have a look at the documentation: :h unicode.txt</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2822"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2822">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19522">unicode-0.16.vmb</a></td>
    <td class="rowodd" valign="top" nowrap><b>16</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-02-16</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">:UnicodeName returns html entity, if possible<br>(automatically uploaded)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=19476">unicode-0.15.vmb</a></td>
    <td class="roweven" valign="top" nowrap><b>15</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-02-05</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000">- make sure, the returned digraphs list is not empty.<br>(automatically uploaded)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19074">unicode-0.14.vmb</a></td>
    <td class="rowodd" valign="top" nowrap><b>14</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-12-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">Added command :Digraphs for better output of digraphs<br>(automatically uploaded)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18569">unicode-0.13.vmb</a></td>
    <td class="roweven" valign="top" nowrap><b>13</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-09-08</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000">- better output for |UnicodeName| (did previously hide messages)<br>(automatically uploaded)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17762">unicode-0.12.vmb</a></td>
    <td class="rowodd" valign="top" nowrap><b>12</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-04-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">- UnicodeName shows digraph, if it exists<br>- better completion of digraphs<br>(automatically uploaded)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17070">unicode-0.10.vmb</a></td>
    <td class="roweven" valign="top" nowrap><b>10</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-12-15</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000">- enable completing of only the names<br>- Really disable the 'completefunc' when disabling the function</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16111">unicode-0.9.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>9</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-07-20</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">:UnicodeName checks for existence of UnicodeData.txt<br>:UnicodeName now also detects combined chars<br>:UnicodeName now also outputs control chars<br>(automatically uploaded)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13962">unicode.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.8</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-09-30</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000"> New Release<br>- Fix an issue with configuring the plugin (Thanks jgm)<br>- Code cleanup<br>- Make use of the preview window, when completing<br>&#160;&#160;Digraph or Unicode Glyphs<br>- By default, the Digraph Glyphs will now be enabled<br>&#160;&#160;using |i_Ctrl-X_CTRL-G| instead of using<br>&#160;&#160;Ctrl-X_Ctrl-C which wouldn't work in a terminal<br>- |:UnicodeName| now displays the hexadecimal Unicode<br>&#160;&#160;Codepoint instead of the decimal one (as this seems<br>&#160;&#160;to be the official way to display unicode<br>&#160;&#160;codepoints).<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=13899">unicode.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-09-23</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000"><br>- Determine the Name of a Character using :UnicodeName<br>- the global variable g:enableUnicodeCompletion determines, whether the completion will be loaded automatically</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=13680">unicode-0.6.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.6</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-08-26</i></td>
    <td class="roweven" valign="top" nowrap>7.2</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000">- many small bugfixes with regard to error-handling and error displaying<br>- use default netrw_http_cmd (instead of hardwiring wget)<br>- small documentation update (Inlude a snippet of UnicodeData.txt and get rid of Index.txt data)</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12871">unicode.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-04-19</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">New public repository at <a target="_blank" href="http://github.com/chrisbra/unicode.vim">http://github.com/chrisbra/unicode.vim</a></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12268">unicode.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-02-01</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000">Use UnicodeData.txt to generate Data<br>(Index.txt does not contain all glyphs).<br>Check for empty file UnicodeData.txt<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=11600">unicode.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-10-26</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">Added digraph completion<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=11564">unicode.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2009-10-22</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="roweven" valign="top" width="2000">Enabled GetLatestVimScripts</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=11563">unicode.vba</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-10-22</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=17599">Christian Brabandt</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  