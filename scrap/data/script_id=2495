<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">Pimp : Pimp provides a python shell in order to support REPL style development</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>9/3</b>,
    Downloaded by 437    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2495">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=15781">Yariv Barkan</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>ftplugin</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Pimp (Python Chimp) is based on (blatantly copied from) the Chimp plugin<br>(<a href="/scripts/script.php?script_id=2348">vimscript #2348</a>).&#160;&#160;It provides a very simple REPL mechanism by allowing you to<br>evaluate the contents of the active vim buffer in a running python console and<br>interact with the results.<br><br>=Screenshots=<br><a target="_blank" href="http://www.flickr.com/photos/nushoin/3121961021/sizes/o/">http://www.flickr.com/photos/nushoin/3121961021/sizes/o/</a><br><a target="_blank" href="http://www.flickr.com/photos/nushoin/3121961023/sizes/o/">http://www.flickr.com/photos/nushoin/3121961023/sizes/o/</a><br><br>To use the plugin you have to open a terminal (e.g. xterm) and run one of the<br>two scripts provided in the 'bin' directory.&#160;&#160;For a split screen configuration<br>run ~/.vim/bin/vimPython.bash. For a gvim session interacting with a python<br>interpreter running in another window run ~/.vim/bin/gvimPython.bash. Note that<br>you have to *execute* the scripts, not source them.<br><br>Now open a python source file or set the filetype to python (:set ft=python).<br>Enter some python code, then hit &lt;Esc&gt; for normal mode, followed by &lt;Leader&gt;pf.<br>The contents of the buffer should now be evaluated in the python console.<br><br>*Note*<br>The launching scripts try to run gvim, either in the terminal version (gvim -v,<br>in the vimPython.bash script) or the gui version (gvimPython.bash). It is<br>possible to run pimp with the non-gui version of vim by changing vimPython.bash.<br>However in this case evaluating marked text in visual mode will not work.<br><br>=Default bindings=<br>Assuming that your &lt;Leader&gt; key is '':<br>\pf Evaluate the current buffer (normal mode)<br>\pb Evaluate the marked code (visual mode).<br><br>=Tips=<br> - To move between the windows in the split screen configuration hit &lt;Ctrl&gt;-a<br>&#160;&#160; &lt;Tab&gt; (&lt;Ctrl&gt;-a is the escape key of gnu screen). <br> - To resize the window hit &lt;Ctrl&gt;-a followed by :resize N&lt;Enter&gt; where N is the<br>&#160;&#160; number of lines, E.g.&#160;&#160;&lt;Ctrl&gt;-a:resize 10&lt;Enter&gt;. <br> - To scroll back hit &lt;Ctrl&gt;-a&lt;Esc&gt;. That enters screen's copy mode (similar to<br>&#160;&#160; vim's visual mode). To exit that mode hit &lt;Esc&gt;.<br> - *New* As of Pimp version 0.5, evaluation of code selected in visual mode<br>&#160;&#160; actually works! Try using the right-extended visual block mode (Ctrl-v $) to<br>&#160;&#160; mark indented code (e.g. inside a function) in order to evaluate it. Take<br>&#160;&#160; advantage of the visual block mode to mark the code without the indentation<br>&#160;&#160; of course.<br> - The code is evaluated in IPython's namespace. That makes it possible to<br>&#160;&#160; access previously defined variables, functions etc. The reverse is true as<br>&#160;&#160; well - it's possible to switch to the IPython window and interact with the<br>&#160;&#160; results of the evaluated code, invoke functions etc.<br> - IPython has an abundance of 'magic' commands. For example it's possible to<br>&#160;&#160; 'cd', 'pwd' etc. from within IPython. Check the IPython documentation for<br>&#160;&#160; details.<br><br>It is advised to read the gnu screen manual (man screen) for additional<br>information about gnu screen's options and capabilities.<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Pimp depends on IPython (<a target="_blank" href="http://ipython.scipy.org">http://ipython.scipy.org</a>) as the python shell and on<br>gnu screen (<a target="_blank" href="http://www.gnu.org/software/screen">http://www.gnu.org/software/screen</a>) for it's operation. It is also<br>recommended to install the full version of vim (the package is named vim-gnome <br>under Ubuntu and vim-enhanced in Fedora).<br><br>Under Debian/Ubuntu:<br>sudo apt-get install ipython screen vim-gnome<br>Under Fedora:<br>yum install ipython screen vim-enhanced<br><br>=Installing the plugin=<br>mkdir ~/.vim<br>cd ~/.vim<br>tar -xvzf /path/to/Pimp.tar.gz<br>chmod +x ~/.vim/bin/*.bash<br><br>Add the following to your ~/.vimrc file:<br>filetype plugin on<br><br>=Keyboard bindings=<br>You may override the default keyboard bindings in your .vimrc file. For example<br>to bind Ctrl-e add the following lines to your .vimrc file:<br>map &lt;c-e&gt; &lt;Plug&gt;PimpEvalFile<br>imap &lt;c-e&gt; &lt;Esc&gt;&lt;Plug&gt;PimpEvalFilea<br>vmap &lt;c-e&gt; &lt;Plug&gt;PimpEvalBlock<br><br>=Problems and solutions=<br>When working in the split screen configuration, vim is run under a gnu screen<br>session.&#160;&#160;gnu screen itself runs inside a terminal emulator, e.g. konsole. Both<br>gnu screen and the terminal it is run in can cause various problems. Those<br>problems include trapping of keyboard shortcuts, wrong handling of colors<br>(relevant to users of CSApprox - <a href="/scripts/script.php?script_id=2390">vimscript #2390</a>) etc. Please refer to the file<br>~/.vim/reference/pimp/PimpCaveats.README for details.<br><br>=Testing=<br>This plugin has been tested on Ubuntu 8.10 and Fedora 9. If you can test it on<br>other platforms I'll be happy to hear about any platform related bugs and<br>solutions, and will integrate the fix into the next version.<br><br>Please send me an email if you have any problem installing or using the plugin.<br>My email is listed on my profile page.<br></td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="2495"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2495">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9720">Pimp-0.5.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-12-23</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=15781">Yariv Barkan</a></i></td>
    <td class="rowodd" valign="top" width="2000">Evaluation of code now works in all visual modes, some bugfixes and enhancements.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9714">Pimp.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2008-12-21</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=15781">Yariv Barkan</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  