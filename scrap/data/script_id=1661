<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">redocommand : Execute commands from the command history.</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>16/8</b>,
    Downloaded by 808    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1661">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>DESCRIPTION<br>Re-executes the last / Nth ex command previously entered in command mode. An<br>optional pattern is used to locate the most recent matching command. This is<br>similar to the command-line window (q:), or navigating the command history via<br>&lt;Up&gt; and &lt;Down&gt;, but provides an even faster way to re-execute a command if<br>you remember some characters or a pattern that identifies the command line.<br>The redocommand itself will not be included in the command history. Global<br>literal replacement can be done via 'old=new' arguments.<br><br>This is modeled after the 'fc -s' command from the Posix shell (which is often<br>aliased to 'r').<br><br>USAGE<br>:[N]Redocommand (or abbreviated :R)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Execute the last / Nth ex command.<br><br>:[N]Redocommand {pattern}<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Execute the last / Nth ex command that matches<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;{pattern}.<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Settings such as 'magic' and 'ignorecase' apply.<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;With N=0, only the very last command from the history<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;is executed if it matches {pattern}; the entire<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;history isn't searched.<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Note: If the {pattern} starts with : (and there is no<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;history command matching the literal ":cmd"), the<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;history is searched for "cmd", anchored at the<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;beginning. This is convenient because ":R :echo" is<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;more intuitive to type than ":R ^echo".<br><br>:[N]Redocommand {old}={new} [{old2}={new2} ...] [{pattern}]<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Execute the last / Nth ex command (that matches<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;{pattern}), replacing all literal occurrences of {old}<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;with {new}.<br><br>:[N]RedoRepeat [{old}={new} ...] (or abbreviated :RR)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Execute the last / Nth ex command that was repeated<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;via :Redocommand. Any replacements done the last time<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;are still in effect; new replacements of {old} to<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;{new} can be added.<br><br>The following variants are useful when you repeatedly use command A in one<br>buffer and command B in another. Instead of passing different [N] values to<br>:RedoRepeat, just recall from the local redo history.<br><br>:[N]RedoBufferRepeat [{old}={new} ...] (or abbreviated :RB)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Like :RedoRepeat, but repeat the last / Nth ex<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;command repeated in the current buffer.<br><br>:[N]RedoWindowRepeat [{old}={new} ...] (or abbreviated :RW)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Like :RedoRepeat, but repeat the last / Nth ex<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;command repeated in the current window.<br><br>EXAMPLE<br>Given the following history:<br>&#160;&#160;&#160;&#160;:history<br>&#160;&#160;&#160;&#160;1 e foo.txt<br>&#160;&#160;&#160;&#160;2 %s/foo/\0bar/g<br>&#160;&#160;&#160;&#160;3 w bar.txt<br>:Redocommand&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;will execute :w bar.txt<br>:Redocommand %&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;will execute :%s/foo\0/bar/g<br>:Redocommand foo&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;will execute :%s/foo\0/bar/g<br>:2Redocommand foo&#160;&#160;&#160;&#160;&#160;&#160; will execute :e foo.txt<br>:Redocommand b=B .txt=&#160;&#160;will execute ':w bar.txt' as :w Bar<br><br>:echo "another command"<br>:RedoRepeat&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; will again execute :w Bar<br>:2RedoRepeat&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;will execute :%s/foo\0/bar/g<br>:RedoRepeat B=F&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; will execute :w Far<br>:Redocommand&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;will execute :echo "another command"<br>:RedoRepeat&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; will execute :w Far</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>INSTALLATION<br>This script is packaged as a vimball. If you have the "gunzip" decompressor<br>in your PATH, simply edit the *.vba.gz package in Vim; otherwise, decompress<br>the archive first, e.g. using WinZip. Inside Vim, install by sourcing the<br>vimball or via the :UseVimball command.<br>&#160;&#160;&#160;&#160;vim redocommand.vba.gz<br>&#160;&#160;&#160;&#160;:so %<br>To uninstall, use the :RmVimball command.<br><br>DEPENDENCIES<br>- Requires Vim 7.0 or higher.<br><br>CONFIGURATION<br>If you do not want the shorthand :R, :RR, :R... commands, define (e.g. in your<br>vimrc):<br>&#160;&#160;&#160;&#160;let g:redocommand_no_short_command = 1</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="1661"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1661">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18293">redocommand.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.40</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-07-20</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">ENH: Add :RedoBufferRepeat / :RB and :RedoWindowRepeat / :RW commands. These are useful when you repeatedly use command A in one buffer and command B in another. Instead of passing different [N] values to :RedoRepeat, just recall from the local redo history.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16948">redocommand.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.30</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-11-23</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">- ENH: Add :RedoRepeat command to repeat the last / Nth :Redocommand when other Ex commands (e.g. :wnext) were issued in between. <br>- ENH: If the {pattern} starts with : (and there is no history command matching the literal ":cmd"), the history is searched for "cmd", anchored at the beginning. This is convenient because ":R :echo" is more intuitive to type than ":R ^echo". </td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=10342">redocommand.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.20</b></td>
    <td class="rowodd" valign="top" nowrap><i>2009-04-03</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Added optional [count] to repeat the Nth, not the last found match. <br>- Split off documentation into separate help file. Now packaging as VimBall.<br>- Using separate autoload script to help speed up VIM startup. </td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=9951">redocommand.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.10.005</b></td>
    <td class="roweven" valign="top" nowrap><i>2009-02-11</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">Implemented ':Redocommand old=new &lt;pattern&gt;'. Now requiring VIM 7. </td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=9071">redocommand.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.00.003</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-08-04</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">Better handling of errors during execution of the command. <br>The redone command is added to the history.&#160;&#160;<br>Last version with support for VIM 6.2 - 6.4; dropped support for VIM 6.0 and 6.1. </td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=6197">redocommand.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.02</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-09-18</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  