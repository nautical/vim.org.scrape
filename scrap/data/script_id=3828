<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">aurum : Plugin for dealing with source files under various VCS control</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>17/8</b>,
    Downloaded by 809    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3828">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=18939">ZyX  </a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td><br>This plugin provides a vim &lt;--&gt; VCS (currently mercurial, git and subversion) <br>integration for your projects. Features:<br>&#160;&#160;&#8226; Partially committing changes (:AuRecord [1]).<br>&#160;&#160;&#8226; Viewing file state at particular revision (<a target="_blank" href="aurum://file">aurum://file</a> [2], :AuFile [3]).<br>&#160;&#160;&#8226; Viewing uncommited changes in a vimdiff, as well as changes between <br>&#160;&#160;&#160;&#160;specific revisions (:AuVimDiff [4]). It is also possible to open multiple <br>&#160;&#160;&#160;&#160;tabs with all changes to all files viewed as side-by-side diffs.<br>&#160;&#160;&#8226; Viewing revisions log (:AuLog [5]). Output is highly customizable.<br>&#160;&#160;&#8226; Viewing working directory status (:AuStatus [6]).<br>&#160;&#160;&#8226; Commiting changes (:AuCommit [7]), commit messages are remembered in case of <br>&#160;&#160;&#160;&#160;rollback (g:aurum_remembermsg [8]).<br>&#160;&#160;&#8226; Obtaining various URL&#8217;s out of remote repository URL (like URL of the HTML <br>&#160;&#160;&#160;&#160;version of the current file with URL fragment pointing to the current line <br>&#160;&#160;&#160;&#160;attached: useful for sharing) (:AuHyperlink [9]). For mercurial it also <br>&#160;&#160;&#160;&#160;supports git and subversion revisions (in case you are using hg-git and <br>&#160;&#160;&#160;&#160;hgsubversion respectively).<br>&#160;&#160;&#8226; aurum#changeset() [10], aurum#repository() [11] and aurum#status() [12] functions <br>&#160;&#160;&#160;&#160;that are to be used from modeline.<br>&#160;&#160;&#8226; Frontends for various other VCS commands.<br>Most commands can be reached with a set of mappings (see aurum-mappings [13]), <br>all mappings are customizable.<br><br>Plugin&#8217;s mercurial driver is able to use mercurial python API as well as its <br>CLI, but remember that the former is much slower and less tested. In order to <br>use mercurial python API you must have vim compiled with +python (mercurial <br>currently does not support python 3) and have mercurial in python&#8217;s sys.path <br>(note: on windows msi installer is not adding mercurial to sys.path, so you <br>won&#8217;t be able to use its python API).<br><br>Plugin requires some additional plugins:<br>&#160;&#160;&#8226; frawor (<a target="_blank" href="https://bitbucket.org/ZyX_I/frawor">https://bitbucket.org/ZyX_I/frawor</a>)<br>&#160;&#160;&#8226; (optional) ansi_esc_echo (<a target="_blank" href="https://bitbucket.org/ZyX_I/ansi_esc_echo">https://bitbucket.org/ZyX_I/ansi_esc_echo</a>)<br>&#160;&#160;&#8226; (optional) one of<br>&#160;&#160;&#160;&#160;&#160;&#160;&#8226; Command-T (<a href="/scripts/script.php?script_id=3025">vimscript #3025</a>)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#8226; ctrlp (<a href="/scripts/script.php?script_id=3736">vimscript #3736</a>)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#8226; FuzzyFinder (<a href="/scripts/script.php?script_id=1984">vimscript #1984</a>)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#8226; unite (<a href="/scripts/script.php?script_id=3396">vimscript #3396</a>)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#8226; ku (<a href="/scripts/script.php?script_id=2337">vimscript #2337</a>)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#8226; tlib (<a href="/scripts/script.php?script_id=1863">vimscript #1863</a>)<br>&#160;&#160;&#160;&#160;for :AuFile [3] prompt option and a number of OpenAny/AnnotateAny mappings.<br>(with their dependencies).<br><br>Note: aurum supports VAM (<a target="_blank" href="https://github.com/MarcWeber/vim-addon-manager">https://github.com/MarcWeber/vim-addon-manager</a>). It <br>&#160;&#160;&#160;&#160;&#160;&#160;is prefered that you use it for aurum installation.<br><br>Project page: <a target="_blank" href="https://bitbucket.org/ZyX_I/aurum">https://bitbucket.org/ZyX_I/aurum</a><br>Documentation: <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html</a><br><br>[1] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line417-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line417-0</a> (:AuRecord)<br>[2] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line718-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line718-0</a> (<a target="_blank" href="aurum://file">aurum://file</a>)<br>[3] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line172-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line172-0</a> (:AuFile)<br>[4] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line460-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line460-0</a> (:AuVimDiff)<br>[5] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line271-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line271-0</a> (:AuLog)<br>[6] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line421-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line421-0</a> (:AuStatus)<br>[7] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line112-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line112-0</a> (:AuCommit)<br>[8] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line1138-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line1138-0</a> (g:aurum_remembermsg)<br>[9] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line214-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line214-0</a> (:AuHyperlink)<br>[10] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line516-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line516-0</a> (aurum#changeset())<br>[11] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line512-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line512-0</a> (aurum#repository())<br>[12] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line520-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line520-0</a> (aurum#status())<br>[13] <a target="_blank" href="http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line891-0">http://vimpluginloader.sourceforge.net/doc/aurum.txt.html#line891-0</a> (aurum-mappings)<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>You can install this plugin using vim-addon-manager (<a href="/scripts/script.php?script_id=2905">vimscript #2905</a>) plugin (install VAM, run &#8220;ActivateAddons aurum&#8221; and add &#8220;call vam#ActivateAddons(['aurum'])&#8221; to your .vimrc. VAM will install all dependencies for you). You can also use archives attached to this page, they contain plugin with all dependencies, so you do not need to fetch them yourself. <br><br>Gentoo users can use the following mercurial overlay: <a target="_blank" href="http://vimpluginloader.hg.sourceforge.net">http://vimpluginloader.hg.sourceforge.net</a>:8000/hgroot/vimpluginloader/pluginloader-overlay.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3828"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3828">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19716">aurum-1.7.1.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.7.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-03-28</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Make :AuH always use current changeset (it was using working directory changeset for url types &#8220;bundle&#8221;, &#8220;changeset&#8221; and &#8220;log&#8221;)<br>Fix &#8220;buffer does not exist&#8221; message shown after committing with AuRecord</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=19490">aurum-1.7.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.7</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-02-09</i></td>
    <td class="roweven" valign="top" nowrap>9999</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Added new powerline support<br>Added a number of AuFile mappings<br>Improved handling of wiping vimdiff buffers<br>Added renames support in AuFile and AuAnnotate<br>Added g:aurum_showprogress option<br>Added partial support for pygit2<br>Added partial support for bzrlib<br>Made querying status in separate process work regardless of presense of pyeval()<br>Fixed cursorbind for AuAnnotate<br>Fixed occasionally happenning &#8220;Cannot convert returned object to vim value&#8221; error<br>Fixed forced branch delete for git driver<br>Made it use new frawor autoloading capabilities</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18934">aurum-1.6.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-11-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Made aurum#status and aurum#branch functions use python multiprocessing module for improving their speed (not in bazaar driver)<br>Made repo.functions.pull also update when using mercurial driver<br>Made repo.functions.pull use &#8220;svn update&#8221; in subversion driver<br>Added full undo support in :AuRecord status buffer (it now can undo changes to file you edited)<br>Removed some unneeded Hit-Enter prompts<br>Added support for passwords in subversion (and other) repository URLs<br>Added mappings for opening diff or vimdiff split(s) from commit buffer<br>Added option for automatically opening diff with changes that are to be committed (excluding unknown and deleted files) when commit buffer is opened<br>Added initial bazaar support (all functions are there and working, but speed is far from optimal)</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18750">aurum-1.5.7.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.5.7</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-10-06</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Made git driver use fast-forward merges when pulling<br>Now using ansi_esc_echo in more cases<br>Fixed AuGrep when using git driver (errors when grep found nothing)<br>Fixed :AuHyperlink when using git driver (problems when pushurl is not defined, regression introduced in 1.5.1)<br>Made gitrepo.push/pull not be silent</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18531">aurum-1.5.6.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.5.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-09-02</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Added support tlib, ku and unite<br>Made AuVimDiff full only narrow file list when &#8220;files&#8221; option was specified<br>Improved code that disables vimdiff when closing one of the buffers: it is now always used and it works when closing both &#8220;original&#8221; and &#8220;changeset&#8221; versions</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18430">aurum-1.5.5.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.5.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-08-19</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Added :AuFile prompt option and corresponding OpenAny global mapping<br>Improved folding and foldtext in diff filetype plugin<br>Fixed phase updating in non-python version of mercurial driver</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18395">aurum-1.5.4.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.5.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-08-12</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Fixed listing of ignored files in git driver</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18392">aurum-1.5.1.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.5.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-08-12</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Added $parents_, $children_ and $tags_ template words, improved mimicing of mercurial log<br>Added support for listing ignored files in repo.functions.status in non-mercurial drivers<br>Improved support for older mercurial versions<br>aurum#status now does not report status for buffers with non-empty &amp;buftype<br>Speeded up some shell calls (requires python)<br>Fixed mercurial repository updating when user did history editing<br>Fixed template compiler that was removing some $words and joining lines under certain conditions<br>Fixed error message when trying to annotate untracked file<br>Fixed <a target="_blank" href="aurum://annotate">aurum://annotate</a> buffers highlighting and detection of full descriptions<br>Made :AuCommit use :startinsert! so that now cursor will appear at the end of line<br>Fixed repo.functions.grep in non-python version of mercurial driver<br>Fixed phases support in non-python version of mercurial driver</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18213">aurum-1.5.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-07-02</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Added autoaddlog AuLog option (lazy loading of changesets that are not immediately displayed)<br>Added powerline support (requires some changes to powerline theme and coloscheme)<br>Added aurum#branch()<br>Added support for vim.bindeval (added to vim since 7.3.569)<br>Added progress bar support to :AuLog<br>Changed AuHyperlink syntax, added AuHyperlink lines (points to a lines range. Currently only github is supported)<br>Added support for phases (mercurial driver only)<br>Added ansi_esc_echo plugin support (untested) (adds colored output, but uses :echo instead of :echom thus it is not saved in :messages)<br>Improved git driver speed<br>Improved repository object updating<br>Fixed some mappings (they did not conform other ones)<br>Fixed calling aurum commands not inside a repository<br>Fixed gettiphex function in git driver in case master branch is not present<br>Refactoring: moved almost all files to autoload/</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17710">aurum-1.4.11.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.11</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-04-03</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Added :AuLog ignfiles diff (for [vim]diff AuLog mappings) and open (for annotate, open, diff and similar AuLog mappings) options<br>Added location list support to :AuGrep<br>Added aurum python module reloading<br>Added g:aurum_loglimit option<br>Added support for custom mappings to :AuLog procinput<br>Renamed g:aurum_closewindow option to g:aurum_closelogwindow and g:aurum_usewin to g:aurum_vimdiffusewin<br>Mercurial graph drawing function optimizations (reduced time needed to draw cpython log from about 116 seconds to 97 on my machine)<br>:AuLog files option is supported in more mappings</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17618">aurum-1.4.10.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4.10</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-03-21</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Added subversion revisions and git sha1s support to mercurial driver for hgsubversion and hg-git respectively (latter works only with python version of driver)<br>Added push and pull (pull+update) global mappings<br>Fixed repository object updating in non-python driver<br>Made it restore some settings values after quiting annotation mode, made :AuAnnotate also set nowrap</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17580">aurum-1.4.9.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.9</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-03-10</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Added :AuOther push/pull/incoming/outgoing command<br>Fixed :AuFile replace<br>Fixed repo.functions.grep in newer mercurial versions</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17551">aurum-1.4.8.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4.8</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-03-02</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Made aurum:// *Cmd events support ++opts<br>Made mappings for <a target="_blank" href="aurum://file">aurum://file</a> and <a target="_blank" href="aurum://diff">aurum://diff</a> be created in more cases when these buffers are obtained using some mapping<br>Support more complex commands in procinput mode<br>Added svn-like logging style<br>Fixed :AuHyperlink with some URL types<br>Fixed :AuVimDiff failure to restore state in some cases</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17431">aurum-1.4.7.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.7</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-02-11</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Made :AuLog procinput be able to handle non-single-key commads (support is limited though)<br>Made subversion driver less fragile<br>Fixed :AuHyperlink support in git and subversion drivers<br>Fixed syntax highlighting for revision numbers in :AuLog subversionrepo<br>Fixed :read <a target="_blank" href="aurum://log">aurum://log</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17389">aurum-1.4.6.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-02-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Improved repo.functions.readfile(): it should now work correctly with binary files<br>Fixed launching :AuRecord in an old vim instance with already opened files</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17388">aurum-1.4.5.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-02-04</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Made *Update avoid &#8220;detached HEAD&#8221; state whenever possible when using git driver</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17384">aurum-1.4.4.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-02-04</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Improved :AuCommit: it now throws an error if there is nothing to commit<br>Improved sourceforge subversion checkouts support<br>Added macros that reloads aurum</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17379">aurum-1.4.3.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-02-04</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Made :AuJunk, :AuTrack and :AuMove normally work with directories</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17357">aurum-1.4.2.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-02-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Added subversion support<br>Fixed committing forgotten files in the git driver<br>Added repository object caching<br>Made different mappings more verbose in case of failure</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17297">aurum-1.4.1.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-01-22</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Renamed :AuStatus &#8220;rev1&#8221; and &#8220;rev2&#8221; options to &#8220;rev&#8221; and &#8220;wdrev&#8221; respectively<br>Added :AuStatus &#8220;changes&#8221; option<br>Added more mappings that invoke full vimdiff<br>Made gM be now default rhs for AuLog date mapping (old &#8220;gD&#8221; is taken by one of full vimdiff mappings)<br>Made git grapher also draw skipped changesets, though without messages</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17209">aurum-1.4.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-01-14</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Added incremental update support to <a target="_blank" href="aurum://log">aurum://log</a> so that you can view and use log buffers while log is being generated<br>Made :AuLog limit option apply after any other selector<br>Added g:aurum_hg_useshell option that makes you able to launch pre/post-{command} hooks while using Mercurial Python API<br><br>Fixed :write <a target="_blank" href="aurum://commit">aurum://commit</a><br>Fixed AuLog Prev/Next mappings: they did not work if some revisions were skipped<br>Fixed :AuCommit called in a diff buffer with full diff: it did not want to commit anything</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17148">aurum-1.3.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-01-03</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Added git driver<br>Added g:aurum_bufleaveremembermsg option<br>Added support for sourcing <a target="_blank" href="aurum://file">aurum://file</a> URLs<br>Added git and gitoneline log styles<br>Added graphing code ported from git<br>Made :AuJunk use untracked files if &#8220;ignore&#8221; option was specified<br><br>Various fixes:<br> &#8212; Fixed :AuMove repo/file repo/file2<br> &#8212; Fixed :AuCommit repo path/to/repo all<br> &#8212; Fixed grepping with mercurial+python driver: it now supports all possible filenames</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17047">aurum-1.2.2.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-12-09</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Added :AuName and :AuBranch commands</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17034">aurum-1.1.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-12-08</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="roweven" valign="top" width="2000">Made it possible to write your own SCM drivers<br>Some additions and fixes to :AuHyperlink remote repositories support</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=16995">aurum-1.0.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-11-29</i></td>
    <td class="rowodd" valign="top" nowrap>7.3</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=18939">ZyX  </a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial release</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  