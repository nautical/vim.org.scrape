<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">securemodelines : Secure, user-configurable modeline support</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>68/28</b>,
    Downloaded by 1541    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1876">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=4078">Ciaran McCreesh</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Secure, user-configurable modeline support for Vim 7.<br><br>Vim's internal modeline support allows all sorts of annoying and potentially insecure options to be set. This script implements a much more heavily restricted modeline parser that permits only user-specified options to be set.<br><br>The g:secure_modelines_allowed_items array contains allowable options. By default it is set as follows:<br><br>&#160;&#160;&#160;&#160;let g:secure_modelines_allowed_items = [<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "textwidth",&#160;&#160; "tw",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "softtabstop", "sts",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "tabstop",&#160;&#160;&#160;&#160; "ts",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "shiftwidth",&#160;&#160;"sw",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "expandtab",&#160;&#160; "et",&#160;&#160; "noexpandtab", "noet",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "filetype",&#160;&#160;&#160;&#160;"ft",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "foldmethod",&#160;&#160;"fdm",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "readonly",&#160;&#160;&#160;&#160;"ro",&#160;&#160; "noreadonly", "noro",<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ "rightleft",&#160;&#160; "rl",&#160;&#160; "norightleft", "norl"<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\ ]<br><br>The g:secure_modelines_verbose option, if set to something true, will make the script warn when a modeline attempts to set any other option.<br><br>The g:secure_modelines_modelines option overrides the number of lines to check. By default it is 5.<br><br>If g:secure_modelines_leave_modeline is defined, the script will not clobber &amp;modeline. Otherwise &amp;modeline will be unset.<br><br>Keeping things up to date on vim.org is a nuisance. For the latest version, visit:<br><br>&#160;&#160;&#160;&#160;<a target="_blank" href="http://github.com/ciaranm/securemodelines">http://github.com/ciaranm/securemodelines</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Install into your plugin directory of choice.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="1876"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1876">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=8598">securemodelines.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>20080424</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-04-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4078">Ciaran McCreesh</a></i></td>
    <td class="rowodd" valign="top" width="2000">Two tweaks, thanks to Christian J. Robinson: Make the messages it echoes end up in the :messages history. Modelines of the format "vim: set ...:" can also be be "vim:set ...:".</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=7142">securemodelines.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>20070518</b></td>
    <td class="roweven" valign="top" nowrap><i>2007-05-18</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4078">Ciaran McCreesh</a></i></td>
    <td class="roweven" valign="top" width="2000">The number of lines to search is now controllable via let g:secure_modelines_modelines=5. If g:secure_modelines_leave_modeline is defined, the internal &amp;modeline variable will be left alone. Previously only one line at the end of the document would be searched for modelines. This is now fixed, thanks to Thomas de Grenier de Latour. The &lt;SID&gt;DoModelines function can now be accessed externally via SecureModelines_DoModelines.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=7104">securemodelines.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>20070513</b></td>
    <td class="rowodd" valign="top" nowrap><i>2007-05-13</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=4078">Ciaran McCreesh</a></i></td>
    <td class="rowodd" valign="top" width="2000">Modelines with no set: are now parsed correctly. rightleft is now included in the default allowed options, for help files. Builtin modelines are now forcibly disabled.<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=7038">securemodelines.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>20070409</b></td>
    <td class="roweven" valign="top" nowrap><i>2007-04-29</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=4078">Ciaran McCreesh</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  