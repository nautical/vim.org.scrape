<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">TeX 9 : A semi-automatic LaTeX ftplugin with lots of firepower!</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>25/10</b>,
    Downloaded by 788    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3508">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=28301">Elias Toivanen</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>ftplugin</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>TeX 9<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; <br>&#160;&#160;&#160;&#160;Author: Elias Toivanen <br>&#160;&#160;&#160;&#160;License: GPL<br>&#160;&#160;&#160;&#160;Version: 1.2.1<br><br>&#160;&#160;&#160;&#160;TeX 9 is a ftplugin that aims to enhance the writing experience of high<br>&#160;&#160;&#160;&#160;quality documents with LaTeX and Vim. The goal of TeX 9 is to be simple<br>&#160;&#160;&#160;&#160;and Vimish, meaning that focus is on carefully thought-out key mappings<br>&#160;&#160;&#160;&#160;and features that are already present in a typical Vim installation. If<br>&#160;&#160;&#160;&#160;you need to write a thesis or research articles and insist on having only<br>&#160;&#160;&#160;&#160;one editor for all editing tasks, then TeX 9 is for you!<br>&#160;&#160;&#160;&#160;<br>&#160;&#160;&#160;&#160;TeX 9 uses Python2.x as its backend and therefore a Vim installation with<br>&#160;&#160;&#160;&#160;Python support is required. TeX 9 is being developed on Arch Linux running<br>&#160;&#160;&#160;&#160;Vim 7.3.<br><br>&#160;&#160;&#160;&#160; The main features of TeX 9 are<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Compile, debug and launch a document viewer from within Vim<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Insert LaTeX code snippets with ease<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Powerful text-object for LaTeX environments <br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Omni-completion of BibTeX database entries and label references<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Omni-completion of mathematical symbols<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* SyncTeX support (only with the Evince document viewer)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Filetype specific indentation (courtesy of Johannes Tanzler)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* LaTeX2e manual (ported to Vim by Mikolaj Machowski)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* No-hassle settings, relatively few mappings<br><br>&#160;&#160;&#160;&#160;Shoutouts<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Carl Mueller, this ftplugin was inspired by his `auctex.vim' script<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Vim-LaTeX people Srinath Avadhanula, Mikolaj Machowski and Benji Fisher<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Johannes Tanzler, Jose Aliste, Sergio Losilla<br><br>==============================================================================<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Unzip the tarball to your local Vim tree (usually ~/.vim)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Update helptags (:helptags ~/.vim/doc/)<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;* Read the help on TeX_9 (:help tex_nine)</td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3508">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19364">tex_nine-1.2.1.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.2.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-01-13</i></td>
    <td class="rowodd" valign="top" nowrap>7.2</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=28301">Elias Toivanen</a></i></td>
    <td class="rowodd" valign="top" width="2000">Bugfix release.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17849">tex_nine-1.2.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-05-01</i></td>
    <td class="roweven" valign="top" nowrap>7.3</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=28301">Elias Toivanen</a></i></td>
    <td class="roweven" valign="top" width="2000">SyncTeX support is back! Also a couple of other new features...</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17064">tex_nine-1.1.6.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1.6</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-12-14</i></td>
    <td class="rowodd" valign="top" nowrap>7.2</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=28301">Elias Toivanen</a></i></td>
    <td class="rowodd" valign="top" width="2000">Bug fixes and improved "go to BibTeX entry" command.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16025">tex_nine-1.1.5.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.1.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-07-03</i></td>
    <td class="roweven" valign="top" nowrap>7.2</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=28301">Elias Toivanen</a></i></td>
    <td class="roweven" valign="top" width="2000">Fixed a bug related to omnicompletion of pictures and fonts.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15586">tex_nine-1.05.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.05</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-05-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.2</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=28301">Elias Toivanen</a></i></td>
    <td class="rowodd" valign="top" width="2000">Fixed a critical bug that caused TeX_9 to crash when upgrading to Ubuntu 11.04 (i.e. Python 2.7, Vim 7.3). Also fixed a couple of minor bugs. Fixed...</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  