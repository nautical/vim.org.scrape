<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">TextTransform : Create text transformation mappings and commands.</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>12/3</b>,
    Downloaded by 220    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:4005">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>DESCRIPTION<br>This plugin allows you to build your own text transformations. You only supply<br>the transformation algorithm in the form of a Vim function that takes a string<br>and returns the transformed text (think substitute()), and TextTransform<br>will create all appropriate mappings and / or commands with a single call!<br><br>Do you often perform the same :substitute commands over and over again? You<br>may be able to save yourself a lot of typing by creating custom commands and<br>mappings for it. Because the mappings (like built-in Vim commands such as gU<br>or g?)&#160;&#160;are applicable to text moved over by {motion}, entire lines, and the<br>visual selection, you'll also have way more flexibility and places where you<br>can apply them (compared to the line-based range of :substitute).<br><br>RELATED WORKS<br>- Idea, design and implementation are based on Tim Pope's unimpaired.vim<br>&#160;&#160;plugin (<a href="/scripts/script.php?script_id=1590">vimscript #1590</a>). It implements XML, URL and C String encoding<br>&#160;&#160;mappings, but isn't extensible with other algorithms.<br>&#160;&#160;The TextTransform plugin enhances unimpaired's transformation function with<br>&#160;&#160;handling of text objects and a list of selection fallbacks, and allows to<br>&#160;&#160;not only create mappings, but also transformation commands.<br><br><br>USAGE<br>TextTransform#MakeMappings( {mapArgs}, {key}, {algorithm} [, {selectionModes}] )<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Create normal and visual mode mappings that apply<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;{algorithm} to the text covered by {motion}, [count]<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;line(s), and the visual selection.<br><br>TextTransform#MakeCommand( {commandOptions}, {commandName}, {algorithm} [, {options}] )<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Create a custom command {commandName} that takes a<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;range (defaulting to the current line), and applies<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;{algorithm} to the line(s).<br><br>TextTransform#MakeSelectionCommand( {commandOptions}, {commandName}, {algorithm}, {selectionModes} )<br><br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Create a custom command {commandName} that applies<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;{algorithm} on the TextTransform-selectionModes<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;specified by {selectionModes}, or the current visual<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;selection (when invoked from visual mode).<br><br>EXAMPLE<br>Here's a stupid transformation function that replaces all alphabetic<br>characters with "X":<br>&#160;&#160;&#160;&#160;function! BlankOut( text )<br>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;return substitute(a:text, '\a', 'X', 'g')<br>&#160;&#160;&#160;&#160;endfunction<br>With this, this single call:<br>&#160;&#160;&#160;&#160;call TextTransform#MakeMappings('', '&lt;Leader&gt;x', 'BlankOut')<br>creates this set of mappings:<br><br>&#160;&#160;&#160;&#160;&lt;Leader&gt;x{motion}&#160;&#160; transforms the text covered by {motion}<br>&#160;&#160;&#160;&#160;&lt;Leader&gt;xx&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;transforms [count] line(s)<br>&#160;&#160;&#160;&#160;{Visual}&lt;Leader&gt;x&#160;&#160; transforms the visual selection<br><br>You can set up a command for this transformation just as easily:<br>&#160;&#160;&#160;&#160;call TextTransform#MakeCommand('', 'TextBlankOut', 'BlankOut')<br>so you can blank out the next three lines via<br>&#160;&#160;&#160;&#160;:.,.+2TextBlankOut</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>INSTALLATION<br>This script is packaged as a vimball. If you have the "gunzip" decompressor<br>in your PATH, simply edit the *.vmb.gz package in Vim; otherwise, decompress<br>the archive first, e.g. using WinZip. Inside Vim, install by sourcing the<br>vimball or via the :UseVimball command.<br>&#160;&#160;&#160;&#160;vim TextTransform*.vmb.gz<br>&#160;&#160;&#160;&#160;:so %<br>To uninstall, use the :RmVimball command.<br><br>DEPENDENCIES<br>- Requires Vim 7.0 or higher.<br>- Requires the ingo-library.vim plugin (<a href="/scripts/script.php?script_id=4433">vimscript #4433</a>), version 1.004 or<br>&#160;&#160;higher.<br>- repeat.vim (<a href="/scripts/script.php?script_id=2136">vimscript #2136</a>) plugin (optional)<br>- visualrepeat.vim (<a href="/scripts/script.php?script_id=3848">vimscript #3848</a>) plugin (optional)</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="4005"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=4005">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=20149">TextTransform-1.11.vmb.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.11</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-05-17</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">- Avoid changing the jumplist.<br>- Add dependency to ingo-library (<a href="/scripts/script.php?script_id=4433">vimscript #4433</a>).<br>- FIX: When the selection mode is a text object, must still establish a visual selection of the yanked text so that g:TextTransformContext contains valid data for use by a:algorithm.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=19407">TextTransform-1.10.vmb.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.10</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-01-21</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">- FIX: In a blockwise visual selection with $ to the end of the lines, only the square block from '&lt; to '&gt; is transformed. Need to yank the selection with gvy instead of defining a new selection with the marks, a mistake inherited from the original unimpaired.vim implementation.<br>- Save and restore the original visual area to avoid clobbering the '&lt; and '&gt; marks and gv by line- and motion mappings.<br>- Temporarily set g:TextTransformContext to the begin and end of the currently transformed area to offer an extended interface to algorithms.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18550">TextTransform-1.03.vmb.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.03</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-09-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">- For the custom operators, handle readonly and nomodifiable buffers by printing just the warning / error, without the multi-line function error.<br>- Avoid clobbering the expression register (for commands that us&#160;&#160;options.isProcessEntireText).</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=18344">TextTransform.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.02</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-07-30</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">Avoid "E706: Variable type mismatch" when TextTransform#Arbitrary#Expression() is used with both Funcref- and String-type algorithms.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17726">TextTransform.vba.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.01</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-04-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="rowodd" valign="top" width="2000">In mappings and selection commands, place the cursor at the beginning of the transformed text, to be consistent with built-in transformation commands like gU, and because it makes much more sense.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17723">TextTransform.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>1.00</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-04-05</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=9713">Ingo Karkat</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  