<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">Smartgf : 'goto file' on steroids!</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>1/1</b>,
    Downloaded by 71    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:4386">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=61711">Alexander Gorkunov</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>About<br>-----<br>**smartgf** is a 'goto file' on steroids!<br><br>It's better than default gf because:<br><br>* It doesn't use ctags. So you don't need to run anything after changes.<br>* It shows you all available matches.<br><br>It's better than ack.vim because:<br><br>* It sets top priority for function/method/class/module definition.<br>* It uses filter by filetype by default.<br>* It skips comments in the search.<br>* You don't need to switch or use quickfix window. It works in command-line mode.<br><br>![smartgf.vim](<a target="_blank" href="https://github.com/gorkunov/smartgf.vim/raw/master/_assets/smartgf.png">https://github.com/gorkunov/smartgf.vim/raw/master/_assets/smartgf.png</a>)<br><br>Watch [this screencast](<a target="_blank" href="https://vimeo.com/56636037">https://vimeo.com/56636037</a>) for more details.<br><br>Installation<br>------------<br>First of all you need to have installed [ack](<a target="_blank" href="http://betterthangrep.com/">http://betterthangrep.com/</a>). So run this:<br><br>&#160;&#160;&#160;&#160;# on ubuntu<br>&#160;&#160;&#160;&#160;sudo apt-get install ack-grep<br><br>&#160;&#160;&#160;&#160;# on mac with homebrew<br>&#160;&#160;&#160;&#160;brew install ack<br><br>Or see details instruction [here](<a target="_blank" href="https://github.com/mileszs/ack.vim">https://github.com/mileszs/ack.vim</a>).<br><br>For quick plugin installing use [pathogen.vim](<a target="_blank" href="https://github.com/tpope/vim-pathogen">https://github.com/tpope/vim-pathogen</a>).<br><br>If you already have pathogen then put smartgf into ~/.vim/bundle like this:<br><br>&#160;&#160;&#160;&#160;cd ~/.vim/bundle<br>&#160;&#160;&#160;&#160;git clone <a target="_blank" href="https://github.com/gorkunov/smartgf.vim.git">https://github.com/gorkunov/smartgf.vim.git</a><br><br>Usage<br>-----<br>Smartgf uses default 'gf' combination. Set cursor position under the function <br>or method and type gf in normal mode. After that you will see dialog with search results. <br>Use 1-9 keys as quick shortcuts to select search result or use j,k to change cursor <br>position in the dialog and o,Enter to choose selected item.<br><br>*Note: By default smartgf uses filter by filetype and sets top priority for method definitions <br>and also skips comments in the search results. If you want to skip those filters use 'gF' instead of 'gf'.*<br><br>*Note: If you use rails.vim you should know that smartgf disable rails.vim 'gf' mappings.<br>You can change smartgf mappings (see configuration section) after that rails.vim should works in a normal way.*<br><br>Configuration<br>-------------<br>If you want to change default smartgf settings add those lines to your vimrc file.<br><br>Available settings for smartgf:<br><br>```viml<br>"Key for running smartpaigf with all filters (ft/comments/def)<br>"default is 'gf'<br>let g:smartgf_key = 'gf'<br><br>"Key for running smartpaigf without filters<br>"default is 'gF'<br>let g:smartgf_no_filter_key = 'gF'<br><br>"Max entries count to display (search results dialog)<br>"default is 9<br>let g:smartgf_max_entries_per_page = 9<br><br>"Min space between text and file path in the search results list<br>"default is 5<br>let g:smartgf_divider_width = 5<br>```<br><br>License<br>-------<br>Smartgf is released under the [wtfpl](<a target="_blank" href="http://sam.zoy.org/wtfpl/COPYING">http://sam.zoy.org/wtfpl/COPYING</a>)<br></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td></td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=4386">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19319">smartgf.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0.2</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-01-06</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=61711">Alexander Gorkunov</a></i></td>
    <td class="rowodd" valign="top" width="2000">Fix navigation keys in readme/legend</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=19318">smartgf.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0.1</b></td>
    <td class="roweven" valign="top" nowrap><i>2013-01-06</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=61711">Alexander Gorkunov</a></i></td>
    <td class="roweven" valign="top" width="2000">Fix docs <a target="_blank" href="https://github.com/gorkunov/smartgf.vim/issues/1">https://github.com/gorkunov/smartgf.vim/issues/1</a></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=19316">smartgf.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.0</b></td>
    <td class="rowodd" valign="top" nowrap><i>2013-01-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=61711">Alexander Gorkunov</a></i></td>
    <td class="rowodd" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  