<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">Headlights : A 'Bundles' menu for Vim (like TextMate)</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>34/10</b>,
    Downloaded by 547    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:3455">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>Headlights adds a 'Bundles' menu to Vim, revealing your bundles (aka. plugins) and the features they provide.<br><br>Headlights creates a menu for each _currently loaded_ bundle, grouping together docs, commands, mappings, abbreviations, functions, highlights, and plugin files.<br><br>Combined with a powerful bundle manager, Headlights will improve your Vim user experience -- nay, your quality of life.<br><br>Here, have a screenshot: <a target="_blank" href="https://github.com/mbadran/headlights/raw/master/headlights_ss.png">https://github.com/mbadran/headlights/raw/master/headlights_ss.png</a><br><br>For development activity, see the GitHub repository: <a target="_blank" href="https://github.com/mbadran/headlights">https://github.com/mbadran/headlights</a><br><br>For further reading, see the Headlights help menu:<br><br>&#160;&#160;&#160;&#160;Bundles &gt; a - i &gt; headlights &gt; Help</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>NOTE: Headlights requires Vim 7+ compiled with Python 2.6+ support.<br><br>THE RECOMMENDED WAY:<br><br>Using Vundle -- <a target="_blank" href="https://github.com/gmarik/vundle">https://github.com/gmarik/vundle</a><br><br>1. Add the following line to the Vundle section in your .vimrc:<br><br>&#160;&#160;&#160;&#160;Bundle 'Headlights'<br><br>2. Run the following Vim command:<br><br>&#160;&#160;&#160;&#160;BundleInstall<br><br>THE MANUAL WAY:<br><br>1. Download the latest package.<br><br>2. Expand the archive's contents into your .vim directory.<br><br>3. Run the following Vim command:<br><br>&#160;&#160;&#160;&#160;helptags &lt;vim_dir&gt;/&lt;plugin_dir&gt;/&lt;headlights_dir&gt;/doc/</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="3455"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=3455">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=18603">headlights_1.5.3.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.5.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-09-15</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="rowodd" valign="top" width="2000">- new option to run on GUI startup<br>- revert to flat menus<br>- new option to spillover menus<br>- doc updates<br>- minor code and menu usability improvements</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17948">headlights_1.5.2.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.5.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-05-13</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="roweven" valign="top" width="2000">- prevent headlights from running on GUI startup (faster)<br>- revamped error handling system<br>- changed the license<br>- doc updates<br>- minor code and usability improvements</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17651">headlights_1.5.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-03-24</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="rowodd" valign="top" width="2000">- huge performance boost (wait for cursorhold)<br>- refactoring<br>- strip the 'vim-' prefix from bundle names<br>- strip the '_vim' suffix from bundle names<br>- doc updates<br>- usability improvements</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17407">headlights_1.4.2.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-02-08</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="roweven" valign="top" width="2000">- highlights bug fix<br>- doc updates</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17375">headlights_1.4.1.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.4.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-02-03</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="rowodd" valign="top" width="2000">- usability fixes <br>- doc updates</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=17371">headlights_1.4.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2012-02-03</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="roweven" valign="top" width="2000">&#160;&#160;&#160;&#160;- menus are now intelligently grouped by bundle (optional)<br>&#160;&#160;&#160;&#160;- autoload bundles are now properly duplicated in the buffer menu<br>&#160;&#160;&#160;&#160;- new category: highlights<br>&#160;&#160;&#160;&#160;- usability improvements<br>&#160;&#160;&#160;&#160;- refactoring and performance tweaks</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=17254">headlights_1.3.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2012-01-19</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="rowodd" valign="top" width="2000">- added plugin load order<br>- fixed issue with python line separators across platforms<br>- fixed menu disordering problem on autoload<br>- refactoring and performance tweaks<br></td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=16078">headlights_1.2.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-07-15</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="roweven" valign="top" width="2000">- major rewrite<br>- added a new buffer-local menu<br>- catered for autoload bundles<br>- performance optimisations<br>- several bug fixes and improvements<br>- dropped autocmds altogether<br>- reintroduced functions as an optional bundle category<br>- cleaned up configuration options<br>- added missing mapping types<br>- removed the 'occurrences' help option<br>- updated reference docs</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=15808">headlights_1.1.zip</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2011-06-05</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="rowodd" valign="top" width="2000">- streamlined the menu layout<br>- fixed the debug mode feature<br>- added a debug menu for debug mode<br>- added an 'occurrences' help option (:helpgrep)<br>- moved vim plugins/bundles to their own menu (runtime)<br>- fixed several minor issues<br>- refactored and cleaned up code</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=15032">headlights.zip</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2011-02-17</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=26833">Mohammed Badran</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  