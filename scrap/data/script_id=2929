<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">mark_tools : Toggle and navigate marks</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>12/3</b>,
    Downloaded by 338    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:2929">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=1837">Sergey Khorev</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td>The plugin is another try on Vim marks. Often I end up using two separate mark groups for navigation (a-f and t-z) so I created a plugin to simplify this. Version 1.1 adds the ability to display marks in quickfix and location list windows.<br><br>Provided mappings:<br>&lt;Plug&gt;ToggleMarkAZ - mark/unmark current position, if there are multiple marks on the line, remove the first<br>&lt;Plug&gt;ToggleMarkZA - mark/unmark current position, if there are multiple marks on the line, remove the last<br>&lt;Plug&gt;ForceMarkAZ - add an unused mark starting from a, even if the position is marked<br>&lt;Plug&gt;ForceMarkZA - add an unused mark starting from z, even if the position is marked<br>&lt;Plug&gt;NextMarkPos - go to next mark<br>&lt;Plug&gt;PrevMarkPos - go to prev mark<br>&lt;Plug&gt;NextMarkLexi - go to previous mark in lexicographical order<br>&lt;Plug&gt;PrevMarkLexi - go to next mark in lexicographical order<br>&lt;Plug&gt;MarksLoc&#160;&#160;&#160;&#160;- open location list window with local mark positions<br>&lt;Plug&gt;MarksQF	&#160;&#160;&#160;&#160;- open quickfix window with marks<br><br>Recommended mapping to put into your .vimrc:<br>nmap &lt;Leader&gt;a &lt;Plug&gt;ToggleMarkAZ<br>nmap &lt;Leader&gt;z &lt;Plug&gt;ToggleMarkZA<br>nmap &lt;Leader&gt;A &lt;Plug&gt;ForceMarkAZ<br>nmap &lt;Leader&gt;Z &lt;Plug&gt;ForceMarkZA<br>nmap &lt;Leader&gt;m &lt;Plug&gt;NextMarkPos<br>nmap &lt;Leader&gt;M &lt;Plug&gt;PrevMarkPos<br>nmap &lt;Leader&gt;l &lt;Plug&gt;NextMarkLexi<br>nmap &lt;Leader&gt;L &lt;Plug&gt;PrevMarkLexi<br>nmap &lt;Leader&gt;w &lt;Plug&gt;MarksLoc<br>nmap &lt;Leader&gt;W &lt;Plug&gt;MarksQF<br><br>So<br>\a and \z will toggle a mark at current line<br>\A and \Z force another mark<br>\m and \M go to next/prev mark<br>\l and \L go to next/prev mark alphabetically<br>\w and \W populate location list/quickfix window with marks<br><br>Also I suggest to install a plugin to visualise marks e.g. quickfixsigns (<a href="/scripts/script.php?script_id=2584">vimscript#2584</a>)<br><br>Customisation:<br>toggle_marks_wrap_search variable controls whether search wraps around or not (order of precedence: w:toggle_marks_wrap_search, b:toggle_marks_wrap_search, g:toggle_marks_wrap_search)<br>Possible values:<br>-1 - use 'wrapscan' option value<br> 0 - do not wrap<br> 1 - always wrap (default)<br><br>Variables below define which marks should be displayed in loclist and quickfix windows respectively<br>let g:lmarks_names = 'abcdefghijklmnopqrstuvwxyz''.'<br>let g:gmarks_names = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'<br><br>I use the plugin on daily basis. If you found any problems or want to suggest a feature, drop me an email.</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>Put into $VIMRUNTIME/plugin or source the plugin directly with ":so mark_tools.vim"<br><br>Also available on <a target="_blank" href="https://bitbucket.org/khorser/vim-mark-tools">https://bitbucket.org/khorser/vim-mark-tools</a> and <a target="_blank" href="https://github.com/khorser/vim-mark-tools">https://github.com/khorser/vim-mark-tools</a></td></tr><tr><td>&#160;</td></tr></table><span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=2929">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=12264">mark_tools.vim</a></td>
    <td class="rowodd" valign="top" nowrap><b>1.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2010-02-01</i></td>
    <td class="rowodd" valign="top" nowrap>7.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=1837">Sergey Khorev</a></i></td>
    <td class="rowodd" valign="top" width="2000">Renamed to mark_tools, added functionality to display marks in quickfix/loclist windows</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=12113">toggle_local_marks.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>1.0</b></td>
    <td class="roweven" valign="top" nowrap><i>2010-01-16</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=1837">Sergey Khorev</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  