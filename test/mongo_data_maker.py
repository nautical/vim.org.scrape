import json
entry= """
{
    "__v" : 1,
    "_id" : 0,
    "author" : {
        "name" : "",
        "email" : ""
    },
    "dependencies" : [],
    "description" : "",
    "devDependencies" : [],
    "downloads" : {
        "total" : 0,
        "month" : 0
    },
    "id" : "",
    "keywords" : [],
    "lastIndexed" : "2013-07-10T13:04:24Z",
    "latestVersion" : "0.0.0",
    "metrics" : {
        "authorDepGithubFreshness" : 0,
        "authorDepGithubInterest" : 0,
        "authorDepNpmFrequency" : 0,
        "authorDepNpmFreshness" : 0,
        "authorDepNpmInterest" : 0,
        "authorDepNpmMaturity" : 0,
        "authorDepNpmNewness" : 0,
        "authorGithubFreshness" : 0,
        "authorGithubInterest" : 0,
        "authorNpmFrequency" : 0,
        "authorNpmFreshness" : 0,
        "authorNpmInterest" : 0,
        "authorNpmMaturity" : 0,
        "authorNpmNewness" : 0,
        "authorTotal" : 0,
        "depGithubFreshness" : 0,
        "depGithubInterest" : 0,
        "depNpmFrequency" : 0,
        "depNpmFreshness" : 0,
        "depNpmInterest" : 0,
        "depNpmMaturity" : 0,
        "depNpmNewness" : 0,
        "ghOwnerDepGithubFreshness" : 0,
        "ghOwnerDepGithubInterest" : 0,
        "ghOwnerDepNpmFrequency" : 0,
        "ghOwnerDepNpmFreshness" : 0,
        "ghOwnerDepNpmInterest" : 0,
        "ghOwnerDepNpmMaturity" : 0,
        "ghOwnerDepNpmNewness" : 0,
        "ghOwnerGithubFreshness" : 0,
        "ghOwnerGithubInterest" : 0,
        "ghOwnerNpmFrequency" : 0,
        "ghOwnerNpmFreshness" : 0,
        "ghOwnerNpmInterest" : 0,
        "ghOwnerNpmMaturity" : 0,
        "ghOwnerNpmNewness" : 0,
        "ghOwnerTotal" : 0,
        "githubFreshness" : 0,
        "githubInterest" : 0,
        "interestingScore" : 0,
        "newScore" : 0,
        "npmFrequency" : 0,
        "npmFreshness" : 0,
        "npmInterest" : 0,
        "npmMaturity" : 0,
        "npmNewness" : 0,
        "popularScore" : 0
    },
    "normalisedKeywords" : [],
    "owner" : "",
    "repository" : {
        "type" : "",
        "url" : ""
    },
    "versions" : [ 
        {
            "id" : "",
            "time" : "2011-05-28T11:43:54Z",
            "_id" : 0
        }
    ]
}"""

parsed_entry = json.loads(entry)
