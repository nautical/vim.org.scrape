<td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red"><tr><td valign="top">

<span class="txth1">TagsParser : Automatic tagfile updating and tag viewer</span> 

<br><br><!-- karma table --><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>&#160;script karma&#160;</b></td>
  <td>
    Rating <b>115/34</b>,
    Downloaded by 2126    &#160;
    <plusone></plusone></td>
  <td class="lightbg">
  <b>&#160;Comments, bugs, improvements&#160;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1535">Vim wiki</a>
  </td>  
</tr></table><p>

</p><table cellspacing="0" cellpadding="0" border="0"><tr><td class="prompt">created by</td></tr><tr><td><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">script type</td></tr><tr><td>utility</td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">description</td></tr><tr><td><br>The TagsParser plugin does two things.<br><br>First it automatically updates the tag files when you write a file out (Dynamic Tags).&#160;&#160;This is controlled by a path so that this does not happen for every file that you ever edit.&#160;&#160;This way as soon as you save a file, you will be able to access tags via the usual vim methods ^], :tag, :ts, ^T, etc.&#160;&#160;There are various options in the plugin that control which files to tag.&#160;&#160;<br><br>This is nice if you have a bunch of source that you are working on, and like to use ctags to navigate through your source, but get annoyed at manually re-tagging your source to pick up any new variables/functions/etc.<br><br>Secondly, there is a tag viewer (the Tag Window) which uses the tag files created by the Dynamic Tag functionality of the script to display the tags that belong to the file you have open.&#160;&#160;For C/C++ and Ada files, it will display the tags hierarchically, so for example, struct members get displayed below the struct they belong to.&#160;&#160;This part of the script can be turned off if you prefer to only use the Dynamic Tags functionality.&#160;&#160;There are many options which control what is displayed and how, and of course, these are all detailed in the included help file.<br><br>Configuring the "TagsParserTagsPath" or "TagsParserProjectConfig" options can be difficult sometimes.&#160;&#160;The ":TagsParserPrintPath" command can help you debug the normal tags path variable, but has not been updated to support debugging the project configurations yet.&#160;&#160;See the "tagsparser-config" help file entry for more details, but this is the short version:<br><br>The first important thing to keep in mind, is to use either unix-style slashes (/), or to 'escape' your windows-style slashes when using a double quoted path ("stuff\\foo").&#160;&#160;Alternatively single quotes can be used instead ('stuff\foo').<br><br>The second important thing to keep in mind is that the directories used in defining the project configuration root directories must be defined using the correct slash style for your platform.&#160;&#160;So for example, MS windows users _must_ use "\" instead of "/".&#160;&#160;Following is an project configuration setup:<br><br>&#160;&#160;let g:TagsParserProjectConfig = {}<br>&#160;&#160;let g:TagsParserProjectConfig['C:\Working\PRJ1\Software\Source Code'] = {}<br>&#160;&#160;let g:TagsParserProjectConfig['C:\Working\PRJ1\Software\Source Code'] = { 'tagsPath' : 'C:/Working/PRJ1/Software/Source Code/OBJ1,C:/Working/PRJ1/Software/Source Code/OBJ1/**,C:/Working/PRJ1/Software/Source Code/BSP,C:/Working/PRJ1/Software/Source Code/BSP/**' }<br>&#160;&#160;let g:TagsParserProjectConfig['C:\Working\PRJ2\Software\OBJ1'] = {}<br>&#160;&#160;let g:TagsParserProjectConfig['C:\Working\PRJ2\Software\OBJ1'] = { 'tagsPath' : 'C:/Working/PRJ2/Software/OBJ1,C:/Working/PRJ2/Software/OBJ1/**' }<br><br>This plugin supports all filetypes that Exuberant Ctags supports, along with Ada.&#160;&#160;For Ada support I have created an Ada Mode parser for Ctags.&#160;&#160;You can find the latest version of that parser here: <a target="_blank" href="http://gnuada.svn.sourceforge.net/viewvc/gnuada/tags/">http://gnuada.svn.sourceforge.net/viewvc/gnuada/tags/</a> At the time that I am writing this version 4.1.0 was the latest and is in this directory: <a target="_blank" href="http://gnuada.svn.sourceforge.net/viewvc/gnuada/tags/ctags-ada-mode-4.1.0">http://gnuada.svn.sourceforge.net/viewvc/gnuada/tags/ctags-ada-mode-4.1.0</a> Directions to install the ada.c file are in the file itself, or you can use the add_ada.vim file to do the necessary updates to the ctags source for you.<br><br>There are many options that govern how the Tag Window appears, I have tried to setup the default options so that it will display nicely with out any customization, but if you so desire there should be many ways to adjust the way the window appears.<br><br>Please email me with any questions you have or bugs you find.&#160;&#160;I am also happy to take suggestions for features or options that would make this plugin more useful for you.<br><br>-------- Notable Additions -------<br>- Example configuration options in help file.<br><br>- Tabpage support that actually works!&#160;&#160;(I hope)...<br><br>- Debugging functionality.<br><br>- TagsParserCurrentFileCWD feature (added in an earlier release, but was missing from documentation.<br><br>--- Previous Notable Additions ---<br>- The big addition to version 0.9 is project configurations, these are only supported in Vim 7.0.&#160;&#160;The full details can be gathered if you check the help page for "TagsParserProjectConfig", but a short version is here:<br>Instead of one path variable ("TagsParserTagsPath"), now you can configure separate paths for different 'projects'.&#160;&#160;A project is configured using a Vim hash (dictionary), where the root directory of a project is used as the hash key.&#160;&#160;Then a TagsPath, TagsLib, and various other variables can be configured uniquely for every project.&#160;&#160;This should reduce the time required to find tags in each project.<br><br>- Tabpages are now supported when using the Tag Window functionality.&#160;&#160;Also added some mappings to make it easier to navigate between tabs and buffers.&#160;&#160;See "TagsParserCtrlTabUsage" for detailed information.<br><br>- All commands have default key mappings (such as &lt;leader&gt;t&lt;space&gt; to toggle the Tag Window On and Off - :TagsParserToggle), and these can all be overridden by a user mapping.<br><br>- The TagsParserSaveInterval variable now prevents tagging a file too frequently (default of 30 second wait period).<br><br>- The TagsParserTagsDir variable can now be used to change the name of the directory that is created to store tag files in (default of ".tags").<br><br>- The TagsParser functions are now autoloaded, hopefully this speeds up vim startup and reduces overhead when the TagsParser is not being used.<br><br>- See "tagsparser-changelog" for more detailed information. </td></tr><tr><td>&#160;</td></tr><tr><td class="prompt">install details</td></tr><tr><td>This script requires a few supporting things to be installed.&#160;&#160;Exuberant Ctags (<a target="_blank" href="http://ctags.sourceforge.net/">http://ctags.sourceforge.net/</a>) and if you are not using Vim 7.0, or just like it better, Perl (<a target="_blank" href="http://www.perl.com/">http://www.perl.com/</a>).<br><br>For most linux/unix people these won't be a problem.&#160;&#160;If you are running windows and need Perl, you will need to install perl (<a target="_blank" href="http://www.activestate.com/Products/ActivePerl/">http://www.activestate.com/Products/ActivePerl/</a> is a decent version), and ctags.&#160;&#160;And for those running MacOS X you will likely only need to install ctags, because the ctags that comes with MacOS X by default is not Exubernat Ctags.<br><br>Untar the tarball in a directory in your vim runtimepath.&#160;&#160;Usually ~/.vim, $VIM/vimfiles, something along those lines.&#160;&#160;There are 3 files: autoload/TagsPaser.vim, plugin/TagsPaser.vim and doc/TagsParser.txt.&#160;&#160;After these files have been extracted, run the following command in Vim ":helptags &lt;path&gt;/doc" to index the new TagsParser help file.<br><br>TagsParser is no longer packaged in a vimball because the TagsParser plugin does not require Vim 7.0, but the vimball plugin does.&#160;&#160;I like the concept of a vimball because it makes installation easier, but I need to make sure that the plugin distribution method supports the same number of users that the plugin supports.</td></tr><tr><td>&#160;</td></tr></table><!-- rating table --><form name="rating" method="post">
<input type="hidden" name="script_id" value="1535"><table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066"><tr><td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&#160;
    <input type="submit" value="rate"></td>
</tr></table></form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1535">upload new version</a>)
<p>
Click on the package to download.
</p><p>

</p><table cellspacing="2" cellpadding="4" border="0" width="100%"><tr class="tableheader"><th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=6835">TagsParser-0.9.1.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2007-03-04</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="rowodd" valign="top" width="2000">Addition of configuration examples.<br>Big fixes for tabpage/multiple file editing (sorry it was so busted in 0.9).<br>Addition of debugging functionality.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=6739">TagsParser-0.9.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9</b></td>
    <td class="roweven" valign="top" nowrap><i>2007-02-13</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="roweven" valign="top" width="2000">Addition of some new features, most notably project configurations and tabpage support (these require Vim 7.0 to use though).&#160;&#160;Many bugfixes also implemented.<br></td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=6394">TagsParser-0.7.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-11-08</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="rowodd" valign="top" width="2000">Many updates and bugfixes... Addition of fully Vim native functions, Perl is no longer required for users of Vim 7.0 and greater.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=6228">TagsParser-0.5.vba.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.5</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-09-25</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="roweven" valign="top" width="2000">Vimball of version 0.5</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=6227">TagsParser-0.5.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-09-25</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="rowodd" valign="top" width="2000">Another bugfix release.&#160;&#160;The complete changes are in the change log.&#160;&#160;It has been a while since I uploaded some fixes so I thought it was time.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5824">TagsParser-0.4.vba</a></td>
    <td class="roweven" valign="top" nowrap><b>0.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-06-11</i></td>
    <td class="roweven" valign="top" nowrap>7.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="roweven" valign="top" width="2000">Some bugfixes and modifications.<br>This is the vimball of version 0.4 (instead of a tarball).&#160;&#160;You must have the vimball script installed (<a target="_blank" href="http://www.vim.org/scripts/script.php?script_id=1502">http://www.vim.org/scripts/script.php?script_id=1502</a>), which it is by default on vim 7.</td>
</tr><tr><td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5823">TagsParser-0.4.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.4</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-06-11</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="rowodd" valign="top" width="2000">Some bugfixes and modifications.</td>
</tr><tr><td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5652">TagsParser-0.3.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.3</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-05-07</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10000">Aaron Cornelius</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr></table><small>ip used for rating: </small>
<!-- finish off the framework -->
          </td>
        </tr></table></td>

  